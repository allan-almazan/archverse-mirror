#ifndef PAGENOTFOUND_H
#define PAGENOTFOUND_H

#include "Core/Page.h"
#include "Core/PageStation.h"

class PageNotFound : public IBasePage
{
private:
	static PageNotFound RegPageNotFound;
	static bool Registered;
public:
	virtual std::ostream& pageHandler(std::ostream& outStream, Poco::Net::NameValueCollection postValues, Poco::Net::NameValueCollection cookies);
	virtual std::string getPageName();
	virtual IBasePage* clone();
	virtual ~PageNotFound();
	PageNotFound()
	{
		//lets auto reg ourselves but only once
		if(!Registered)
		{
			PageStation::getPageStation()->addRequestHandler(this);
			Registered = true;
		}
	}
};
#endif