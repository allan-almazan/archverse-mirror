#ifndef HTMLUTILITYS_H
#define HTMLUTILITYS_H

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

std::string& URLDecode(std::string& strIn);
std::string toLower (const std::string & s);
void Split(const std::string& str, const std::string& delim, std::vector<std::string>& output);


#endif