#ifndef BASEPAGE_H
#define BASEPAGE_H

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Exception.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/OptionSet.h>
#include <Poco/Util/HelpFormatter.h>
#include <iostream>
#include <vector>
#include <map>

using Poco::Net::ServerSocket;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServerParams;
using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;

typedef std::pair<std::string, std::string> stringPair;

class IBasePage : public HTTPRequestHandler
{
private:
	std::string _format;
public:
	virtual std::ostream& pageHandler(std::ostream& outStream, Poco::Net::NameValueCollection postValues, Poco::Net::NameValueCollection cookies) = 0;
	virtual std::string getPageName() = 0;
	virtual IBasePage* clone() = 0;
	virtual ~IBasePage()
	{

	}
	IBasePage()
	{
	}
	void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response);
	
};

#endif