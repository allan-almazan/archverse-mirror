#ifndef PAGE_STATION_H
#define PAGE_STATION_H

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerParams.h>

#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPServerParams.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Exception.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/OptionSet.h>
#include <Poco/Util/HelpFormatter.h>

#include <iostream>
#include <vector>
#include <map>
#include <functional>
#include <iterator>
#include <algorithm>
#include <string>
#include "Core/Page.h"

#include "Core/HTMLUtilitys.h"

using Poco::Net::ServerSocket;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServerParams;

//Basic Singleton Factory Design Pattern
class PageStation : public HTTPRequestHandlerFactory
{
private:
	//these handlers must never be free'd they are all allocated on the stack or static variables
	std::map< std::string, IBasePage* > m_PageHandlers;
	std::string _format;
	static PageStation* station;
	PageStation();
public:
	
	static PageStation* getPageStation();
	~PageStation();
	void addRequestHandler(IBasePage* pageHandler);
	HTTPRequestHandler* createRequestHandler(const HTTPServerRequest& request);
};


	/// The main application class.
	///
	/// This class handles command-line arguments and
	/// configuration files.
	/// Start the Archverse executable with the help
	/// option (/help on Windows, --help on Unix) for
	/// the available command line options.
	///
	/// To use the sample configuration file (Archverse.properties),
	/// copy the file to the directory where the Archverse executable
	/// resides. If you start the debug version of the Archverse
	/// (Archverse[.exe]), you must also create a copy of the configuration
	/// file named Archverse.properties. In the configuration file, you
	/// can specify the port on which the server is listening (default
	/// 9980)
	///
	/// To test Archverse you can use any web browser (http://localhost:9980/).
class Archverse: public Poco::Util::ServerApplication
{
public:
	Archverse();
	
	~Archverse();

protected:
	void initialize(Application& self);
		
	void uninitialize();

	void defineOptions(OptionSet& options);

	void handleOption(const std::string& name, const std::string& value);

	void displayHelp();

	int main(const std::vector<std::string>& args);
	
private:
	bool _helpRequested;
};
#endif