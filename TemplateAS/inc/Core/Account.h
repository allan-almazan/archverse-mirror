#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <string>
#include <iostream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

//we dont load these all into memory, only load them for people who are online
class Account
{
private:
	std::string userName;
	std::string passwordHash;
	PlayerEntity* playerEntity;	
public:
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & userName;
		ar & passwordHash;
		ar & playerEntity;
	}
	Account(std::string userName, std::string passwordHash);
	//returns success
	bool AttachPlayer(PlayerEntity* playerEntity);


};

#endif