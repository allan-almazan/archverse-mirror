#ifndef AJAXCONTROL_H
#define AJAXCONTROL_H

#include <string>
#include <boost/function.hpp>
#include <poco/net/namevaluecollection.h>
#include <iostream>

#include "XMLWidget.h"

class AJAXControl : XMLWidget
{
public:
	AJAXControl(std::string& commandName);
	void registerCallback(boost::function<std::ostream& outStream (std::ostream& outStream, Poco::Net::NameValueCollection postValues, Poco::Net::NameValueCollection cookies)> registerFunction)
};

#endif