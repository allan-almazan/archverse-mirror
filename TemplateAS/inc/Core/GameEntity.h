#ifndef GAMEENTITY_H
#define GAMEENTITY_H

#include <iostream>
#include <vector>
#include <loki/visitor.h>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/shared_ptr.hpp>
class GameEntity : public Loki::BaseVisitable<>
{
protected:
	std::vector<boost::shared_ptr<GameEntity> > children;
public:
	void addChild(boost::shared_ptr<GameEntity> entity);
	void removeChild(boost::shared_ptr<GameEntity> entity);
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		children & ar;	
	}
	virtual ~GameEntity() = 0;
	
};

#endif