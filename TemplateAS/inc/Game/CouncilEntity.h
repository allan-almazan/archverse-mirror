#ifndef COUNCILENTITY_H
#define COUNCILENTITY_H

#include "Core/GameEntity.h"
#include "Game/PlayerEntity.h"
#include "Game/CommerceEntity.h"
#include "Game/EffectEntity.h"
#include "Game/AbilityEntity.h"
#include "Game/DiplomaticEntity.h"
#include "Game/NewsEntity.h"
#include "Game/ProjectEntity.h"
#include "Game/ResourceEntity.h"

#include <Boost/shared_ptr.hpp>
#include <vector>
#include <string>

enum CouncilRanking;
class CouncilEntity : public GameEntity
{
private:
	int																		m_gameID;
	int																		m_councilID;

	std::string																m_name;
	std::string																m_description;

	std::vector< boost::shared_ptr<PlayerEntity> >									m_playerList;
	std::vector< boost::shared_ptr<DiplomaticEntity> >								m_diplomaticList;
	std::vector< boost::shared_ptr<NewsEntity> >									m_newsList;
	std::vector< boost::shared_ptr<CommerceEntity> >								m_commerceList;
	std::vector< boost::shared_ptr<AbilityEntity> >								m_abilityList;
	std::vector< boost::shared_ptr<EffectEntity> >									m_effectList;
	std::vector< std::pair<shared_ptr<EffectEntity>, CouncilRanking> >		m_officerList;
	
public:
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		m_gameID & ar;
		m_councilID & ar;
		m_name & ar;
		m_description & ar;
		m_playerList & ar;
		m_diplomaticList & ar;
		m_newsList & ar;
		m_commerceList & ar;
		m_abilityList & ar;
		m_effectList & ar;
		m_officerList & ar;
		((GameEntity*)this) & ar;
	}
};

#endif 