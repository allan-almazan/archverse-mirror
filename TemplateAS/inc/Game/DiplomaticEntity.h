#ifndef DIPLOMATICENTITY_H
#define DIPLOMATICENTITY_H

#include "Core/GameEntity.h"
#include <string>


enum DiplomaticMessageType;
enum DiplomaticMessageTarget;
class DiplomaticEntity : public GameEntity
{
	//static members
private:
	static int messageCounter = 0;
private:
	std::string				m_diplomaticMessage;
	int						m_messageID;
	int						m_senderID;
	int						m_reciverID;
	DiplomaticMessageType	m_messageType;
	DiplomaticMessageTarget	m_targetType;
	
public:
	DiplomaticEntity(std::string diplomaticMessage,  int senderID, 
					int reciverID, DiplomaticMessageType messageType, 
					DiplomaticMessageTarget targetType, int messageID = messageCounter++)
	{
		m_diplomaticMessage = diplomaticMessage;
		m_messageID = messageID;
		m_senderID = senderID;
		m_reciverID = reciverID;
		m_messageType = messageType;
		m_targetType = targetType;
	}
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		m_diplomaticMessage & ar;
		m_messageID & ar;
		m_senderID & ar;
		m_reciverID & ar;
		m_targetType & ar;
		((GameEntity*)this) & ar;
	}
};
enum DiplomaticMessageType
{
	NORMAL = 0,
	OPENTRADE,
	CLOSETRADE,
	OFFERTRUCE,
	DEMANDTRUCE,
	ACCEPTTRUCE,
	OFFERALLIANCE,
	ACCEPTALLIANCE
};
enum DiplomaticMessageTarget
{
	PLAYER = 0,
	COUNCIL,
	EMPIRE
};

#endif 