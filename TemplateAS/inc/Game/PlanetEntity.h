#ifndef PLANETENTITY_H
#define PLANETENTITY_H

#include "Core/GameEntity.h"

class PlanetEntity : public GameEntity
{
private:

public:
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
	}
private:
	std::string 
		m_name;
	std::vector< boost::shared_ptr<ControlModel> > m_controlModel;
	boost::shared_ptr<PlayerEntity> m_owner;
	boost::shared_ptr<ClusterEntity> m_cluster;
};

#endif 