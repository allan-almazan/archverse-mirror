#ifndef RACE_H
#define RACE_H

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

#include "AbilityEntity.h"
#include "EffectEntity.h"
#include "EnvironmentEntity.h"

enum RaceSociety;
enum RaceIndex
class Race
{
private:
	std::string											m_raceName;
	std::string											m_raceDescription;
	RaceSociety											m_raceSociety;
	RaceIndex											m_raceIndex;
	std::vector< boost::shared_ptr<EffectEntity> >		m_racialEffects;
	std::vector< boost::shared_ptr<AbilityEntity> >		m_racialAbilitys;
	std::vector< boost::shared_ptr<EnvironmentEntity> > m_racialEnvironment;
	std::vector< boost::shared_ptr<TechEntity> >		m_racialInateTechs;
public:
};
enum RaceSociety 
{
	SOCIETY_TOTALISM = 0,
	SOCIETY_CLASSISM,
	SOCIETY_PERSONALISM
};

enum RaceIndex
{
	RACE_HUMAN = 0,
	RACE_TARGOID,
	RACE_BUCKANEER,
	RACE_TECANOID,
	RACE_EVINTOS,
	RACE_AGERUS,
	RACE_BOSALIAN,
	RACE_XELOSS,
	RACE_XERUSIAN,
	RACE_XESPERADOS
};
#endif