#ifndef PROJECTENTITY_H
#define PROJECTENTITY_H

#include "Core/GameEntity.h"

#include <string>
#include <vector>
#include <Boost/shared_ptr.hpp>

class ProjectEntity : public GameEntity
{
private:
	int													m_projectID;
	
	std::string											m_name;
	std::string											m_description;
	
	boost::shared_ptr<ResourceEntity>					m_cost;

	std::vector< boost::shared_ptr<AbilityEntity> >		m_abilityList;
	std::vector< boost::shared_ptr<EffectEntity> >		m_effectList;
public:
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		m_projectID & ar;
		m_name & ar;
		m_description & ar;
		m_cost & ar;
		m_abilityList & ar;
		m_effectList & ar;
		((GameEntity*)this) & ar;
	}
};

#endif 