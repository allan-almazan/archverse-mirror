#ifndef TECHENTITY_H
#define TECHENTITY_H

#include "Core/GameEntity.h"

class TechEntity : public GameEntity
{
public:
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
	}
private:
	//Tech Entitys have Descriptions, Levels, Costs, Effects
	std::string 
		m_description,
		m_name;
	int 
		m_level,
		m_rpCost;

	std::vector< boost::shared_ptr<EffectEntity> > m_effects;
	std::vector< boost::shared_ptr<TechEntity> > m_prerequisites;
};

#endif 