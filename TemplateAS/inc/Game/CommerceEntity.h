#ifndef COMMERCEENTITY_H
#define COMMERCEENTITY_H

#include "Core/GameEntity.h"


class CommerceEntity : public GameEntity
{
	//Questionable design practices might want to set up some weak references to participants and planets instead of ids
	//must avoid circular references
private:
	int							m_participantID1;
	int							m_participantID2;
	int							m_planetID1;
	int							m_planetID2;
	
public:
	CommerceEntity(int participantID1, int participantID2, int plantID1, int planetID2);
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		m_participantID1 & ar;
		m_participantID2 & ar;
		m_planetID1 & ar;
		m_planetID2 & ar;
		((GameEntity*)this) & ar;
	}
};

#endif 