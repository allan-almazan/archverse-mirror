#ifndef ENVIRONMENTENTITY_H
#define ENVIRONMENTENTITY_H

#include "Core/GameEntity.h"

#include <string>
#include <vector>

enum EnvironmentType;
class EnvironmentEntity : public GameEntity
{
private:
	std::vector< std::pair<int, EnvironmentType> >		m_environmentList; 
public:
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		m_environmentList & ar;
		((GameEntity*)this) & ar;
	}
};

#endif 