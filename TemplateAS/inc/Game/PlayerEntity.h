#ifndef PLAYERENTITY_H
#define PLAYERENTITY_H

#include "Core/GameEntity.h"

#include "Game/ControlModel.h"
#include "Game/Race.h"
#include "Game/AbilityEntity.h"
#include "Game/AdmiralEntity.h"
#include "Game/ClusterEntity.h"
#include "Game/CommerceEntity.h"
#include "Game/CouncilEntity.h"
#include "Game/DiplomaticEntity.h"
#include "Game/EffectEntity.h"
#include "Game/EnvironmentEntity.h"
#include "Game/FleetEntity.h"
#include "Game/PlanetEntity.h"
#include "Game/ProjectEntity.h"
#include "Game/ResourceEntity.h"
#include "Game/ShipEntity.h"
#include "Game/TechEntity.h"

#include <Poco/Net/IPAddress.h>
#include <Poco/Timestamp.h>
#include <Boost/shared_ptr.hpp>
#include <vector>
#include <string>

class PlayerEntity : public GameEntity
{
	//static members
private:
	static int playerCounter = 0;
private:
	std::vector<std::pair<Poco::Net::IPAddress, Poco::Timestamp> >	m_recentLogins;
	boost::shared_ptr<Race>											m_race;
	boost::shared_ptr<ControlModel>									m_controlModel;
	shared_ptr<EnvironmentEntity>									m_preferedEnvironment;
	std::string														m_playerName;
	int																m_gameID;
	int																m_playerID;
	int																m_accountID;

	std::vector< boost::shared_ptr<PlanetEntity> >							m_planetList;
	std::vector< boost::shared_ptr<AbilityEntity> >							m_abilityList;
	std::vector< boost::shared_ptr<EffectEntity> >							m_effectList;
	std::vector< boost::shared_ptr<ProjectEntity> >							m_ownedProjectList;
	std::vector< boost::shared_ptr<ResourceEntity> >						m_ownedResourceList;
	std::vector< boost::shared_ptr<TechEntity> >							m_knownTechList;
	std::vector< boost::shared_ptr<NewsEntity> >							m_newsList;
	std::vector< boost::shared_ptr<PreferenceEntity> >						m_preferenceList;


public:
	//need to add some stuff in here about gameID, needs to be auto incremented by universe
	PlayerEntity(boost::shared_ptr<Race>& race, std::string& name, int accountID, int playerID = PlayerEntity::playerCounter++);

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		m_recentLogins & ar;
		m_race & ar;
		m_controlModel & ar;
		m_playerName & ar;
		m_gameID & ar;
		m_playerID & ar;
		m_accountID & ar;
		m_planetList & ar;
		m_abilityList & ar;
		m_effectList & ar;
		m_preferedEnvironment & ar;
		m_ownedProjectList & ar;
		m_knownTechList & ar;
		m_newsList & ar;
		m_preferenceList & ar;
		((GameEntity*)this) & ar;
	}
	void setControlModel(boost::shared_ptr<ControlModel>& controlModel);
	void addLogin(Poco::Net::IPAddress& ip);
	void addPlanet(shared_ptr<PlanetEntity>& planet);
	void addAbility(shared_ptr<AbilityEntity>& ability);
	void addEffect(shared_ptr<EffectEntity>& effect);
	void addProject(shared_ptr<ProjectEntity>& project);
	void addTech(shared_ptr<TechEntity>& tech);
	void addNews(shared_ptr<NewsEntity>& news);
	void addPreference(shared_ptr<PreferenceEntity>& preference);
};

#endif 