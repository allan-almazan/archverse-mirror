#ifndef FLEETENTITY_H
#define FLEETENTITY_H

#include "Core/GameEntity.h"
enum FleetOwnerType;
class FleetEntity : public GameEntity
{
private:
	int					m_fleetID;
	int					m_ownerID;
	FleetOwnerType		m_ownerType;
public:
	FleetEntity(int fleetID, int ownerID, FleetOwnerType ownerType)
	{
		m_fleetID = fleetID;
		m_ownerID = ownerID;
		m_ownerType = ownerType;
	}
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		m_fleetID & ar;
		m_ownerID & ar;
		m_ownerType & ar;
		((GameEntity*)this) & ar;
	}
};
enum FleetOwnerType
{
	PLAYER = 0,
	COUNCIL,
	EMPIRE
};

#endif 