#ifndef EFFECTENTITY_H
#define EFFECTENTITY_H

#include "Core/GameEntity.h"


enum EffectApplication;
enum EffectType;

class EffectEntity : public GameEntity
{
private:
	EffectApplication			m_applicationType;
	EffectType					m_effectType;
	int							m_effectValue;
public:
	EffectEntity(EffectApplication applicationType, EffectType effectType, int effectValue);
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		m_applicationType & ar;
		m_effectType & ar;
		m_effectValue & ar;
		((GameEntity*)this) & ar;
	}
};

#endif 