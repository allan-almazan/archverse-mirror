#include "Core/HTMLUtilitys.h"

using namespace std;

std::string& URLDecode(std::string& strIn)
{
	//stringstream tempSS;
	//int i, j;
	//for(i=0, j=0; strIn.at(j); i++, j++)
	//{
	//	if ((strIn.at(i) = strIn.at(j)) == '%')
	//	{
	//		tempSS << std::hex << &strIn.at(j+1)
	//			strIn.at(i) = tempSS //= hex2dec(&strIn.at(j+1));
	//		j += 2;
	//	} else if (strIn.at(j) == '+')
	//	{
	//		strIn.at(i) = ' ';
	//	}
	//}
	//strIn.at(i) = 0;
	return strIn;
}
std::string toLower (const std::string & s)
{
	std::string d (s);

	std::transform (d.begin (), d.end (), d.begin (), tolower);
	return d;
}  // end of tolower
void Split(const std::string& str, const std::string& delim, std::vector<std::string>& output)
{
    size_t offset = 0;
    size_t delimIndex = 0;
    
    delimIndex = str.find(delim, offset);

	while (delimIndex != std::string::npos)
    {
        output.push_back(str.substr(offset, delimIndex - offset));
        offset += delimIndex - offset + delim.length();
        delimIndex = str.find(delim, offset);
    }

    output.push_back(str.substr(offset));
}

