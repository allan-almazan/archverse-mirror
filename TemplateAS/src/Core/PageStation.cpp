#include "Core/PageStation.h"
#include "Core/Page.h"


PageStation::PageStation()	
{
	
}

PageStation* PageStation::getPageStation()
{
	if(station == NULL)
	{
		station = new PageStation();
		
	}	
	return station;
}

PageStation::~PageStation()
{
	//if this is being called someone (poco) is not playing nicely with singletons
	//so we have to save ourselves and make sure we dont call something we just free'd
	station = NULL;
}

void PageStation::addRequestHandler(IBasePage* pageHandler)
{
	std::pair<std::string, IBasePage* > pair;
	
	//case insensitive file names are quite helpfull on the web
	std::string pageName (toLower(pageHandler->getPageName()));
	pair.first = pageName;
	pair.second = pageHandler;
	m_PageHandlers.insert(pair);
}

HTTPRequestHandler* PageStation::createRequestHandler(const HTTPServerRequest& request)
{

	IBasePage* PageHandler;
	
	//this is the 2nd half to make sure we always deal with case insensitive requests
	std::string pageRequestName ( toLower(request.getURI())) ;
	std::map< std::string, IBasePage* >::const_iterator itr = m_PageHandlers.find(pageRequestName);
	if( itr!= m_PageHandlers.end())
	{
		//Page Handler is going to be free'd by poco so we have to clone 
		//ourselves in order to work for the next request
		PageHandler = itr->second->clone();
	}
	else
	{
		PageHandler = m_PageHandlers.begin()->second->clone();
	}
	return PageHandler;
}



Archverse::Archverse(): _helpRequested(false)
{
}

Archverse::~Archverse()
{
}


void Archverse::initialize(Application& self)
{
	loadConfiguration(); // load default configuration files, if present
	ServerApplication::initialize(self);
}
	
void Archverse::uninitialize()
{
	ServerApplication::uninitialize();
}

void Archverse::defineOptions(OptionSet& options)
{
	ServerApplication::defineOptions(options);
	
	options.addOption(
		Option("help", "h", "display help information on command line arguments")
			.required(false)
			.repeatable(false));
}

void Archverse::handleOption(const std::string& name, const std::string& value)
{
	ServerApplication::handleOption(name, value);

	if (name == "help")
		_helpRequested = true;
}

void Archverse::displayHelp()
{
	HelpFormatter helpFormatter(options());
	helpFormatter.setCommand(commandName());
	helpFormatter.setUsage("OPTIONS");
	helpFormatter.setHeader("A Web server for the online game Archverse");
	helpFormatter.format(std::cout);
}

int Archverse::main(const std::vector<std::string>& args)
{
	if (_helpRequested)
	{
		displayHelp();
	}
	else
	{
		// get parameters from configuration file
		unsigned short port = (unsigned short) config().getInt("Archverse.port", 9980);
		
		
		// set-up a server socket
		ServerSocket svs(port);
		// set-up a HTTPServer instance
		HTTPServerParams* params = new HTTPServerParams;
		
		HTTPServer srv(PageStation::getPageStation(), svs, params);
		
		// start the HTTPServer
		srv.start();
		// wait for CTRL-C or kill
		waitForTerminationRequest();
		// Stop the HTTPServer
		srv.stop();
		
	}
	return Application::EXIT_OK;
}

PageStation* PageStation::station = 0;// initialize singleton pointer

