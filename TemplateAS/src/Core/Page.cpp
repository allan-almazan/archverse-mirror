#include"Core/Page.h"
#include"Core/HTMLUtilitys.h"

#include <string>
#include <vector>
#include <algorithm>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

void toStringPair(std::string& str, Poco::Net::NameValueCollection& data)
{
	std::vector<std::string> splitElement;
	Split(str,"=", splitElement);
	if(splitElement.size() == 2)
	{
		data.add(URLDecode(splitElement[0]), URLDecode(splitElement[1]));
	}
}

void IBasePage::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response)
{
	Poco::Net::NameValueCollection postValues;
	Poco::Net::NameValueCollection postCookies;
	std::vector<std::string> headerParts;
	std::string headerRequest;
	Application& app = Application::instance();
	app.logger().information("Request from " + request.clientAddress().toString());	
	
	//Parse out and decode the Post Data from the request
	std::istream& istr = request.stream();
	istr >> headerRequest;
	Split(headerRequest, "&", headerParts);
	
	std::for_each(headerParts.begin(), headerParts.end(),bind(toStringPair, boost::lambda::_1, boost::lambda::var(postValues)));

	//Parse out and decode cookies from the request
	
	request.getCookies(postCookies);
	

	response.setChunkedTransferEncoding(true);
	response.setContentType("text/html");
	
	std::ostream& ostr = response.send();
	pageHandler(ostr, postValues, postCookies);
}


