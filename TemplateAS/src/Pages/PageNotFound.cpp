#include "Pages/PageNotFound.h"

std::ostream& PageNotFound::pageHandler(std::ostream& outStream, Poco::Net::NameValueCollection postValues, Poco::Net::NameValueCollection cookies)
{
	std::string postData = "";
	
	outStream << "<?xml version=\"1.0\"?>\n";
	outStream << "<?xml-stylesheet type=\"text/xsl\" href=\"http://www.archverse.com/PageNotFound.xslt\" ?>\n";
	outStream << "<page>\n";

	if(postValues.find(postData) != postValues.end())
		postData = postValues.get(postData);

	if (postData == "")
	{
		postData = "";
	}

	outStream << "\t<title>Page Not Found</title>\n";
	outStream << "\t<post>" << postData << "</post>\n";
	outStream << "</page>\n";

	return outStream;
}
IBasePage* PageNotFound::clone()
{
	return new PageNotFound();
}
std::string PageNotFound::getPageName()
{
	return "/";
}

PageNotFound::~PageNotFound()
{

}
bool PageNotFound::Registered = false;
PageNotFound PageNotFound::RegPageNotFound;