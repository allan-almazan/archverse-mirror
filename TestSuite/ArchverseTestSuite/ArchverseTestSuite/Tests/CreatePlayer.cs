﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace ArchverseTestSuite
{
    class CreatePlayer
    {
        public static bool BasicTest()
        {
            string auth = Login.LoginForAuth();
            string str = HTTPPost.HttpPost("http://localhost:9890/ArchverseCore/Pages/CreatePlayer.as", "{\"AuthString\" : \"" + auth + "\", \"PlayerName\" : \"Noob\", \"RaceId\" : 0}");
            if(str == "{\n}")
                return true;
            else
                return false;
        }
    }
}
