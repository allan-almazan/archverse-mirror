﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace ArchverseTestSuite
{
    class SetBuildRatio
    {
        public static bool BasicTest()
        {
            string auth = Login.LoginForAuth();
            string str = HTTPPost.HttpPost("http://localhost:9890/ArchverseCore/Pages/Domestic/SetBuildRatio.as", "{\"AuthString\" : \"" + auth + "\", \"TerritoryId\" : 0, \"Factories\" : 10, \"Mines\" : 10, \"MillitaryBases\" : 10, \"Labs\" : 70}");
            JsonSerializer serializer = new JsonSerializer();
            JContainer obj = serializer.Deserialize(new JsonTextReader(new StringReader(str))) as JContainer;
            JToken token = obj["Name"];
            if (token != null && token.Type != JsonTokenType.Null)
                return true;
            else
                return false;
        }
    }
}

