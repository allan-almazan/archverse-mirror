﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace ArchverseTestSuite
{
    class InteractWithAction
    {
        public static bool BasicTest()
        {
            string auth = Login.LoginForAuth();
            string str = HTTPPost.HttpPost("http://localhost:9890/ArchverseCore/Pages/Domestic/InteractWithAction.as", "{\"AuthString\" : \"" + auth + "\", \"ActionType\" : 0, \"ActionChoice\" : true}");
            JsonSerializer serializer = new JsonSerializer();
            JContainer obj = serializer.Deserialize(new JsonTextReader(new StringReader(str))) as JContainer;
            JToken token = obj["Actions"];
            if (token != null && token.Type != JsonTokenType.Null)
                return true;
            else
                return false;
        }
    }
}
