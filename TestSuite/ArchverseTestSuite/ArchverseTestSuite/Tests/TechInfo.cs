﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace ArchverseTestSuite.Tests
{
    class TechInfo
    {
        public static bool BasicTest()
        {
            string str = HTTPPost.HttpPost("http://localhost:9890/ArchverseCore/Pages/Info/TechInfo.as", "{\"TechId\" : 1}");
            JsonSerializer serializer = new JsonSerializer();
            JContainer obj = serializer.Deserialize(new JsonTextReader(new StringReader(str))) as JContainer;
            JToken token = obj["Id"];
            if (token.Type != JsonTokenType.Null)
                return true;
            else
                return false;
        }
    }
}

