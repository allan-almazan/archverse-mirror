﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ArchverseTestSuite
{
    class Login
    {
        public static string LoginForAuth()
        {
            string str = HTTPPost.HttpPost("http://localhost:9890/ArchverseCore/Pages/Login.as", "{\"UserName\" : \"noobcity\", \"Password\" : \"noob\"}");
            JsonSerializer serializer = new JsonSerializer();
            JContainer obj = serializer.Deserialize(new JsonTextReader(new StringReader(str))) as JContainer;
            JToken token = obj["AuthString"];
            if (token.Type != JsonTokenType.Null)
                return obj.Value<string>("AuthString");
            else
                throw new Exception("Failed to log in");
        }
        public static bool BasicTest()
        {
            string str = HTTPPost.HttpPost("http://localhost:9890/ArchverseCore/Pages/Login.as", "{\"UserName\" : \"noobcity\", \"Password\" : \"noob\"}");
            JsonSerializer serializer = new JsonSerializer();
            JContainer obj = serializer.Deserialize(new JsonTextReader(new StringReader(str))) as JContainer;
            JToken token = obj["AuthString"];
            if(token.Type != JsonTokenType.Null)
                return true;
            else
                return false;
        }
    }
}
