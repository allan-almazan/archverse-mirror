﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace ArchverseTestSuite.Tests
{
    class ComponentInfo
    {
        public static bool BasicTest()
        {
            string str = HTTPPost.HttpPost("http://localhost:9890/ArchverseCore/Pages/Info/ComponentInfo.as", "{\"ComponentId\" : 0}");
            JsonSerializer serializer = new JsonSerializer();
            JContainer obj = serializer.Deserialize(new JsonTextReader(new StringReader(str))) as JContainer;
            JToken token = obj["ComponentType"];
            if (token.Type != JsonTokenType.Null)
                return true;
            else
                return false;
        }
    }
}
