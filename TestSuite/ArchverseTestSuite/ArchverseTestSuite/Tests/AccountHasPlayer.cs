﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;

namespace ArchverseTestSuite
{
    class AccountHasPlayer
    {
        public static bool BasicTest()
        {
            string auth = Login.LoginForAuth();
            string str = HTTPPost.HttpPost("http://localhost:9890/ArchverseCore/Pages/AccountHasPlayer.as", "{\"AuthString\" : \"" + auth + "\"}");
            JsonSerializer serializer = new JsonSerializer();
            JContainer obj = serializer.Deserialize(new JsonTextReader(new StringReader(str))) as JContainer;
            JToken token = obj["Result"];
            if (token.Type != JsonTokenType.Null)
                return true;
            else
                return false;
        }
    }
}
