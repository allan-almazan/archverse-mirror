﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ArchverseTestSuite.Tests;

namespace ArchverseTestSuite
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Base Tests");
            Console.WriteLine(CreateAccount.BasicTest());
            Console.WriteLine(Login.BasicTest());
            Console.WriteLine(AccountHasPlayer.BasicTest());
            Console.WriteLine(CreatePlayer.BasicTest());
            Console.WriteLine("Info Tests");
            Console.WriteLine(ComponentInfo.BasicTest());
            Console.WriteLine(TechInfo.BasicTest());
            Console.WriteLine(RaceInfo.BasicTest());
            Console.WriteLine("Domestic Tests");
            Console.WriteLine(AvailibleGovernments.BasicTest());
            Console.WriteLine(BuildProject.BasicTest());
            Console.WriteLine(Summary.BasicTest());
            Console.WriteLine(ChangeGovernment.BasicTest());
            Console.WriteLine(ChangeResearch.BasicTest());
            Console.WriteLine(CurrentGovernment.BasicTest());
            Console.WriteLine(CurrentProject.BasicTest());
            Console.WriteLine(CurrentBuildRatio.BasicTest());
            Console.WriteLine(CurrentResearch.BasicTest());
            Console.WriteLine(InteractableActions.BasicTest());
            Console.WriteLine(InteractWithAction.BasicTest());
            Console.WriteLine(KnownTechs.BasicTest());
            Console.WriteLine(OwnedPlanets.BasicTest());
            Console.WriteLine(TerritorySummary.BasicTest());
            Console.WriteLine(SetBuildRatio.BasicTest());
            Console.ReadKey();
        }
    }
}
