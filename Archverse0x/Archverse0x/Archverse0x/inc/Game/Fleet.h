#ifndef ARCHVERSECORE_GAME_FLEET
#define ARCHVERSECORE_GAME_FLEET

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"
#include"Game\Ship.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using namespace json_spirit;
		using namespace ArchverseCore::System;
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;

		class FleetList;
		class FleetTable;
		class FleetPresentation_JSON;
		class FleetStorage_BDB;
		class Player;
		class Council;

		class Fleet
		{
		public:
			enum FleetMissionType
			{
				FM_TRAIN,
				FM_EXPEDITION,
				FM_STATION,
				FM_BLOCKADE,
				FM_RAID,
				FM_PATROL
			};
			friend class FleetList;
			friend class FleetTable;
			friend class FleetPresentation_JSON;
			friend class FleetStorage_BDB;
		private:
			uint32_t _id;
			uint32_t _admiralId;
			uint32_t _ownerId;
			uint32_t _attackFormation;
			uint32_t _defenseFormation;
			uint32_t _attackOrders;
			uint32_t _defenseOrders;
			FleetMissionType _currentMission;
			uint32_t _missionStartTurn;
			uint32_t _missionData;
			string _name;
			vector<Ship> _ships;

		};

		class FleetList
		{
			vector<uint32_t>& _fleets;
		public:
			FleetList(vector<uint32_t>& fleets) : _fleets(fleets) {}
			void ProcessMissions(Council& council, Player& player);
		};

		class FleetTable : public DBCore<Fleet, FleetStorage_BDB>
		{
		public:
			bool FleetById(uint32_t fleetId, Fleet& destination);
		};

		class FleetStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const Fleet& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, Fleet& destination);
		};
	}
}

#endif //ARCHVERSECORE_GAME_FLEET