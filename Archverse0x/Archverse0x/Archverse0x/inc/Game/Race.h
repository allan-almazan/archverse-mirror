#ifndef ARCHVERSECORE_GAME_RACE
#define ARCHVERSECORE_GAME_RACE

#include"System\DBCore.h"
#include"System\JSON\json_spirit.h"
#include"Game\ControlModel.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using namespace json_spirit;
		using ArchverseCore::System::DBCore;

		class Tech;
		class RaceTable;
		class RacePresentation_JSON;

		class Race
		{
			friend class RaceTable;
			friend class RacePresentation_JSON;

			enum SocialType
			{
				SOCIETY_TOTALISM,
				SOCIETY_CLASSISM,
				SOCIETY_PERSONALISM
			};
		private:
			uint32_t _id;
			SocialType _socialType;
			ControlModel _controlModel;
			string _name;
			string _description;
			vector<uint32_t> _abilities;
			vector<uint32_t> _startingTechs;
		public:
			const string& Name() const {return _name;}
			void AddStartingTech(const Tech& techId);
			static string SocialTypeString(SocialType socialType);
		};
		class RaceTable
		{
		private:
			vector<Race> _races;
		public:
			RaceTable();
			//throws on failure
			Race& RaceFromId(uint32_t raceId);
			Race& Seraphim() {return _races[0];}
			Race& Human() {return _races[1];}
		};
		class RacePresentation_JSON
		{
		public:
			static void RaceOverview(Race& race, Object& destination);
		};
	}
}

#endif //ARCHVERSECORE_GAME_RACE