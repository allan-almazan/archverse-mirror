#ifndef ARCHVERSECORE_GAME_TECH
#define ARCHVERSECORE_GAME_TECH

#include"System\DBCore.h"
#include"System\JSON\json_spirit.h"
#include"Game\ControlModel.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using std::vector;
		using std::string;
		using boost::uint8_t;
		using json_spirit::Object;


		class TechList;
		class TechTable;
		class TechPresentation_JSON;

		class Tech
		{
			friend class TechList;
			friend class TechTable;
			friend class TechPresentation_JSON;
		private:
			uint32_t _id;
			ControlModel _modifiers;
			vector<uint32_t> _spyOps;
			vector<uint32_t> _buildings;
			vector<uint32_t> _components;
			vector<uint32_t> _prerequisites;
			string _name;
			string _description;
			string _category;
		public:
			const string& Name() const {return _name;}
			uint32_t Id() const {return _id;}
		};

		class TechList
		{
			friend class TechPresentation_JSON;
		private:
			vector<uint32_t>& _techs;
		public:
			TechList(vector<uint32_t>& techs) : _techs(techs) {};
		};
		class TechTable
		{
		private:
			vector<Tech*> _techList;
		public:
			TechTable();
			~TechTable();
			Tech& TechFromId(uint32_t techId);
		};

		class TechPresentation_JSON
		{
		public:
			static void TechOverview(Tech& tech, Object& destination);
			static void TechListOverview(TechList& techList, Object& destination);
		};
	}
}

#endif //ARCHVERSECORE_GAME_TECH