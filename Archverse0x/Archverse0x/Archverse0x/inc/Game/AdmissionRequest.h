#ifndef ARCHVERSECORE_GAME_ADMISSIONREQUEST
#define ARCHVERSECORE_GAME_ADMISSIONREQUEST

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using std::pair;
		using ArchverseCore::System::DBCore;

		class Council;
		class AdmissionRequestList;
		class AdmissionRequestTable;
		class AdmissionRequestStorage_BDB;
		class AdmissionRequestPresentation_JSON;

		class AdmissionRequest
		{
			friend class AdmissionRequestList;
			friend class AdmissionRequestTable;
			friend class AdmissionRequestStorage_BDB;
			friend class AdmissionRequestPresentation_JSON;
		private:
			uint32_t _id;
			uint32_t _playerId;
			uint32_t _councilId;
			string _message;
		public:
			uint32_t Id() {return _id;}
			uint32_t PlayerId() {return _playerId;}
			uint32_t CouncilId() {return _councilId;}
			const string& Message() {return _message;}
		};
		class AdmissionRequestTable : public DBCore<AdmissionRequest, AdmissionRequestStorage_BDB>
		{
		public:
			void CreateAdmissionRequest(uint32_t councilId, uint32_t playerId, const string& message, AdmissionRequest& destination);
		};
		class AdmissionRequestList
		{
			friend class AdmissionRequestPresentation_JSON;
		private:
			vector<uint32_t>& _admissionRequests;
		public:
			AdmissionRequestList(vector<uint32_t>& admissionRequests) : _admissionRequests(admissionRequests) {}
			void AddRequest(uint32_t requestId);
		};

		class AdmissionRequestStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const AdmissionRequest& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, AdmissionRequest& destination);
		};

		class AdmissionRequestPresentation_JSON
		{
		public:
			static void AdmissionRequestOverview(Council& council, Object& destination);
			static void AdmissionRequestDetail(uint32_t admissionId, Object& destination);
		};
	}
}
#endif //ARCHVERSECORE_GAME_ADMISSIONREQUEST