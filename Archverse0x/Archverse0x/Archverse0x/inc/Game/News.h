#ifndef ARCHVERSECORE_GAME_NEWS
#define ARCHVERSECORE_GAME_NEWS

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using namespace json_spirit;
		using namespace ArchverseCore::System;

		class NewsTable;
		class NewsList;
		class NewsStorage_BDB;
		class NewsPresentation_JSON;

		class News
		{
			friend class NewsTable;
			friend class NewsList;
			friend class NewsStorage_BDB;
			friend class NewsPresentation_JSON;
		private:
			uint32_t _id;
			uint32_t _turn;
			uint32_t _owner;
			string _title;
			string _body;
			
		};

		class NewsTable : public DBCore<News, NewsStorage_BDB>
		{
		public:
			bool NewsById(uint32_t newsId, News& destination);
		};

		class NewsList
		{
			friend class NewsPresentation_JSON;
		private:
			vector<uint32_t>& _news;
		public:
			NewsList(vector<uint32_t>& news) : _news(news) {}
			void CreateUpdateNews(int popChange, int mineChange, int factoryChange, int millitaryBaseChange, int labChange, uint32_t turn);
		};

		class NewsStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const News& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, News& destination);
		};

		class NewsPresentation_JSON
		{
		public:
			static void NewsOverview(News& news, Object& destination);
			static void NewsListOverview(NewsList& news, Object& destination);
		};
	}
}

#endif //ARCHVERSECORE_GAME_NEWS