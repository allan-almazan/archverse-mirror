#ifndef ARCHVERSECORE_GAME_SHIPHULL
#define ARCHVERSECORE_GAME_SHIPHULL

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using namespace json_spirit;
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;

		class ShipDesign;
		class ShipHullTable;
		class ShipHullList;
		class ShipHullPresentation_JSON;

		struct WeaponSlot
		{
			enum WeaponSlotType
			{
				FIXED,
				TURRET
			};
		public:
			float _direction;
			WeaponSlotType _type;
			uint32_t _maxSize;
			static string WeaponTypeString(WeaponSlotType slotType);
		};
		class ShipHull
		{
			friend class ShipHullTable;
			friend class ShipHullList;
			friend class ShipHullPresentation_JSON;
		private:
			uint32_t _id;
			uint32_t _size; //in tonnes
			uint32_t _baseHp;
			uint32_t _baseShield;
			uint32_t _baseCost;
			string _name;
			string _description;
			vector<WeaponSlot> _weaponSlots;
			vector<uint32_t> _prerequisites;
		public:
			uint32_t Id() const {return _id;}
			const string& Name() const {return _name;}
			
		};

		class ShipHullTable
		{
		public:
			const ShipHull& ShipHullFromId(uint32_t id);
			bool ValidateShipDesignForHull(ShipDesign& shipDesign);
		};

		class ShipHullList
		{
		private:
			vector<uint32_t>& _shipHulls;
		public:
			ShipHullList(vector<uint32_t>& shipHulls) : _shipHulls(shipHulls) {}
		};

		class ShipHullPresentation_JSON
		{
		public:
			static void ShipHullOverview(ShipHull& hull, Object& destination);
		};
	}
}

#endif //ARCHVERSECORE_GAME_SHIPHULL