#ifndef ARCHVERSECORE_GAME_ARCHVERSE
#define ARCHVERSECORE_GAME_ARCHVERSE

#include<boost\cstdint.hpp>
#include<vector>

namespace ArchverseCore
{
	namespace Game
	{
		class GameInfo;
		class UserAccountTable;
		class MessageTable;
		class RelationTable;
		class RegionTable;
		class TerritoryTable;
		class PlayerTable;
		class CouncilTable;
		class AdmissionRequestTable;
		class RaceTable;
		class PrerequisiteTable;
		class TechTable;
		class ComponentTable;
		class NewsTable;
		class ShipDesignTable;
		class ProjectTable;
		class EventTable;
		class FleetTable;
		class ActionTable;
		class AdmiralTable;

		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;

		class Archverse
		{
		private:
			GameInfo* _gameInfo;
			UserAccountTable* _userAccountTable;
			MessageTable* _playerMessageTable;
			MessageTable* _councilMessageTable;
			RelationTable* _playerRelationTable;
			RelationTable* _councilRelationTable;
			RegionTable* _regionTable;
			TerritoryTable* _territoryTable;
			PlayerTable* _playerTable;
			CouncilTable* _councilTable;
			AdmissionRequestTable* _admissionTable;
			RaceTable* _raceTable;
			PrerequisiteTable* _prerequisiteTable;
			TechTable* _techTable;
			ComponentTable* _componentTable;
			ShipDesignTable* _shipDesignTable;
			NewsTable* _newsTable;
			ProjectTable* _projectTable;
			EventTable* _eventTable;
			FleetTable* _fleetTable;
			ActionTable* _actionTable;
			AdmiralTable* _admiralTable;
		public:
			Archverse();
			~Archverse();
			GameInfo* GameInfo() { return _gameInfo; }
			UserAccountTable* UserAccountTable() { return _userAccountTable; }
			MessageTable* PlayerMessageTable() { return _playerMessageTable; }
			MessageTable* CouncilMessageTable() { return _councilMessageTable; }
			RelationTable* PlayerRelationTable() { return _playerRelationTable; }
			RelationTable* CouncilRelationTable() { return _councilRelationTable; }
			RegionTable* RegionTable() { return _regionTable; }
			TerritoryTable* TerritoryTable() { return _territoryTable; }
			PlayerTable* PlayerTable() { return _playerTable; }
			CouncilTable* CouncilTable() {return _councilTable;}
			AdmissionRequestTable* AdmissionRequestTable() { return _admissionTable; }
			RaceTable* RaceTable() { return _raceTable; }
			TechTable* TechTable() { return _techTable; }
			PrerequisiteTable* PrerequisiteTable() { return _prerequisiteTable; }
			ComponentTable* ComponentTable() { return _componentTable; }
			ShipDesignTable* ShipDesignTable() { return _shipDesignTable; }
			NewsTable* NewsTable() { return _newsTable; }
			ProjectTable* ProjectTable() { return _projectTable; }
			EventTable* EventTable() { return _eventTable; }
			FleetTable* FleetTable() { return _fleetTable; }
			ActionTable* ActionTable() { return _actionTable; }

			static const uint32_t TICKS_IN_TURN = 300;
		};
	}
}
extern ArchverseCore::Game::Archverse* g_Archverse;
#define GAMEINFO (*g_Archverse->GameInfo())
#define USER_ACCOUNT_TABLE (*g_Archverse->UserAccountTable())
#define PLAYER_MESSAGE_TABLE (*g_Archverse->PlayerMessageTable())
#define COUNCIL_MESSAGE_TABLE (*g_Archverse->CouncilMessageTable())
#define REGION_TABLE (*g_Archverse->RegionTable())
#define TERRITORY_TABLE (*g_Archverse->TerritoryTable())
#define PLAYER_TABLE (*g_Archverse->PlayerTable())
#define COUNCIL_TABLE (*g_Archverse->CouncilTable())
#define PLAYER_RELATION_TABLE (*g_Archverse->PlayerRelationTable())
#define COUNCIL_RELATION_TABLE (*g_Archverse->CouncilRelationTable())
#define ADMISSION_REQUEST_TABLE (*g_Archverse->AdmissionRequestTable())
#define RACE_TABLE (*g_Archverse->RaceTable())
#define TECH_TABLE (*g_Archverse->TechTable())
#define PREREQUISITE_TABLE (*g_Archverse->PrerequisiteTable())
#define COMPONENT_TABLE (*g_Archverse->ComponentTable())
#define SHIP_DESIGN_TABLE (*g_Archverse->ShipDesignTable())
#define NEWS_TABLE (*g_Archverse->NewsTable())
#define PROJECT_TABLE (*g_Archverse->ProjectTable())
#define EVENT_TABLE (*g_Archverse->EventTable())
#define FLEET_TABLE (*g_Archverse->FleetTable())
#define ACTION_TABLE (*g_Archverse->ActionTable())

#endif //ARCHVERSECORE_GAME_GAMEINFO