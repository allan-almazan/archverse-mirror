#ifndef ARCHVERSECORE_GAME_ACTION
#define ARCHVERSECORE_GAME_ACTION

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using namespace ArchverseCore::System;
		using namespace json_spirit;
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;

		class ActionList;
		class ActionTable;
		class ActionStorage_BDB;
		class Player;

		class Action
		{
		public:
			enum ActionType
			{
				AT_ENTERED_ALLIANCE,
				AT_ENTERED_WAR,
				AT_ENTERED_PEACE_TREATY,
				AT_BROKE_ALLIANCE,
				AT_BROKE_PEACE_TREATY,
				AT_LOST_LARGE_BATTLE,
				AT_WON_LARGE_BATTLE,
				AT_HAS_EXPEDITION,
				AT_WAITING = 1 << 31
			};
			
		private:
			friend class ActionList;
			friend class ActionListPresentation_JSON;

			ActionType _actionType;
			uint32_t _turn;
			uint32_t _data;
			
			bool Interact(Player& player, int input);
			string ActionString() const;

		};

		class ActionList
		{
			friend class ActionTable;
			friend class ActionStorage_BDB;
		private:
			uint32_t _id;
			vector<Action> _actions;

			void PushAction(const Action& action);

		public:
			void ClearOldActions();
			bool HasAction(Action::ActionType actionType);
			void AddAction(Action::ActionType actionType, uint32_t turn, uint32_t data);
			void FindActions(Action::ActionType actionType, vector<Action>& actions);
			void InteractWithActions(Action::ActionType actionType, int input, Player& player);
		};

		class ActionTable : public DBCore<ActionList, ActionStorage_BDB>
		{
		public:
			bool ActionById(uint32_t actionId, ActionList& destination);
		};
		class ActionListPresentation_JSON
		{

		public:
			static void ActionsOverview(vector<Action>& actions, Object& destination);
		};
		class ActionStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const ActionList& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, ActionList& destination);
		};
	}
}
#endif //ARCHVERSECORE_GAME_ACTION