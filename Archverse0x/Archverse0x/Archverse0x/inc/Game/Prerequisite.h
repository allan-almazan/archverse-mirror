#ifndef ARCHVERSECORE_GAME_PREREQUISITE
#define ARCHVERSECORE_GAME_PREREQUISITE

#include"System\DBCore.h"
#include"System\JSON\json_spirit.h"
#include"Game\ControlModel.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using std::vector;
		using std::string;
		using boost::uint8_t;

		class Player;
		class PrerequisiteList;
		class PrerequisiteTable;
		class PrerequisitePresentation_JSON;
		class Tech;

		class Prerequisite
		{
			friend class Player;
			friend class PrerequisiteList;
			friend class PrerequisiteTable;
			friend class PrerequisitePresentation_JSON;
		public:
			enum RequisiteType
			{
				RT_RACE = 0,
				RT_TERRITORY_COUNT,
				RT_TECH,
				RT_TECH_COUNT,
				RT_BUILDING_COUNT,
				RT_POWER,
				RT_RANK,
				RT_REGION,
				RT_COMMANDER_LEVEL,
				RT_FLEET,
				RT_COUNCIL_SIZE,
				RT_RP,
				RT_MP,
				RT_KP,
				RT_PP,
				RT_HAS_SHIP,
				RT_SHIP_POOL,
				RT_POPULATION,
				RT_GOVERNMENT_MODE,
				RT_TITLE,
				RT_COUNCIL_SPEAKER,
				RT_COUNCIL_WAR,
				RT_WAR_IN_COUNCIL,
				RT_VALOR,

				CM_POPULATION_GROWTH,
				CM_POPULATION_DENSITY,
				CM_LIFE_TECH_RESEARCH,
				CM_SOCIAL_TECH_RESEARCH,
				CM_INFO_TECH_RESEARCH,
				CM_MATTER_ENERGY_TECH_RESEARCH,
				CM_RACIAL_TECH_RESEARCH,
				CM_RESEARCH,
				CM_COMMAND_AND_CONTROL,
				CM_SHIP_PRODUCTION,
				CM_SHIP_STRENGTH,
				CM_MORALE,
				CM_SURVIVAL,
				CM_BERZERKER,
				CM_ADMIRALTY,
				CM_MINING,
				CM_PRODUCTION,
				CM_EFFICIANCY,
				CM_COMMERCE,
				CM_DIPLOMACY
			};

			enum RequisiteOperator
			{
				RO_GREATER = 0,
				RO_LESS,
				RO_EQUAL,
				RO_GREATER_EQUAL,
				RO_LESS_EQUAL,
				RO_NOT
			};
			enum RequisiteBoolOperator
			{
				RBO_NONE = 0,
				RBO_OR_ARG,
				RBO_OR_REQUISITE,
				RBO_AND_ARG
			};
		private:
			uint32_t _id;
			RequisiteType _type;
			RequisiteOperator _operator;
			uint32_t _argument;
			RequisiteBoolOperator _boolOperator;
			uint32_t _boolArgument;
			string _description;
		public:
			bool evaluate(Player& player);
			//bool evaluate(ShipDesign& shipDesign);

		};
		class PrerequisiteList
		{
			friend class PrerequisitePresentation_JSON;
			vector<uint32_t>& _prerequisites;
		public:
			PrerequisiteList(vector<uint32_t>& prerequisites) : _prerequisites(prerequisites) {}
		};
		class PrerequisiteTable
		{
		private:
			vector<Prerequisite> _prerequisiteList;
		public:
			void AddPrerequisite(Prerequisite& prerequisite);
			Prerequisite& PrerequisiteFromId(uint32_t prerequisiteId);

			uint32_t TechRequisite(Tech& requirement);
		};
		class PrerequisitePresentation_JSON
		{
		public:
			static void PrerequisiteOverview(Prerequisite& prerequisite, Object& destination);
			static void DesignPrerequisiteListOverview(PrerequisiteList& prerequisiteList, Object& destination);
			static void PrerequisiteListOverview(PrerequisiteList& prerequisiteList, Object& destination);
		};
		
	}
}

#endif //ARCHVERSECORE_GAME_PREREQUISITE