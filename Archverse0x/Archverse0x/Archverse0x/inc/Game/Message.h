#ifndef ARCHVERSECORE_GAME_MESSAGE
#define ARCHVERSECORE_GAME_MESSAGE

#include"System\DBCore.h"
#include"System\JSON\json_spirit.h"
#include"Game\Relationship.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>
#include <boost\foreach.hpp>
#include <exception>
#include <boost\lexical_cast.hpp>
#include <algorithm>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using std::exception;
		using boost::lexical_cast;
		using namespace json_spirit;
		using ArchverseCore::System::DBCore;
		class MessagePresentation_JSON;
		class MessageStorage_BDB;
		class MessageTable;
		template<typename T, typename TTABLE>
		class MessageBox;


		enum MessageStatus
		{
			MS_UNREAD,
			MS_READ,
			MS_REPLIED
		};
		enum MessageType
		{
			MT_NORMAL,
			MT_DECLARE_WAR,
			MT_DECLARE_TOTAL_WAR,
			MT_DECLARE_HOSTILITIES,
			MT_CANCEL_ALLIANCE,
			MT_CANCEL_TRADE,
			MT_OFFER_TRUCE,
			MT_OFFER_TRADE,
			MT_OFFER_ALLIANCE,
			MT_OFFER_SUBORDINATE,
			MT_REJECT_OFFER

		};

		class DiplomaticMessage
		{
		private:
			friend class MessagePresentation_JSON;
			friend class MessageStorage_BDB;
			friend class MessageTable;
			template<typename T, typename TTABLE>
			friend class MessageBox;

			uint32_t _id;
			uint32_t _senderId;
			uint32_t _targetId;
			MessageStatus _status;
			MessageType _type;
			string _title;
			string _body;
		public:
			
		};
		class MessageStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const DiplomaticMessage& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, DiplomaticMessage& destination);
			//static int (*)(Db *, const Dbt *, const Dbt *, Dbt *) Callback() { return NULL;}
		};

		class MessageTable : public DBCore<DiplomaticMessage, MessageStorage_BDB>
		{
		public:
			MessageTable(const string& tableName) : DBCore<DiplomaticMessage, MessageStorage_BDB>(tableName) {}
			bool MessageFromId(uint32_t messageId, DiplomaticMessage& destination);
			bool SaveMessage(const DiplomaticMessage& source);
			void CreateMessage(uint32_t senderId, uint32_t targetId, MessageType type, 
				const string& title, const string& body, DiplomaticMessage& destination);
		};
		template<typename T, typename TTABLE>
		class MessageBox
		{
		private:
			vector<uint32_t>& _inbox;
			vector<uint32_t>& _sent;
			vector<uint32_t>& Inbox() {return _inbox;}
			vector<uint32_t>& Sent() {return _sent;}
			MessageTable& _table;
			TTABLE& _ttable;

		public:
			MessageBox(vector<uint32_t>& inbox, vector<uint32_t>& sent, MessageTable& table, TTABLE& ttable) :
			  _inbox(inbox), _sent(sent), _table(table), _ttable(ttable){}
			static void SendMessage(MessageTable& table, TTABLE& ttable, RelationTable& rTable, DiplomaticMessage& message)
			{
				T sendingPlayer, recivingPlayer;
				if(ttable.Exists(message._senderId) &&
					ttable.Exists(message._targetId))
				{
					class PushMessage
					{
					private:
						uint32_t _messageId;
						vector<uint32_t>& (MessageBox<T, TTABLE>::*_target)();
					public:
						PushMessage(uint32_t messageId, vector<uint32_t>& (MessageBox<T, TTABLE>::*target)()) : _messageId(messageId), _target(target) {}
						bool operator()(T& t) const
						{
							(t.Messages().*_target)().push_back(_messageId);
							return true;
						}
					};
					table.SaveMessage(message);
					ttable.LockedTransaction(message._senderId, PushMessage(message._id, &MessageBox<T, TTABLE>::Sent));
					ttable.LockedTransaction(message._targetId, PushMessage(message._id, &MessageBox<T, TTABLE>::Inbox));
					if(message._type != MT_NORMAL)
					{
						Relationship relationship;
						Relation relation;
						switch(message._type)
						{
						case MT_DECLARE_WAR:
							relation = RELATION_WAR;
							break;
						case MT_DECLARE_TOTAL_WAR:
							relation = RELATION_TOTAL_WAR;
							break;
						case MT_DECLARE_HOSTILITIES:
							relation = RELATION_HOSTILE;
							break;
						case MT_CANCEL_ALLIANCE:
							relation = RELATION_NONE;
							break;
						case MT_CANCEL_TRADE:
							relation = RELATION_NONE;
							break;
						}
						switch(message._type)
						{
						case MT_DECLARE_WAR:
						case MT_DECLARE_TOTAL_WAR:
						case MT_DECLARE_HOSTILITIES:
						case MT_CANCEL_ALLIANCE:
						case MT_CANCEL_TRADE:

							//rTable.CreateRelationship(message._senderId, message._targetId, relation, relationship);
							T t;
							if(ttable.LoadElement(message._senderId, t) == 0)
							{
								t.Relations().RelationTo(message._targetId, true, relationship);
								ttable.LockedSetterUpdate(message._senderId, T::DiplomaticStateSetter(relation, true), relationship.Id());
								ttable.LockedSetterUpdate(message._targetId, T::DiplomaticStateSetter(relation, false), relationship.Id());
							}
						}

					}


				}
			}
			bool ReadMessage(DiplomaticMessage& destination)
			{
				using std::find;
				if(find(_sent.begin(), _sent.end(), destination._senderId) != _sent.end())
				{
					_table.LockedFieldUpdate(destination._id, &DiplomaticMessage::_status, MS_READ);
					return true;
				}
				else if(find(_inbox.begin(), _inbox.end(), destination._senderId) == _inbox.end())
				{
					return true;
				}
				return false;
			}
			void NewMessages(vector<DiplomaticMessage>& messages)
			{
				DiplomaticMessage tmpMessage;
				BOOST_FOREACH(uint32_t messageId, _inbox)
				{
					tmpMessage = DiplomaticMessage();
					if(_table.MessageFromId(messageId, tmpMessage) && 
						tmpMessage._status == MS_UNREAD)
					{
						messages.push_back(tmpMessage);
					}
				}
			}
			void SentMessages(vector<DiplomaticMessage>& messages)
			{
				messages.resize(_sent.size(), DiplomaticMessage());
				for (size_t i = 0; i < _sent.size(); i++)
				{
					if(!_table.MessageFromId(_sent[i], messages[i]))
						throw exception("bad message id detected");
				}
			}
			void RecivedMessages(vector<DiplomaticMessage>& messages)
			{
				messages.resize(_inbox.size(), DiplomaticMessage());
				for (size_t i = 0; i < _inbox.size(); i++)
				{
					if(!_table.MessageFromId(_inbox[i], messages[i]))
						throw exception("bad message id detected");
				}
			}
			void AcceptOffer(uint32_t messageId, RelationTable& relationTable)
			{
				if(find(_inbox.begin(), _inbox.end(), messageId) != _inbox.end())
				{
					DiplomaticMessage message;
					if(_table.MessageFromId(messageId, message))
					{
						if(message._type != MT_NORMAL)
						{
							Relationship relationship;
							Relation relation;
							switch(message._type)
							{
							case MT_OFFER_TRUCE:
								relation = RELATION_TRUCE;
								break;
							case MT_OFFER_TRADE:
								relation = RELATION_TRADE;
								break;
							case MT_OFFER_ALLIANCE:
								relation = RELATION_ALLY;
								break;
							case MT_OFFER_SUBORDINATE:
								relation = RELATION_SUBORDINARY;
								break;
							}
							relationTable.CreateRelationship(message._senderId, message._targetId, relation, relationship);
							_ttable.LockedSetterUpdate(message._senderId, T::DiplomaticStateSetter(relation, false), relationship.Id());
							_ttable.LockedSetterUpdate(message._targetId, T::DiplomaticStateSetter(relation, true), relationship.Id());
							return;
						}
					}
				}
				//fall through case
				throw exception("Inaccessible messageId");
				
			}
		};	
		class MessagePresentation_JSON
		{
		public:
			template<class T, class TTABLE>
			static void NewMessageSummary(MessageTable& table, MessageBox<T, TTABLE>& messageBox, Object& destination)
			{
				vector<DiplomaticMessage> newMessages;
				messageBox.NewMessages(newMessages);
				destination.push_back(Pair("Messages", Array()));
				Array& messageList = destination[0].value_.get_array();
				messageList.resize(newMessages.size(), Object());
				for(size_t i = 0; i < newMessages.size(); i++)
				{
					Object& messageListElement = messageList[i].get_obj();
					DiplomaticMessage& newMessageElement = newMessages[i];
					messageListElement.push_back(Pair("MessageId", lexical_cast<string>(newMessageElement._id)));
					messageListElement.push_back(Pair("SenderId", lexical_cast<string>(newMessageElement._senderId)));
					messageListElement.push_back(Pair("TargetId", lexical_cast<string>(newMessageElement._targetId)));
					messageListElement.push_back(Pair("Status", lexical_cast<string>(newMessageElement._status)));
					messageListElement.push_back(Pair("Title", newMessageElement._title));
				}
			}
			template<class T, class TTABLE>
			static void SentMessages(MessageTable& table, MessageBox<T, TTABLE>& messageBox, Object& destination)
			{
				vector<DiplomaticMessage> sentMessages;
				messageBox.SentMessages(sentMessages);
				destination.push_back(Pair("Messages", Array()));
				Array& messageList = destination[0].value_.get_array();
				messageList.resize(sentMessages.size(), Object());
				for(size_t i = 0; i < sentMessages.size(); i++)
				{
					Object& messageListElement = messageList[i].get_obj();
					DiplomaticMessage& sentMessageElement = sentMessages[i];
					messageListElement.push_back(Pair("MessageId", lexical_cast<string>(sentMessageElement._id)));
					messageListElement.push_back(Pair("SenderId", lexical_cast<string>(sentMessageElement._senderId)));
					messageListElement.push_back(Pair("TargetId", lexical_cast<string>(sentMessageElement._targetId)));
					messageListElement.push_back(Pair("Status", lexical_cast<string>(sentMessageElement._status)));
					messageListElement.push_back(Pair("Title", sentMessageElement._title));
				}
			}
			template<class T, class TTABLE>
			static void RecivedMessages(MessageTable& table, MessageBox<T, TTABLE>& messageBox, Object& destination)
			{
				vector<DiplomaticMessage> recivedMessages;
				messageBox.RecivedMessages(recivedMessages);
				destination.push_back(Pair("Messages", Array()));
				Array& messageList = destination[0].value_.get_array();
				messageList.resize(recivedMessages.size(), Object());
				for(size_t i = 0; i < recivedMessages.size(); i++)
				{
					Object& messageListElement = messageList[i].get_obj();
					DiplomaticMessage& recivedMessageElement = recivedMessages[i];
					messageListElement.push_back(Pair("MessageId", lexical_cast<string>(recivedMessageElement._id)));
					messageListElement.push_back(Pair("SenderId", lexical_cast<string>(recivedMessageElement._senderId)));
					messageListElement.push_back(Pair("TargetId", lexical_cast<string>(recivedMessageElement._targetId)));
					messageListElement.push_back(Pair("Status", lexical_cast<string>(recivedMessageElement._status)));
					messageListElement.push_back(Pair("Title", recivedMessageElement._title));
				}
			}
			template<class T, class TTABLE>
			static void ReadMessage(MessageTable& table, uint32_t messageId, MessageBox<T, TTABLE>& messageBox, Object& destination)
			{
				DiplomaticMessage targetMessage;
				if(table.MessageFromId(messageId, targetMessage) &&
					messageBox.ReadMessage(targetMessage))
				{
					destination.push_back(Pair("MessageId", lexical_cast<string>(targetMessage._id)));
					destination.push_back(Pair("SenderId", lexical_cast<string>(targetMessage._senderId)));
					destination.push_back(Pair("TargetId", lexical_cast<string>(targetMessage._targetId)));
					destination.push_back(Pair("Status", lexical_cast<string>(targetMessage._status)));
					destination.push_back(Pair("Type", lexical_cast<string>(targetMessage._type)));
					destination.push_back(Pair("Title", targetMessage._title));
					destination.push_back(Pair("Body", targetMessage._body));
				}

			}
		};
	}
}


#endif //ARCHVERSECORE_GAME_REGION