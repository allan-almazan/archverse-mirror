#ifndef ARCHVERSECORE_GAME_GAMEINFO
#define ARCHVERSECORE_GAME_GAMEINFO

#include "System\DBCore.h"
#include "Game\Council.h"

#include <boost\cstdint.hpp>
#include <vector>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using ArchverseCore::System::DBCore;
		//typedef int (*bdbCallback)(Db *, const Dbt *, const Dbt *, Dbt *);

		class GameInfo;

		class GameInfoData
		{
		public:
			GameInfoData()
			{
				_nextAdmissionId = _nextCouncilId = _nextRelationId = _nextMessageId = _nextRegionId = _nextTerritoryId = _nextPlayerId = 0;
				_running = true;
			}
			friend class GameInfo;
		private:
			uint32_t _nextPlayerId;
			uint32_t _tick;
			bool _running;
			uint32_t _nextTerritoryId;
			uint32_t _nextRegionId;
			uint32_t _nextMessageId;
			uint32_t _nextRelationId;
			uint32_t _nextCouncilId;
			uint32_t _nextAdmissionId;
			uint32_t _nextNewsId;
			vector<uint32_t> _councils;
		};

		class GameInfo : public DBCore<GameInfoData, GameInfo>
		{
			
		private:
			GameInfoData gid;
		public:
			GameInfo()
			{
				
			}
			uint32_t IncrementPlayerID();
			uint32_t IncrementTerritoryID();
			uint32_t IncrementRegionID();
			uint32_t IncrementMessageID();
			uint32_t IncrementRelationID();
			uint32_t IncrementCouncilID();
			uint32_t IncrementAdmissionID();
			uint32_t IncrementNewsID();
			uint32_t Tick();
			bool Running();
			void Running(bool running);
			void AddTicks(uint32_t ticks);
			uint32_t RegionCount();
			CouncilList Councils();
			GameInfoData& GID() {return gid;}
			static CouncilList Councils(GameInfoData& gameInfo);
			static bool serialize(vector<uint8_t>& destination, const GameInfoData& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, GameInfoData& destination);
			//static bdbCallBack Callback() { return NULL;}
		};
	}
}


#endif //ARCHVERSECORE_GAME_GAMEINFO