#ifndef ARCHVERSECORE_GAME_USERACCOUNT_H
#define ARCHVERSECORE_GAME_USERACCOUNT_H

#include "System/DBCore.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using ArchverseCore::System::DBCore;

		class UserAccountTable;
		class UserAccount
		{
			friend class UserAccountTable;
		private:
			uint32_t _playerId;
			string _userName;
			string _password;
			string _lastGeneratedAuthString;
		public:
			UserAccount() {_playerId = 0;};
			uint32_t PlayerId() {return _playerId;}
			void PlayerId(const uint32_t& id) {_playerId = id;}
			const string& UserName() { return _userName;}
			bool Login(const string& password, string& authString);
			static bool serialize(vector<uint8_t>& destination, const UserAccount& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, UserAccount& destination);
			//static bdbCallback Callback() {return NULL;}
		};
		class UserAccountTable : public DBCore<UserAccount, UserAccount>
		{
		private:
			bool LoadUserAccount(const string& userName, UserAccount& destination);
		public:
			bool UserAccountFromAuthString(const string& authString, UserAccount& destination);
			bool UserAccountFromName(const string& authString, UserAccount& destination);
			bool Login(const string& username, const string& password, string& authString, UserAccount& destination);
			bool CreateUser(const string& userName, const string& password);
			bool SaveUser(const UserAccount& userAccount);
		};
	}
}

#endif //ARCHVERSECORE_GAME_USERACCOUNT_H