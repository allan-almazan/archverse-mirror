#ifndef ARCHVERSECORE_GAME_SHIPDESIGN
#define ARCHVERSECORE_GAME_SHIPDESIGN

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using namespace json_spirit;
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;
		using namespace ArchverseCore::System;

		class Ship;
		class ShipDesignTable;
		class ShipDesignStorage_BDB;
		class ShipDesignPresentation_JSON;
		class ShipDesignList;


		class ShipDesign
		{
			friend class ShipDesignTable;
			friend class ShipDesignStorage_BDB;
			friend class ShipDesignPresentation_JSON;
			friend class ShipDesignList;

		private:
			uint32_t _id;
			uint32_t _ownerId;
			uint32_t _hullId;
			uint32_t _shieldId;
			uint32_t _computerId;
			uint32_t _armorId;
			uint32_t _currentPool;
			uint32_t _cost;
			string _name;
			vector<uint32_t> _weapons;
			vector<uint32_t> _devices;
		public:
			uint32_t Id() const {return _id;}
			uint32_t OwnerId() const {return _ownerId;}
			const string& Name() const {return _name;}
			uint32_t Cost() const {return _cost;}
			
		};

		class ShipDesignTable : public DBCore<ShipDesign, ShipDesignStorage_BDB>
		{
		public:
			ShipDesignTable() : DBCore<ShipDesign, ShipDesignStorage_BDB>() {}
			bool ShipDesignFromId(uint32_t id, ShipDesign& destination);
		};

		class ShipDesignStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const ShipDesign& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, ShipDesign& destination);
		
		};

		class ShipDesignList
		{
			friend class ShipDesignPresentation_JSON;
		private:
			vector<uint32_t>& _designs;
		public:
			ShipDesignList(vector<uint32_t>& designs) : _designs(designs) {}
		};

		class ShipDesignPresentation_JSON
		{
		public:
			static void ShipDesignOverview(ShipDesign& design, Object& destination);
			static void ShipDesignListOverview(ShipDesignList& designList, Object& destination);
		};


	}
}

#endif //ARCHVERSECORE_GAME_SHIPDESIGN