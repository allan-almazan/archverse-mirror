#ifndef ARCHVERSECORE_GAME_REGION
#define ARCHVERSECORE_GAME_REGION

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using ArchverseCore::System::DBCore;
		class RegionPresentation_JSON;
		class RegionStorage_BDB;
		class RegionTable;
		class TerritoryList;

		class Region
		{
		private:
			friend class RegionPresentation_JSON;
			friend class RegionStorage_BDB;
			friend class RegionTable;
			uint32_t _id;
			string _name;
			vector<uint32_t> _territories;

		public:
			uint32_t Id() {return _id;}
			string& Name() {return _name;}
			string NameForNewTerritory();
			TerritoryList Territories();
		};
		class RegionStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const Region& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, Region& destination);
			//static int (*)(Db *, const Dbt *, const Dbt *, Dbt *) Callback() { return NULL;}
		};
		class RegionTable : public DBCore<Region, RegionStorage_BDB>
		{
		private:
			static const uint32_t STARTING_REGIONS = 10;
		public:
			RegionTable();
			bool RegionFromId(uint32_t regionId, Region& destination);
			bool RandomRegionForPlacement(Region& destination);
		};
		class RegionPresentation_JSON
		{
			
		};
	}
}


#endif //ARCHVERSECORE_GAME_REGION