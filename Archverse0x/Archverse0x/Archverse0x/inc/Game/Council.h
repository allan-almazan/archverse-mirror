#ifndef ARCHVERSECORE_GAME_COUNCIL
#define ARCHVERSECORE_GAME_COUNCIL

#include"System\DBCore.h"
#include"Game\Relationship.h"
#include"Game\Message.h"
#include "Game\Archverse.h"
#include"System\JSON\json_spirit.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using namespace json_spirit;
		using ArchverseCore::System::DBCore;

		class CouncilTable;
		class CouncilStorage_BDB;
		class CouncilPresentation_JSON;
		class PlayerList;
		class Player;
		class AdmissionRequestList;
		class AdmissionRequest;
		struct ControlModel;

		class Council
		{
			friend class CouncilTable;
			friend class CouncilStorage_BDB;
			friend class CouncilPresentation_JSON;
		private:
			uint32_t _id;
			uint32_t _speakerId;
			uint32_t _production;
			uint32_t _power;
			bool _acceptingNewPlayers;

			string _name;
			string _motto;
			vector<uint32_t> _actions;
			vector<uint32_t> _players; 
			vector<uint32_t> _sentMessages;
			vector<uint32_t> _recivedMessages;
			vector<uint32_t> _relations;
			vector<uint32_t> _admissionRequests;

			static const uint32_t MAX_COUNCIL_SIZE = 20;
			void EnterTruce(const uint32_t& relationId) {}
			void EnterAlliance(const uint32_t& relationId) {}
			void EnterTrade(const uint32_t& relationId) {}
			void EnterWar(const uint32_t& relationId) {}
			void DeclareWar(const uint32_t& relationId) {}
			void EnterTotalWar(const uint32_t& relationId) {}
			void DeclareTotalWar(const uint32_t& relationId) {}
			void DominateCouncil(const uint32_t& relationId) {}
			void EnterSubordinary(const uint32_t& relationId) {}
			void ClearRelation(const uint32_t& relationId) {}
		public:
			MessageBox<Council, CouncilTable> Messages();
			RelationList Relations();
			PlayerList Players();
			AdmissionRequestList PlayerAdmissionRequests();
			uint32_t Id() { return _id; }
			uint32_t SpeakerId() {return _speakerId; }
			static void (Council::*DiplomaticStateSetter(Relation relation, bool initiate))(const uint32_t& relationId);
			void AdmisionRequest(uint32_t playerId, const string& messageBody);
			void RemovePlayer(const uint32_t& playerId);
			void Motto(const string& motto) { _motto = motto; }
			void Name(const string& name) { _name = name; }
			const string& Name() const { return _name; }
			void Update(ControlModel& councilControlModel, uint32_t turn);
		};

		class CouncilTable : public DBCore<Council, CouncilStorage_BDB>
		{
		private:
			void AddCouncil(Council& council);
		public:
			bool CouncilById(uint32_t councilId, Council& destination);
			void CreateNewCouncil(const string& name, const string& motto, uint32_t founderId, Council& destination);
			void CreateNewCouncil(Council& destination);
			void RandomCouncil(Council& destination);
		};
		class CouncilStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const Council& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, Council& destination);
		};
		using json_spirit::Object;
		class CouncilPresentation_JSON
		{
		public:
			static void CouncilOverview(Council& council, Object& destination);
			static void CouncilMinimalOverview(Council& council, Object& destination);
			static void CouncilMemberDiplomaticStatus(Council& council, Player& relation, Object& destination);
		};

		class CouncilList
		{
			friend class CouncilTable;
		private:
			vector<uint32_t>& _councils;
		public:
			CouncilList(vector<uint32_t>& councils) : _councils(councils) {}
			template<typename F>
			void ForeachWrite(F& f)
			{
				BOOST_FOREACH(uint32_t elementId, _councils)
				{
					COUNCIL_TABLE.LockedTransaction(elementId, f);
				}
			}
		};
	}
}

#endif //ARCHVERSECORE_GAME_COUNCIL