#ifndef ARCHVERSECORE_GAME_PLAYER
#define ARCHVERSECORE_GAME_PLAYER

#include "System\JSON\json_spirit.h"
#include "System\DBCore.h"
#include "Game\Relationship.h"
#include "Game\Message.h"
#include "Game\ControlModel.h"
#include "Game\Prerequisite.h"
#include "Game\Action.h"
#include "Game\Archverse.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>
#include<time.h>


namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using std::pair;
		using ArchverseCore::System::DBCore;
		class PlayerStorage_BDB;
		class PlayerPresentation_JSON;
		class PlayerTable;
		class PlayerList;
		class TerritoryList;
		class UserAccount;
		class Council;
		class TechList;
		class NewsList;
		class EventList;
		class ActionList;

		class Player
		{
			friend class PlayerPresentation_JSON;
			friend class PlayerStorage_BDB;
			friend class PlayerTable;
			friend class PlayerList;
		private:
			
			uint32_t _id;
			uint32_t _userAccount;
			uint32_t _valor;
			uint32_t _turn;
			uint32_t _tickPosition;
			uint32_t _currentMP; //Military Points
			uint32_t _currentKP; //Knowledge Points
			uint32_t _currentPP; //Production Points
			uint32_t _currentRP; //Resource Points
			uint32_t _lastTurnMP;
			uint32_t _lastTurnKP;
			uint32_t _lastTurnPP;
			uint32_t _lastTurnRP;
			uint32_t _council;
			uint32_t _government;
			uint32_t _race;
			uint32_t _techGoal;
			uint32_t _rank;
			uint32_t _councilVote;
			uint32_t _power;
			uint32_t _votePower;
			uint32_t _population;
			uint32_t _actionsId;
			uint32_t _targetSecurityLevel;
			uint32_t _currentSecurityLevel;
			bool _blockaded;
			time_t _blockTime;
			ControlModel _controlModel;
			string _name;
			
			vector<uint32_t> _abilities;
			vector<uint32_t> _admirals;
			vector<uint32_t> _battlePlans;
			vector<uint32_t> _events;
			vector<uint32_t> _news;
			vector<uint32_t> _territories;
			vector<uint32_t> _relations;
			vector<uint32_t> _shipDesigns;
			vector<uint32_t> _knownTechs;
			vector<uint32_t> _sentMessages;
			vector<uint32_t> _recivedMessages;

			void EnterTruce(const uint32_t& relationId) {}
			void EnterAlliance(const uint32_t& relationId) {}
			void EnterTrade(const uint32_t& relationId) {}
			void EnterWar(const uint32_t& relationId) {}
			void DeclareWar(const uint32_t& relationId) {}
			void EnterHostilities(const uint32_t& relationId) {}
			void DeclareHostilities(const uint32_t& relationId) {}
			void ClearRelation(const uint32_t& relationId) {}

		public:
			Player() {memset(reinterpret_cast<void*>(&_id), 0, reinterpret_cast<int*>(&_name) - reinterpret_cast<int*>(&_id));}
			TerritoryList Territories();
			MessageBox<Player, PlayerTable> Messages();
			RelationList Relations();
			TechList KnownTechs();
			NewsList CurrentNews();
			uint32_t Id() const { return _id; }
			uint32_t CouncilId() const { return _council; }
			const string& Name() const { return _name; }
			uint32_t Turn() const {return _turn;}
			void JoinCouncil(const uint32_t& councilId);
			static void (Player::*DiplomaticStateSetter(Relation relation, bool initiate))(const uint32_t& relationId);
			void CouncilVote(const uint32_t& targetId) { _councilVote = targetId; }
			uint32_t CouncilVote() { return _councilVote; }
			uint32_t TechGoal() { return _techGoal; }
			uint32_t ValueForPrerequisite(Prerequisite::RequisiteType reqType);
			bool Update(Council& council, ControlModel& councilControlModel, uint32_t tick);
			const ControlModel& ControlModel() const {return _controlModel;}
			void FinishTerritoryUpdate(uint32_t pp, uint32_t rp, uint32_t kp, uint32_t mp, uint32_t population, uint32_t power);
			EventList Events();
			void TargetSecurityLevel(uint32_t level) { _targetSecurityLevel = level; }
			void Actions(ActionList& destination);
			void ExpeditionTerritory(int luckRoll);
		};

		class PlayerStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const Player& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, Player& destination);
			//static bdbCallback Callback() {return NULL;}
		};

		class PlayerTable : public DBCore<Player, PlayerStorage_BDB>
		{	
		public:
			bool PlayerById(uint32_t playerId, Player& destination);
			bool PlayerByAccount(const string& accountName, Player& destination);
			bool PlayerByName(const string& name, Player& destination);
			bool SavePlayer(const Player& source);
			bool RemovePlayer(uint32_t playerId);
			bool PlayerByAuthString(const string& authString, Player& destination);
			bool CreatePlayer(UserAccount& user, const string& playerName, const uint32_t raceId, Player& destination); 

		};

		using json_spirit::Object;
		class PlayerPresentation_JSON
		{
		public:
			static void PlayerOverview(Player& player, Object& destination);
			static void PlayerOverview(Player& player, const Council& council, Object& destination);
			static void PlayerDetail(Player& player, Object& destination);
			static void PlayerPrivateInfoDump(Player& player, Object& destination);
			static void PlayerDiplomaticStatus(Player& player, const Player& relation, Object& destination);
		};
		class PlayerList
		{
		private:
			vector<uint32_t>& _players;
		public:
			PlayerList(vector<uint32_t>& players) : _players(players) {}
			bool AddPlayer(Player& source);
			void AddPlayer(const uint32_t& source);
			bool RemovePlayer(uint32_t playerId);
			bool ContainsPlayer(uint32_t playerId);
			template<typename F>
			void ForeachWrite(F& f)
			{
				BOOST_FOREACH(uint32_t elementId, _players)
				{
					PLAYER_TABLE.LockedTransaction(elementId, f);
				}
			}
		};
	}
}

#endif //ARCHVERSECORE_GAME_PLAYER