#ifndef ARCHVERSECORE_GAME_SHIP
#define ARCHVERSECORE_GAME_SHIP

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using namespace json_spirit;
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;

		class ShipPool;
		

		class Ship
		{
		private:
			uint32_t _designId;
			float _damagePct; //0-100;
			uint32_t _crew;
			uint32_t _experiance;
		};

		class ShipPool
		{


		};
	}
}

#endif //ARCHVERSECORE_GAME_SHIP