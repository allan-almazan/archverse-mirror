#ifndef ARCHVERSECORE_GAME_PROJECT
#define ARCHVERSECORE_GAME_PROJECT


#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<vector>
#include<list>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using namespace json_spirit;
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;
		using std::list;

		class ProjectList;
		class ProjectTable;
		class ProjectStorage_BDB;
		class ProjectPresentation_JSON;

		class Council;
		class Player;
		struct ControlModel;
		class Territory;
		class PrerequisiteList;

		class Project
		{
			friend class ProjectList;
			friend class ProjectTable;
			friend class ProjectStorage_BDB;
			friend class ProjectPresentation_JSON;
		private:
			uint32_t _id;
			uint32_t _rpCost;
			uint32_t _ppCost;
			string _name;
			string _description;
			vector<uint32_t> _prerequisites;
		public:
			uint32_t Id() const {return _id;}
			PrerequisiteList Prerequisites();
			const string& Name() const {return _name;}
			void (*OnUpdateTurn)(Council& council, Player& player, Territory& territory, ControlModel& controlModel);
			//void OnUpdateTurn(Council& council, Player& player, Territory& territory, ControlModel& controlModel);
			//void CalcBattleEffects(StaticFleetEffectList& fleetEffects);
		};

		class ProjectTable
		{
			
		private:
			list<Project> _projectStorage;
			vector<Project*> _projects;
		public:
			Project& ProjectFromId(uint32_t projectId);
			Project& CreateProject(uint32_t rpCost, uint32_t ppCost, const string& name, const string& description, const vector<uint32_t>& prerequisites);
		};

		class ProjectPresentation_JSON
		{
		public:
			static void ProjectOverview(Project& project, Object& destination);
			static void ProjectListOverview(ProjectList& projectList, Object& destination);
		};

		class ProjectList
		{
			friend class ProjectPresentation_JSON;
		private:
			vector<uint32_t>& _projects;
		public:
			ProjectList(vector<uint32_t>& projects) : _projects(projects) {}

		};
	}
}
#endif //ARCHVERSECORE_GAME_PROJECT