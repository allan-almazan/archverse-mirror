#ifndef ARCHVERSECORE_GAME_CONTROLMODEL
#define ARCHVERSECORE_GAME_CONTROLMODEL

#include"System\DBCore.h"
#include"System\JSON\json_spirit.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>
#include<boost/lexical_cast.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using json_spirit::Object;
		using std::string;
		using std::vector;

		class ControlModelPresentation_JSON;

		struct ControlModel
		{
			enum CMType
			{
				CM_POPULATION_GROWTH,
				CM_POPULATION_DENSITY,
				CM_LIFE_TECH_RESEARCH,
				CM_SOCIAL_TECH_RESEARCH,
				CM_INFO_TECH_RESEARCH,
				CM_MATTER_ENERGY_TECH_RESEARCH,
				CM_RACIAL_TECH_RESEARCH,
				CM_RESEARCH,
				CM_COMMAND_AND_CONTROL,
				CM_SHIP_PRODUCTION,
				CM_SHIP_STRENGTH,
				CM_MORALE,
				CM_SURVIVAL,
				CM_BERZERKER,
				CM_ADMIRALTY,
				CM_MINING,
				CM_PRODUCTION,
				CM_EFFICIANCY,
				CM_COMMERCE,
				CM_DIPLOMACY
			};
			friend class ControlModelPresentation_JSON;
		public:
			 uint32_t _populationGrowth;
			 uint32_t _populationDensity;
			 uint32_t _lifeTechResearch;
			 uint32_t _socialTechResearch;
			 uint32_t _infoTechResearch;
			 uint32_t _matterEnergyTechResearch;
			 uint32_t _racialTechResearch;
			 uint32_t _research;
			 uint32_t _commandAndControl;
			 uint32_t _shipProduction;
			 uint32_t _shipStrength;
			 uint32_t _morale;
			 uint32_t _survival;
			 uint32_t _berzerker;
			 uint32_t _admiralty;
			 uint32_t _mining;
			 uint32_t _production;
			 uint32_t _efficiency;
			 uint32_t _commerce;
			 uint32_t _diplomacy; //mitigates jump timers and rule based restrictions

			 ControlModel& operator+=(ControlModel& target);
		};

		class ControlModelPresentation_JSON
		{
		public:
			static void ControlModelDump(const ControlModel& controlModel, Object& destination);
		};
	}
}

#endif //ARCHVERSECORE_GAME_CONTROLMODEL