#ifndef ARCHVERSECORE_GAME_TERRITORY
#define ARCHVERSECORE_GAME_TERRITORY

#include "System\JSON\json_spirit.h"
#include "System\DBCore.h"
#include "Game\ControlModel.h"
#include "Game\Archverse.h"

#include<boost\cstdint.hpp>
#include<vector>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using ArchverseCore::System::DBCore;
		class TerritoryPresentation_JSON;
		class TerritoryStorage_BDB;
		class TerritoryTable;
		class TerritoryList;
		class Player;
		class Council;
		struct ControlModel;
		class PlanetBuildRatio;
		class ProjectList;

		class Territory
		{
		private:
			class PlanetBuildRatio
			{
			public:
				char Factory() const { return _factory; }
				void Factory(char val) { _factory = val; }
				char ResearchLab() const { return _researchLab; }
				void ResearchLab(char val) { _researchLab = val; }
				char MillitaryBase() const { return _millitaryBase; }
				void MillitaryBase(char val) { _millitaryBase = val; }
				char Mine() const { return _mine; }
				void Mine(char val) { _mine = val; }
				char FactoryLeftover() const { return _factoryLeftover; }
				void FactoryLeftover(char val) { _factoryLeftover = val; }
				char ResearchLabLeftover() const { return _researchLabLeftover; }
				void ResearchLabLeftover(char val) { _researchLabLeftover = val; }
				char MillitaryBaseLeftover() const { return _millitaryBaseLeftover; }
				void MillitaryBaseLeftover(char val) { _millitaryBaseLeftover = val; }
				char MineLeftover() const { return _mineLeftover; }
				void MineLeftover(char val) { _mineLeftover = val; }
				PlanetBuildRatio()
				{
					_factory = 0;	
					_researchLab = 0;	
					_millitaryBase = 0;	
					_mine = 0;

					_factoryLeftover = 0;
					_researchLabLeftover = 0;
					_millitaryBaseLeftover = 0;
					_mineLeftover = 0;
				}

			private:
				char _factory;	
				char _researchLab;	
				char _millitaryBase;	
				char _mine;

				char _factoryLeftover;
				char _researchLabLeftover;
				char _millitaryBaseLeftover;
				char _mineLeftover;
			};
		public:
			enum TerritoryAttribute
			{
				PA_LOST_TRABOTULIN_LIBRARY = 0,
				PA_COGNITION_AMPLIFIER,
				PA_MILITARY_STRONGHOLD,
				PA_ANCIENT_RUINS,
				PA_ARTIFACT,
				PA_MASSIVE_ARTIFACT,
				PA_ASTEROID,
				PA_MOON,
				PA_RADIATION,
				PA_SEVERE_RADIATION,
				PA_HOSTILE_MONSTER,
				PA_OBSTINATE_MICROBE,
				PA_BEAUTIFUL_LANDSCAPE,
				PA_BLACK_HOLE,
				PA_NEBULA,
				PA_DARK_NEBULA,
				PA_VOLCANIC_ACTIVITY,
				PA_INTENSE_VOLCANIC_ACTIVITY,
				PA_OCEAN,
				PA_IRREGULAR_CLIMATE,
				PA_MAJOR_SPACE_ROUTE,
				PA_MAJOR_SPACE_CROSSROUTE,
				PA_FRONTIER_AREA,
				PA_GRAVITY_CONTROLED,
				PA_SHIP_YARD,
				PA_MAINTENANCE_CENTER,
				PA_UNDERGROUND_CAVERNS,
				PA_RARE_ORE,
				PA_MOON_CLUSTER,
			};
			enum TerritorySize
			{
				SIZE_TINY = 0,
				SIZE_SMALL,
				SIZE_MEDIUM,
				SIZE_LARGE,
				SIZE_HUGE,
			};

			enum TerritoryResourceLevel
			{
				RESOURCE_ULTRA_POOR = 0,
				RESOURCE_POOR,
				RESOURCE_NORMAL,
				RESOURCE_RICH,
				RESOURCE_ULTRA_RICH
			};
			static const uint32_t STARTING_MILLITARY_BASES = 10;
			static const uint32_t STARTING_LABS = 10;
			static const uint32_t STARTING_MINES = 10;
			static const uint32_t STARTING_FACTORIES = 10;
			static const uint32_t BASE_WORKFORCE_FOR_BUILDING = 100;
			static const uint32_t BUILDING_CONSTRUCTION_COST = 100;

		private:
			friend class TerritoryPresentation_JSON;
			friend class TerritoryStorage_BDB;
			friend class TerritoryTable;
			friend class TerritoryList;
			uint32_t _id;
			uint32_t _ownerId;
			uint32_t _regionId;
			uint32_t _population;
			uint32_t _factories;
			uint32_t _mines;
			uint32_t _millitaryBases;
			uint32_t _researchLabs;
			uint32_t _power;
			uint32_t _targetProjectId;
			uint32_t _projectCompletion; // 0 - 100 pct
			PlanetBuildRatio _buildRatio;
			ControlModel _controlModel;
			TerritorySize _size;
			TerritoryResourceLevel _resource;
			string _name;
			vector<uint32_t> _attributes;
			vector<uint32_t> _projects;

			static string AttributeString(const TerritoryAttribute& attribute);
		public:
			const string& Name() const { return _name; }
			string SizeString() const;
			string ResourceString() const;
			string AttributesString() const;
			static uint32_t MaxPopulationForSize(TerritorySize territorySize);
			uint32_t OwnerId() const { return _ownerId; }
			void OwnerId(const uint32_t& id) { _ownerId = id; }
			uint32_t Id() const { return _id; }
			uint32_t RegionId() const { return _regionId; }
			void Update(Council& council, Player& player, uint32_t turn);
			uint32_t Raid(uint32_t raidPower); //returns plunder
			ProjectList Projects();
			void UpdateBuildRatio(char factories, char mines, char millitaryBases, char labs);
		};
		class TerritoryTable : public DBCore<Territory, TerritoryStorage_BDB>
		{
		public:
			bool TerritoryFromId(uint32_t territoryId, Territory& destination);
			bool CreateNewTerritory(Territory& destination, int luckRoll);
			bool CreateNewHomeTerritory(uint32_t raceId, Territory& destination);
			bool RemoveTerritory(uint32_t territoryId);
		};
		class TerritoryStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const Territory& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, Territory& destination);
			//static int (*)(Db *, const Dbt *, const Dbt *, Dbt *) Callback() {return NULL;}
		};
		class TerritoryList
		{
			friend class TerritoryPresentation_JSON;
		private:
			vector<uint32_t>& _territories;
		public:
			TerritoryList(vector<uint32_t>& territories) : _territories(territories) {}
			uint32_t TerritoryCountFromRegion(uint32_t regionId);
			uint32_t TerritoryPower();
			uint32_t TerritoryCount() {return _territories.size();}
			bool TerritoryFromId(uint32_t territoryId, Territory& destination);
			bool TerritoryFromOrder(uint32_t orderPosition, Territory& destination);
			void AddTerritory(Territory& target);
			bool RemoveTerritory(uint32_t territoryId);
			template<typename F>
			void ForeachWrite(F& f)
			{
				BOOST_FOREACH(uint32_t elementId, _territories)
				{
					TERRITORY_TABLE.LockedTransaction(elementId, f);
				}
			}
		};
		class TerritoryPresentation_JSON
		{
		public:
			static void PrivateTerritoryOverview(TerritoryList& territories, Object& destination);
			static void PublicTerritoryOverview(TerritoryList& territories, Object& destination);
			static void SiegeTerritoryList(TerritoryList& territories, Object& destination);
			static void BlockadeTerritoryList(TerritoryList& territories, Object& destination);
			static void RaidTerritoryList(TerritoryList& territories, Object& destination);
			static void TerritorySummary(Territory& territory, Object& destination);
			static void ProjectSummary(Territory& territory, Object& destination);
		};

		
	}
}


#endif //ARCHVERSECORE_GAME_TERRITORY