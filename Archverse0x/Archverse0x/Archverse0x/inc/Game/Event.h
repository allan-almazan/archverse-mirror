#ifndef ARCHVERSECORE_GAME_EVENT
#define ARCHVERSECORE_GAME_EVENT


#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<vector>
#include<list>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		using namespace json_spirit;
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;
		using std::list;

		class EventList;
		class EventTable;
		class EventStorage_BDB;
		class EventPresentation_JSON;

		class PrerequisiteList;
		class Council;
		class Player;
		class Territory;
		class ControlModel;

		class Event
		{
			friend class EventList;
			friend class EventTable;
			friend class EventStorage_BDB;
			friend class EventPresentation_JSON;
		private:
			uint32_t _id;
			string _name;
			string _description;
			vector<uint32_t> _prerequisites;
		public:
			uint32_t Id() const {return _id;}
			PrerequisiteList Prerequisites();
			const string& Name() const {return _name;}
			bool (*OnUpdateTurn)(Council& council, Player& player, ControlModel& controlModel);
		};

		class EventTable
		{

		private:
			list<Event> _eventStorage;
			vector<Event*> _events;
		public:
			Event& EventFromId(uint32_t projectId);
			Event& CreateEvent(const string& name, const string& description, const vector<uint32_t>& prerequisites);
		};

		class EventPresentation_JSON
		{
		public:
			static void EventOverview(Event& project, Object& destination);
			static void EventListOverview(EventList& projectList, Object& destination);
		};

		class EventList
		{
			friend class EventPresentation_JSON;
		private:
			vector<uint32_t>& _events;
		public:
			EventList(vector<uint32_t>& events) : _events(events) {}

		};
	}
}
#endif //ARCHVERSECORE_GAME_EVENT