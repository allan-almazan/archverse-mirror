#ifndef ARCHVERSECORE_GAME_ADMIRAL
#define ARCHVERSECORE_GAME_ADMIRAL

#include"System\JSON\json_spirit.h"
#include"System\DBCore.h"

#include<vector>
#include<string>
#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{

		using namespace json_spirit;
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;

		class AdmiralTable;
		class AdmiralList;
		class AdmiralPresentation_JSON;
		class AdmiralStorage_BDB;

		class Race;

		class Admiral
		{
			friend class AdmiralTable;
			friend class AdmiralList;
			friend class AdmiralPresentation_JSON;
			friend class AdmiralStorage_BDB;
		private:
			uint32_t _id;
			uint32_t _owner;
			uint32_t _race;
			uint32_t _level;
			uint32_t _experiance;
			string _name;
			vector<uint32_t> _attributes;
		};
		class AdmiralList
		{
			friend class AdmiralTable;
			friend class AdmiralStorage_BDB;
		private:
			vector<Admiral>& _admirals;

			void PushAdmiral(const Admiral& admiral);

		public:
			AdmiralList(vector<Admiral>& admirals) : _admirals(admirals) {}
		};

		class AdmiralTable : public DBCore<Admiral, AdmiralStorage_BDB>
		{
		private:
			void AddAdmiral(Council& council);
		public:
			bool AdmiralById(uint32_t admiralId, Admiral& destination);
			void CreateNewAdmiral(Race& admiralRace, Player& spawner, Admiral& destination);
			void CreateNewAdmiral(Admiral& destination);
		};

		class AdmiralStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const Admiral& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, Admiral& destination);
		};

		class AdmiralPresentation_JSON
		{

		};
	}
}

#endif //ARCHVERSECORE_GAME_ADMIRAL