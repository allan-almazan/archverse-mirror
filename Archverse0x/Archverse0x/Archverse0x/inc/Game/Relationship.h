#ifndef ARCHVERSECORE_GAME_RELATIONSHIP
#define ARCHVERSECORE_GAME_RELATIONSHIP

#include"System\DBCore.h"
#include"System\JSON\json_spirit.h"

#include<boost\cstdint.hpp>
#include<vector>
#include<string>

namespace ArchverseCore
{
	namespace Game
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::vector;
		using std::string;
		using namespace json_spirit;
		using ArchverseCore::System::DBCore;

		enum Relation
		{
			RELATION_NONE = 0,
			RELATION_SUBORDINARY,
			RELATION_ALLY,
			RELATION_TRADE,
			RELATION_TRUCE,
			RELATION_WAR,
			RELATION_TOTAL_WAR,
			RELATION_BOUNTY,
			RELATION_HOSTILE
		};

		class RelationStorage_BDB;
		class RelationTable;
		class RelationPresentation_JSON;

		class Relationship
		{
			friend class RelationStorage_BDB;
			friend class RelationTable;
			friend class RelationPresentation_JSON;
			uint32_t _id;
			Relation _relation;

			uint32_t _member1;
			uint32_t _member2;
		public:
			const uint32_t Id()  const {return _id;}
			const uint32_t Member1() const {return _member1;}
			const uint32_t Member2() const {return _member2;}
			const Relation MemberRelation() const {return _relation;}
			string RelationText() const;
		};
		
		class RelationStorage_BDB
		{
		public:
			static bool serialize(vector<uint8_t>& destination, const Relationship& source);
			static bool deserialize(const uint8_t* source, const size_t sourceSize, Relationship& destination);
		};

		class RelationTable : public DBCore<Relationship, RelationStorage_BDB>
		{
		public:
			RelationTable(const string& tableName) : DBCore<Relationship, RelationStorage_BDB>(tableName) {}
			bool RelationById(uint32_t relationId, Relationship& destination);
			bool CreateRelationship(uint32_t member1, uint32_t member2, Relation relation, Relationship& destination);
		};
		class RelationList
		{
		private:
			uint32_t _ownerId;
			vector<uint32_t>& _relations;
			RelationTable& _relationTable;
		public:
			RelationList(vector<uint32_t>& relations, RelationTable& relationTable) : _relations(relations), _relationTable(relationTable) {}
			bool RelationTo(uint32_t target, bool create, Relationship& destination);
		};
		using json_spirit::Object;
		class RelationPresentation_JSON
		{
		};
	}
}

#endif //ARCHVERSECORE_GAME_RELATIONSHIP