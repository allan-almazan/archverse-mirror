#ifndef ARCHVERSECORE_SYSTEM_DBCORE_H
#define ARCHVERSECORE_SYSTEM_DBCORE_H

#include <algorithm>
#include <functional>
#include <db_cxx.h>
#include <vector>
#include <boost\cstdint.hpp>
#include <string>

namespace ArchverseCore 
{ 
	namespace System
	{
		using std::vector;
		using boost::uint8_t;
		using boost::uint32_t;
		using std::string;

		template<class RSLT, class FACTORY>
		class DBCore
		{
		private:
			Db* _dbConnection;
			DbEnv* _environment;
			string typeName;
			void Init()
			{
				_environment = new DbEnv(0);
				_environment->open(NULL, DB_CREATE | DB_THREAD | DB_INIT_LOCK | DB_INIT_MPOOL,0);
				_dbConnection = new Db(_environment, 0);
				string name = typeName;
				name += ".db";
				
				_dbConnection->set_error_stream(&std::cout);
				u_int32_t oFlags = DB_CREATE | DB_THREAD;
				// Open the database
				_dbConnection->open(NULL,                // Transaction pointer 
					name.c_str(),          // Database file name 
					NULL,                // Optional logical database name
					DB_BTREE,            // Database access method
					oFlags,              // Open flags
					0);                  // File mode (using defaults)
				//if(FACTORY::Callback() != NULL)
				//{
				//	name = typeName + "_secondary.db";
				//}
			}
		public:
			
			DBCore()
			{
				using std::replace;

				string name = string(typeid(RSLT).name());
				name.replace(0,6, "");
				replace(name.begin(), name.end(), '<', '_');
				replace(name.begin(), name.end(), '>', '_');
				replace(name.begin(), name.end(), ' ', '_');
				replace(name.begin(), name.end(), ':', '_');
				typeName = name;
				Init();
				
			}
			DBCore(const string& name)
			{
				typeName = name;
				Init();
				
			}
			~DBCore()
			{
				_dbConnection->close(0);
				_environment->close(0);
				delete _dbConnection;
				delete _environment;
			}
			bool Exists(uint32_t index)
			{
				Dbt key(&index, sizeof(index));
				int rslt = _dbConnection->exists(0,&key, 0);
				return rslt == 0;
			}
			bool Exists(const string& index)
			{
				Dbt key(const_cast<char*>(&index[0]), index.size());
				int rslt = _dbConnection->exists(0,&key, 0);
				return rslt == 0;
			}

			template<class INDEX, class FUNC>
			bool LockedTransaction(const INDEX& index, const FUNC& functionObject)
			{
				RSLT destination;
				int rslt = 0;
				if((rslt = LoadElement(index, destination)) == 0 || rslt == DB_NOTFOUND)
				{
					if(functionObject(destination))
					{
						return SaveElement(index, destination) == 0;
					}
				}
				return false;
			}
			template<class INDEX, class FUNC>
			bool LockedTransaction(const INDEX& index, FUNC& functionObject)
			{
				RSLT destination;
				int rslt = 0;
				if((rslt = LoadElement(index, destination)) == 0 || rslt == DB_NOTFOUND)
				{
					if(functionObject(destination))
					{
						return SaveElement(index, destination) == 0;
					}
				}
				return false;
			}
			template<class INDEX, class FLDTYPE>
			bool LockedFieldUpdate(const INDEX& index, FLDTYPE RSLT::* fldPosition, const FLDTYPE& value)
			{
				RSLT destination;
				int rslt = 0;
				if((rslt = LoadElement(index, destination)) == 0 || rslt == DB_NOTFOUND)
				{
					destination.*fldPosition = value;
					return SaveElement(index, destination) == 0;
					
				}
				return false;
			}

			template<class INDEX, class FLDTYPE>
			bool LockedSetterUpdate(const INDEX& index, void (RSLT::*fldPosition)(const FLDTYPE&), const FLDTYPE& value)
			{
				RSLT destination;
				int rslt = 0;
				if((rslt = LoadElement(index, destination)) == 0 || rslt == DB_NOTFOUND)
				{
					(destination.*fldPosition)(value);
					return SaveElement(index, destination) == 0;
					
				}
				return false;
			}

			int LoadElement(uint32_t index, RSLT& destination)
			{
				unsigned char buffer[262144];
				Dbt key(&index, sizeof(index));
				Dbt data;
				memset(&data, 0, sizeof(Dbt));
				data.set_flags(DB_DBT_USERMEM);
				data.set_ulen(262144);
				data.set_data(&buffer);
				int ret = 0;

				ret = _dbConnection->get(0, &key, &data, 0);
				if(ret == 0 && data.get_data() != NULL)
				{
					FACTORY::deserialize(reinterpret_cast<uint8_t*>(data.get_data()), data.get_size(), destination);
				}
				return ret;
			}
			int SaveElement(uint32_t index, const RSLT& source)
			{
				vector<uint8_t> buffer;
				FACTORY::serialize(buffer, source);
				Dbt key(&index, sizeof(index));
				Dbt data(&buffer[0], buffer.size());
				int ret = 0;
				ret = _dbConnection->put(0, &key, &data, 0);
				return ret;
			}

			int LoadElement(const string& index, RSLT& destination)
			{
				unsigned char buffer[262144];
				Dbt key(const_cast<char*>(&index[0]), index.size());
				Dbt data;
				memset(&data, 0, sizeof(Dbt));
				data.set_flags(DB_DBT_USERMEM);
				data.set_ulen(262144);
				data.set_data(&buffer);

				int ret = 0;

				ret = _dbConnection->get(0, &key, &data, 0);
				if(ret == 0 && data.get_data() != NULL)
				{
					FACTORY::deserialize(reinterpret_cast<uint8_t*>(data.get_data()), data.get_size(), destination);
				}
				
				return ret;
			}
			int SaveElement(const string& index, const RSLT& source)
			{
				vector<uint8_t> buffer;
				FACTORY::serialize(buffer, source);
				Dbt key(const_cast<char*>(&index[0]), index.size());
				Dbt data(&buffer[0], buffer.size());
				int ret = 0;
				ret = _dbConnection->put(0, &key, &data, 0);
				return ret;
			}
		};
		
	}
}

#endif //ARCHVERSECORE_SYSTEM_DBCORE_H