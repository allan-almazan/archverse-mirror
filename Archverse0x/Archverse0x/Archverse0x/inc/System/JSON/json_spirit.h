#ifndef JASON_SPIRIT
#define JASON_SPIRIT

/* Copyright (c) 2007 John W Wilkinson

   This source code can be used for any purpose as long as
   this comment is retained. */

#pragma once

#include "json_spirit_value.h"
#include "json_spirit_reader.h"
#include "json_spirit_writer.h"

#include<string>

bool same_name( const json_spirit::Pair& pair, const std::string& name );
const json_spirit::Value& find_value( const json_spirit::Object& obj, const std::string& name );


#endif
