#ifndef ARCHVERSECORE_SYSTEM_SERIALIZATIONUTILITYS_H
#define ARCHVERSECORE_SYSTEM_SERIALIZATIONUTILITYS_H

#include<vector>
#include<string>
#include<boost\cstdint.hpp>
#include<string.h>

namespace ArchverseCore
{
	namespace System
	{
		using boost::uint32_t;
		using boost::uint8_t;
		using std::string;
		using std::vector;
		using std::pair;
		

		//static class for helper functions
		class SerializationUtilitys
		{
			
		public:
			static pair<const void*, size_t> BuildSerializationPair(const void* start, const void* end);
			static pair<const void*, size_t> BuildSerializationPair(const string& member);
			static pair<const void*, size_t> BuildSerializationPair(const vector<uint32_t>& member);
			
			static void WriteSerializationPairs(vector<pair<const void*, size_t> >& pairs, vector<uint8_t>& buffer);
			static int DeserializePair(void* start, const void* end, const uint8_t* bufPtr);
			static int DeserializePair(string& member, const uint8_t* bufPtr);
			static int DeserializePair(vector<uint32_t>& member, const uint8_t* bufPtr);

			template<typename T>
			static int DeserializePair(vector<T>& member, const uint8_t* bufPtr)
			{
				size_t pairLength = 0;
				memcpy(&pairLength, bufPtr,sizeof(pairLength));
				bufPtr += sizeof(pairLength);
				if(pairLength > 0)
				{
					member.clear();
					member.resize(pairLength / sizeof(uint32_t));	
					memcpy(&member[0], bufPtr, pairLength);
				}
				return pairLength + sizeof(pairLength);
			}

			template<typename T>
			static pair<const void*, size_t> BuildSerializationPair(const vector<T>& member)
			{
				if(member.size() > 0)
				{
					return pair<const void*, size_t>(&member.at(0), (member.size() * sizeof(T)));
				}
				else
				{
					return pair<const void*, size_t>(NULL, 0);
				}
			}
		};
	}
}

#endif //ARCHVERSECORE_SYSTEM_SERIALIZATIONUTILITYS_H