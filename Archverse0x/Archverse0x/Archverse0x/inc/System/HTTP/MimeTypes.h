
#ifndef HTTP_MIME_TYPES_HPP
#define HTTP_MIME_TYPES_HPP

#include <string>
namespace ArchverseCore
{
	namespace System
	{
		namespace HTTP
		{
			/// Convert a file extension into a MIME type.
			std::string ExtensionToType(const std::string& extension);
		}
	}
}



#endif // HTTP_MIME_TYPES_HPP
