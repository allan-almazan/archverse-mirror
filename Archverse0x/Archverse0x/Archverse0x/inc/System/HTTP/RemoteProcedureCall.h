#ifndef REMOTE_PROCEDURE_CALL_H
#define REMOTE_PROCEDURE_CALL_H

#include "System/HTTP/RequestHandler.h"
#include "System/JSON/json_spirit.h"
#include "System/HTTP/HTMLUtilitys.h"

#include <boost/algorithm/string.hpp>
#include<string>
#include<iostream>
#include<vector>
#include <algorithm>

namespace ArchverseCore
{
	namespace System
	{
		namespace HTTP
		{
			using json_spirit::Object;
			using boost::algorithm::replace_all;
			using std::string;

			class IRemoteProcedureCall
			{
			public:
				virtual string getPageName() = 0;  
				virtual ~IRemoteProcedureCall() {};
				virtual void pageHandler(Object& response, Object& parameters) = 0;
			};
			template<class T>
			class RemoteProcedureCallImpl : public IRemoteProcedureCall
			{
			protected:
				RemoteProcedureCallImpl() 
				{
					RequestHandler::GetRequestHandler()->AddRequestHandler(static_cast<T*>(this));
				}
			public:
				
				string getPageName()
				{
					string str = std::string(typeid(T).name());
					str.replace(0,6, "");
					replace_all(str, "::", "/");
					return "/" + str + ".as";
				}
			};
		}
	}
}
#endif