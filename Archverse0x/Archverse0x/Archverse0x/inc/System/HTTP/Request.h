#ifndef HTTP_REQUEST_HPP
#define HTTP_REQUEST_HPP

#include <string>
#include <vector>
#include "System/http/Header.h"
namespace ArchverseCore
{
	namespace System
	{
		namespace HTTP
		{
			/// A request received from a client.
			struct Request
			{
				Request()
				{
					contentLength = 0;
				}
				std::string method;
				std::string postValue;
				std::string uri;
				int httpVersionMajor;
				int httpVersionMinor;
				int contentLength;
				std::vector<Header> Headers;
			};
		}
	}
}
#endif // HTTP_REQUEST_HPP
