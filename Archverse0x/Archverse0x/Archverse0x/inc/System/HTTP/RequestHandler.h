#ifndef HTTP_RequestHandler_HPP
#define HTTP_RequestHandler_HPP

#include "System/http/HTMLUtilitys.h"

#include <string>
#include <boost/noncopyable.hpp>
#include <map>
#include <algorithm>

namespace ArchverseCore
{
	namespace System
	{
		namespace HTTP
		{
			using std::string;
			using std::map;

			struct Reply;
			struct Request;
			class IRemoteProcedureCall;

			/// The common handler for all incoming requests.
			class RequestHandler
				: private boost::noncopyable
			{
			public:
				/// Handle a request and produce a reply.
				void HandleRequest(const Request& req, Reply& rep);
				static RequestHandler* GetRequestHandler() 
				{
					static RequestHandler* requestHandler = new RequestHandler();
					return requestHandler;
				}
				void AddRequestHandler(IRemoteProcedureCall* pageHandler);
			private:
				map< string, IRemoteProcedureCall* > _pageHandlers;
				IRemoteProcedureCall* GetRPC(const std::string& methodName)
				{
					IRemoteProcedureCall* PageHandler;
				
					//this is the 2nd half to make sure we always deal with case insensitive requests
					string pageRequestName ( toLower(methodName)) ;
					map< string, IRemoteProcedureCall* >::iterator itr = _pageHandlers.find(pageRequestName);
					if( itr!= _pageHandlers.end())
					{
						PageHandler = itr->second;
					}
					else
					{
						PageHandler = NULL;
					}
					return PageHandler;
				}
				
				/// Perform URL-decoding on a string. Returns false if the encoding was
				/// invalid.
				static bool url_decode(const string& in, string& out);
			};
		}
	}
}
#endif // HTTP_RequestHandler_HPP
