#ifndef HTMLUTILITYS_H
#define HTMLUTILITYS_H

#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>


namespace ArchverseCore
{
	namespace System
	{
		namespace HTTP
		{
			using namespace std;
			std::string& URLDecode(std::string& strIn);
			std::string toLower (const std::string & s);
			void Split(const std::string& str, const std::string& delim, std::vector<std::string>& output);

			template <class T>
			bool from_string(T& t, 
							 const std::string& s, 
							 std::ios_base& (*f)(std::ios_base&))
			{
			  std::istringstream iss(s);
			  return !(iss >> f >> t).fail();
			}
			template <class T>
			T from_string(const std::string& s,
							 std::ios_base& (*f)(std::ios_base&) = std::dec)
			{
				T t;
				std::istringstream iss(s);
				(iss >> f >> t);
				return t;
			}
			template<typename T>
			inline std::ostream& XMLOutput(std::ostream& strm, const std::string& tag, const T& value)
			{
				strm<< "<" + tag + ">";
				strm<< value;
				strm<< "</" + tag + ">";
				return strm;
			}
			template<typename T>
			inline std::ostream& RPCRequirement(std::ostream& strm, const std::string& varName)
			{
				std::string str = std::string(typeid(T).name());
				str.replace(0,6, "");
				XMLOutput(strm, str , varName);
				return strm;
			}



			inline string replaceAll( const string& s, const string& f, const string& r ) {
				if ( s.empty() || f.empty() || f == r || s.find(f) == string::npos ) {
					return s;
				}
				ostringstream build_it;
				size_t i = 0;
				for ( size_t pos; ( pos = s.find( f, i ) ) != string::npos; ) {
					build_it.write( &s[i], pos - i );
					build_it << r;
					i = pos + f.size();
				}
				if ( i != s.size() ) {
					build_it.write( &s[i], s.size() - i );
				}
				return build_it.str();
			}

			#define PREREQUISITE(strm, assertion)			\
			{												\
				if(!(assertion))							\
				{											\
					XMLOutput(strm, "result", false);		\
					XMLOutput(strm, "reason", #assertion);	\
					return strm;							\
				}											\
			}

		}
	}
}
#endif