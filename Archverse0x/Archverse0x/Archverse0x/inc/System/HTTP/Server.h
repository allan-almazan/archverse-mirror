

#ifndef HTTP_SERVER_HPP
#define HTTP_SERVER_HPP

#include <boost/asio.hpp>
#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include "System/http/Connection.h"
#include "System/http/RequestHandler.h"

namespace ArchverseCore
{
	namespace System
	{
		namespace HTTP
		{
			/// The top-level class of the HTTP server.
			class Server
				: private boost::noncopyable
			{
			public:
				/// Construct the server to listen on the specified TCP address and port, and
				/// serve up files from the given directory.
				explicit Server(const std::string& address, const std::string& port,std::size_t thread_pool_size);

				/// Run the server's io_service loop.
				void Run();

				/// Stop the server.
				void Stop();

			private:
				/// Handle completion of an asynchronous accept operation.
				void HandleAccept(const boost::system::error_code& e);

				/// The number of threads that will call io_service::run().
				std::size_t _threadPoolSize;

				/// The io_service used to perform asynchronous operations.
				boost::asio::io_service _ioService;

				/// Acceptor used to listen for incoming connections.
				boost::asio::ip::tcp::acceptor _acceptor;

				/// The next connection to be accepted.
				connection_ptr _newConnection;

			};

		}
	}
}
#endif // HTTP_SERVER_HPP
