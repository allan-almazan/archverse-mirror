

#ifndef HTTP_CONNECTION_HPP
#define HTTP_CONNECTION_HPP

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "System/HTTP/Reply.h"
#include "System/HTTP/Request.h"
#include "System/HTTP/RequestHandler.h"
#include "System/HTTP/RequestParser.h"

namespace ArchverseCore
{
	namespace System
	{
		namespace HTTP
		{
			/// Represents a single connection from a client.
			class Connection
				: public boost::enable_shared_from_this<Connection>,
				private boost::noncopyable
			{
			public:
				/// Construct a connection with the given io_service.
				explicit Connection(boost::asio::io_service& io_service,
					RequestHandler& handler);

				/// Get the socket associated with the connection.
				boost::asio::ip::tcp::socket& Socket();

				/// Start the first asynchronous operation for the connection.
				void Start();

			private:
				/// Handle completion of a read operation.
				void HandleRead(const boost::system::error_code& e,
					std::size_t bytes_transferred);

				/// Handle completion of a write operation.
				void HandleWrite(const boost::system::error_code& e);

				/// Strand to ensure the connection's handlers are not called concurrently.
				boost::asio::io_service::strand _strand;

				/// Socket for the connection.
				boost::asio::ip::tcp::socket _socket;

				/// The handler used to process the incoming request.
				RequestHandler& _requestHandler;

				/// Buffer for incoming data.
				boost::array<char, 8192> _buffer;

				/// The incoming request.
				Request _request;

				/// The parser for the incoming request.
				RequestParser _requestParser;

				/// The reply to be sent back to the client.
				Reply _reply;
			};
			typedef boost::shared_ptr<Connection> connection_ptr;
		}
	}
}




#endif // HTTP_CONNECTION_HPP
