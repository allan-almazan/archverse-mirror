

#ifndef HTTP_HEADER_HPP
#define HTTP_HEADER_HPP

#include <string>

namespace ArchverseCore
{
	namespace System
	{
		namespace HTTP
		{
			struct Header
			{
				std::string name;
				std::string value;
			};
		}
	}
}


#endif // HTTP_HEADER_HPP
