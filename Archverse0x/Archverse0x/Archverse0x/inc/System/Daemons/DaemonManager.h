#ifndef ARCHVERSECORE_SYSTEM_DAEMONS_DAEMONMANAGER
#define ARCHVERSECORE_SYSTEM_DAEMONS_DAEMONMANAGER

#include "System\Daemons\BaseDaemon.h"

#include<boost\cstdint.hpp>
#include<vector>


namespace ArchverseCore
{
	namespace System
	{
		namespace Daemons
		{
			using boost::uint32_t;
			using std::vector;

			class DaemonManager
			{
			private:
				vector<BaseDaemon*> _registeredDaemons;
			public:
				void Run();
				void AddDaemon(BaseDaemon* newDaemon);
			};
		}
	}
}

#endif //ARCHVERSECORE_SYSTEM_DAEMONS_DAEMONMANAGER