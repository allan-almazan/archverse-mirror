#ifndef ARCHVERSECORE_SYSTEM_DAEMONS_PLAYER_UPDATE
#define ARCHVERSECORE_SYSTEM_DAEMONS_PLAYER_UPDATE

#include "System/Daemons/BaseDaemon.h"
#include "Game/ControlModel.h"

#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace Game
	{
		class Council;
		class Player;
		struct ControlModel;
		class Territory;
	}
	namespace System
	{
		namespace Daemons
		{
			using namespace ArchverseCore::Game;


			class PlayerUpdate : public BaseDaemon
			{
			private:
				static void ProcessCouncil(Council& council, uint32_t tick);
				static void ProcessPlayer(Council& council, Player& player, ControlModel& councilControlModel, uint32_t tick);
				static void ProcessTerritory(Council& council, Player& player, Territory& territory);
			protected:
				virtual int Run(uint32_t currentTick, uint32_t lastTickToProcess);
			};
		}
	}
}
#endif //ARCHVERSECORE_SYSTEM_DAEMONS_PLAYER_UPDATE