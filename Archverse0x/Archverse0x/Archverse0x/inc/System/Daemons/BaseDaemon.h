#ifndef ARCHVERSECORE_SYSTEM_DAEMONS_BASEDAEMON_H
#define ARCHVERSECORE_SYSTEM_DAEMONS_BASEDAEMON_H

#include<boost\cstdint.hpp>

namespace ArchverseCore
{
	namespace System
	{
		namespace Daemons
		{
			using boost::uint32_t;
			class BaseDaemon
			{
			protected:
				BaseDaemon()
				{
					_lastCycleTime = 0;
					_lastTickProcessed = 0;
				}
				uint32_t _lastCycleTime;
				uint32_t _lastTickProcessed;
				virtual int Run(uint32_t currentTick, uint32_t lastTickToProcess) = 0;
			public:
				void Run(uint32_t currentTick);
				
			};
		}
	}
}

#endif //ARCHVERSECORE_SYSTEM_DAEMONS_BASEDAEMON_H