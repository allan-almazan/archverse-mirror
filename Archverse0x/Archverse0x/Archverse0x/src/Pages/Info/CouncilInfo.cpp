#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Council.h"
#include "Game\Archverse.h"

#include<string>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Info
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class CouncilInfo : public RemoteProcedureCallImpl<CouncilInfo>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Council council;
					if(!COUNCIL_TABLE.CouncilById(lexical_cast<uint32_t>(find_value(parameters, "CouncilId").get_str()),council))
					{
						throw exception("bad councilId");
					}
					CouncilPresentation_JSON::CouncilOverview(council, response);
				}
			};
			CouncilInfo g_CouncilInfo;
		}
	}
}