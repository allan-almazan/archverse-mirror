#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Info
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class PlayerInfo : public RemoteProcedureCallImpl<PlayerInfo>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerById(lexical_cast<uint32_t>(find_value(parameters, "PlayerId").get_str()),player))
					{
						throw exception("bad playerId");
					}
					PlayerPresentation_JSON::PlayerOverview(player, response);
					
				}
			};
			PlayerInfo g_PlayerInfo;
		}
	}
}