#include "System/HTTP/RemoteProcedureCall.h"
#include "Game/UserAccount.h"
#include "Game/Archverse.h"

#include<string>
#include<ostream>

namespace ArchverseCore
{
	namespace Pages
	{
		using namespace ArchverseCore::Game;
		using namespace json_spirit;
		using std::ostream;
		using std::string;
		using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

		class Login : public RemoteProcedureCallImpl<Login>
		{
			virtual void pageHandler(Object& response, Object& parameters) 
			{
				UserAccount loadedUserAccount;
				string authString;
				if(USER_ACCOUNT_TABLE.Login(find_value(parameters, "UserName").get_str(), find_value(parameters, "Password").get_str(), authString, loadedUserAccount))
				{
					response.push_back( Pair( "AuthString", authString ) );
				}
			}
		};
		Login g_Login;
	}
}