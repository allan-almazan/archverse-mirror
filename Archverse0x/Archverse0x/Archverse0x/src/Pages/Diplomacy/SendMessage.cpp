#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Diplomatic
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class SendMessage : public RemoteProcedureCallImpl<SendMessage>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					DiplomaticMessage sendingMessage;
					PLAYER_MESSAGE_TABLE.CreateMessage(player.Id(),
						lexical_cast<uint32_t>(find_value(parameters, "TargetId").get_str()), 
						(MessageType)lexical_cast<int>(find_value(parameters, "MessageType").get_str()),
						find_value(parameters, "Title").get_str(), find_value(parameters, "Body").get_str(), sendingMessage);
					MessageBox<Player, PlayerTable>::SendMessage(PLAYER_MESSAGE_TABLE, PLAYER_TABLE, PLAYER_RELATION_TABLE, sendingMessage);
					
				}
			};
			SendMessage g_SendMessage;
		}
	}
}