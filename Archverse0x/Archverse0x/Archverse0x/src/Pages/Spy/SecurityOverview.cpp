#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Spy
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class SecurityOverview : public RemoteProcedureCallImpl<SecurityOverview>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					response.push_back(Pair("CurrentSecurityLevel", lexical_cast<string>(0)));
					response.push_back(Pair("TargetSecurityLevel", lexical_cast<string>(1)));
					response.push_back(Pair("SecurityCosts", lexical_cast<string>(0)));

					response.push_back(Pair("RecentSecurityActions", Array()));
					Array& securityActions = response.back().value_.get_array();
					//loop this
					{
						securityActions.push_back(Object());
						Object& element = securityActions.back().get_obj();
						element.push_back(Pair("Description", "You launched Astroids at (#54)billybob, destroying 500 labs and killing 40billion on witchita-5"));
						element.push_back(Pair("Turn", lexical_cast<string>(300)));
					}

	
				}
			};
			SecurityOverview g_SecurityOverview;
		}
	}
}