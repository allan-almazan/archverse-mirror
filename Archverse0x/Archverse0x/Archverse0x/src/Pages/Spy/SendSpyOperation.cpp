#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Spy
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class SendSpyOperation : public RemoteProcedureCallImpl<SendSpyOperation>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					uint32_t targetId = lexical_cast<uint32_t>(find_value(parameters, "TargetId").get_str());
					uint32_t spyOperation = lexical_cast<uint32_t>(find_value(parameters, "SpyOpId").get_str());

					response.push_back(Pair("Result", 
						"your spy failed to achive its mission and was killed. This has caused a diplomatic fiasco. Many of your ministers have commited suicide, it will take quite a long time to repair the damage"));
				}
			};
			SendSpyOperation g_SendSpyOperation;
		}
	}
}