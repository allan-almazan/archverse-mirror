#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\Race.h"
#include "Game\Archverse.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Info
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class RaceInfo : public RemoteProcedureCallImpl<RaceInfo>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					RacePresentation_JSON::RaceOverview(RACE_TABLE.RaceFromId(find_value(parameters, "RaceId").get_int()), response);
				}
			};
			RaceInfo g_RaceInfo;
		}
	}
}