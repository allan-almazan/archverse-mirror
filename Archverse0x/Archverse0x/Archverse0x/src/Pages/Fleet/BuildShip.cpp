#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Component.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>
#include <list>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace FleetManagement
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::list;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class BuildShip : public RemoteProcedureCallImpl<BuildShip>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					
				}
			};
			BuildShip g_BuildShip;
		}
	}
}