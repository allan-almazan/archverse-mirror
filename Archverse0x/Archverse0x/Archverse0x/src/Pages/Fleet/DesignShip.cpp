#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Component.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>
#include <list>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace FleetManagement
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::list;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class DesignShip : public RemoteProcedureCallImpl<DesignShip>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					string designName = find_value(parameters, "Name").get_str();
					uint32_t hullId = lexical_cast<uint32_t>(find_value(parameters, "HullId").get_str());
					Array components = find_value(parameters, "Components").get_array();
					list<uint32_t> componentIds;
					BOOST_FOREACH(Value& val, components)
					{
						Object& component = val.get_obj();
						//the names of these parameters will determine their position
						//'Weapon1' 'Weapon2' 'Shield' etc
						componentIds.push_back(lexical_cast<uint32_t>(component[0].value_.get_str()));
					}
				}
			};
			DesignShip g_DesignShip;
		}
	}
}