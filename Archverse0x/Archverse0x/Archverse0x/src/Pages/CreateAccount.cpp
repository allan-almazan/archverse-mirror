#include "System/HTTP/RemoteProcedureCall.h"
#include "Game/UserAccount.h"
#include "Game/Archverse.h"

#include<string>
#include<ostream>
#include<exception>

namespace ArchverseCore
{
	namespace Pages
	{
		using namespace ArchverseCore::Game;;
		using namespace json_spirit;
		using std::ostream;
		using std::string;
		using std::exception;
		using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;
		class CreateAccount : public RemoteProcedureCallImpl<CreateAccount>
		{
			virtual void pageHandler(Object& response, Object& parameters) 
			{
				if(!USER_ACCOUNT_TABLE.CreateUser(find_value(parameters, "UserName").get_str(), find_value(parameters, "Password").get_str()))
				{
					throw exception("could not create user");
				}
			}
		};
		CreateAccount g_CreateAccount;
	}
}