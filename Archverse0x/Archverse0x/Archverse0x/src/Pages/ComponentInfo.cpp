#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\Component.h"
#include "Game\Archverse.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Info
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class ComponentInfo : public RemoteProcedureCallImpl<ComponentInfo>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					ComponentPresentation_JSON::ComponentOverview(COMPONENT_TABLE.ComponentFromId(find_value(parameters, "ComponentId").get_int()), response);
				}
			};
			ComponentInfo g_ComponentInfo;
		}
	}
}