#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\GameInfo.h"
#include "Game\Archverse.h"
#include "System\DBCore.h"

#include<string>
#include<ostream>
#include<exception>

namespace ArchverseCore
{
	namespace Pages
	{
		using namespace ArchverseCore::Game;
		using ArchverseCore::System::DBCore;
		using namespace json_spirit;
		using std::ostream;
		using std::string;
		using std::exception;
		using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

		class CreatePlayer : public RemoteProcedureCallImpl<CreatePlayer>
		{
			virtual void pageHandler(Object& response, Object& parameters) 
			{
				UserAccount requestingUser;
				if(!USER_ACCOUNT_TABLE.UserAccountFromAuthString(find_value(parameters, "AuthString").get_str(),requestingUser))
				{
					throw exception("bad authString");
				}
				
				Player createdPlayer;
				if(!PLAYER_TABLE.CreatePlayer(requestingUser, find_value(parameters, "PlayerName").get_str(),find_value(parameters, "RaceId").get_int(), createdPlayer))
				{
					throw exception("failed to create player");
				}
			}
		};
		CreatePlayer g_CreatePlayer;
	}
}