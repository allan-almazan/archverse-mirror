#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Archverse.h"
#include "Game\Council.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace CouncilManagement
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class ChangeCouncilVote : public RemoteProcedureCallImpl<ChangeCouncilVote>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
						throw exception("bad authString");

					Council requestedCouncil;
					if(!COUNCIL_TABLE.CouncilById(player.CouncilId(), requestedCouncil))
						throw exception("bad councilId");

					uint32_t councilVote = lexical_cast<uint32_t>(find_value(parameters, "CouncilVote").get_str());
					if(!requestedCouncil.Players().ContainsPlayer(councilVote))
						throw exception("cannot vote for players outside of your council");

					PLAYER_TABLE.LockedSetterUpdate(player.Id(), &Player::CouncilVote, councilVote);
				}
			};
			ChangeCouncilVote g_ChangeCouncilVote;
		}
	}
}