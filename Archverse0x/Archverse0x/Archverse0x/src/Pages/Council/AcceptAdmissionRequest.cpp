#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"
#include "Game\AdmissionRequest.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace CouncilManagement
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class AcceptAdmissionRequest : public RemoteProcedureCallImpl<AcceptAdmissionRequest>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					AdmissionRequest request;
					if(ADMISSION_REQUEST_TABLE.LoadElement(lexical_cast<uint32_t>(find_value(parameters, "RequestId").get_str()), request) != 0)
					{
						throw exception("bad admission request Id");
					}
					Council requestedCouncil;
					if(!COUNCIL_TABLE.CouncilById(request.CouncilId(), requestedCouncil))
					{
						throw exception("bad council");
					}
					if(requestedCouncil.SpeakerId() != player.Id())
					{
						throw exception("not the council speaker");
					}
					PLAYER_TABLE.LockedSetterUpdate(request.PlayerId(), &Player::JoinCouncil, requestedCouncil.Id());
				}
			};
			AcceptAdmissionRequest g_AcceptAdmissionRequest;
		}
	}
}