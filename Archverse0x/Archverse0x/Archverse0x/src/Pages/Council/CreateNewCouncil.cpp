#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace CouncilManagement
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class CreateNewCouncil : public RemoteProcedureCallImpl<CreateNewCouncil>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					Council newCouncil;
					COUNCIL_TABLE.CreateNewCouncil(find_value(parameters, "Name").get_str(),
						find_value(parameters, "Motto").get_str(), player.Id(), newCouncil);

					PLAYER_TABLE.LockedSetterUpdate(player.Id(), &Player::JoinCouncil, newCouncil.Id());
				}
			};
			CreateNewCouncil g_CreateNewCouncil;
		}
	}
}