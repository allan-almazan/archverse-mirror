#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace CouncilManagement
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class CouncilNewMessages : public RemoteProcedureCallImpl<CouncilNewMessages>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					Council council;
					if(!COUNCIL_TABLE.CouncilById(player.CouncilId(), council))
					{
						throw exception("bad council Id");
					}
					if(council.SpeakerId() != player.Id())
					{
						throw exception("not the council speaker");
					}
					MessagePresentation_JSON::NewMessageSummary(COUNCIL_MESSAGE_TABLE, council.Messages(), response);
				}
			};
			CouncilNewMessages g_CouncilNewMessages;
		}
	}
}