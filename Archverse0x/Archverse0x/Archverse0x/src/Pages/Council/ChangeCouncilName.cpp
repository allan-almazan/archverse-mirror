#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace CouncilManagement
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class ChangeCouncilName : public RemoteProcedureCallImpl<ChangeCouncilName>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					Council requestedCouncil;
					if(!COUNCIL_TABLE.CouncilById(player.CouncilId(), requestedCouncil))
					{
						throw exception("bad council");
					}
					if(requestedCouncil.SpeakerId() != player.Id())
					{
						throw exception("not the council speaker");
					}
					COUNCIL_TABLE.LockedSetterUpdate(player.CouncilId(), &Council::Name, find_value(parameters, "Name").get_str());
				}
			};
			ChangeCouncilName g_ChangeCouncilName;
		}
	}
}