#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\Tech.h"
#include "Game\Archverse.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Info
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class TechInfo : public RemoteProcedureCallImpl<TechInfo>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					TechPresentation_JSON::TechOverview(TECH_TABLE.TechFromId(find_value(parameters, "TechId").get_int()), response);
				}
			};
			TechInfo g_TechInfo;
		}
	}
}