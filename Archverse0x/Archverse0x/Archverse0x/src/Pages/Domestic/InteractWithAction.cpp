#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"
#include "Game\Tech.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Domestic
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class InteractWithActions : public RemoteProcedureCallImpl<InteractWithActions>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					ActionList playerActions;
					player.Actions(playerActions);
					vector<Action> waitingActions;
					int actionType = find_value(parameters, "ActionType").get_int();
					//validate the user input to make sure they didnt just decide to interact with something that doesnt exist
					switch(actionType)
					{
					case Action::AT_WAITING | Action::AT_HAS_EXPEDITION:
						break;
					default:
						throw exception("unknown/unrecognized action");
					}

					int choice = find_value(parameters, "ActionChoice").get_bool();
					playerActions.InteractWithActions((Action::ActionType)actionType, choice, player);
				}
			};
			InteractWithActions g_InteractWithActions;
		}
	}
}