#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Territory.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Domestic
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class CurrentProject : public RemoteProcedureCallImpl<CurrentProject>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					Territory territory;
					if(!TERRITORY_TABLE.TerritoryFromId(find_value(parameters, "TerritoryId").get_int(), territory))
						throw exception("bad TerritoryId");
					if(territory.OwnerId() != player.Id())
						throw exception("you do not own this territory");

					TerritoryPresentation_JSON::ProjectSummary(territory, response);
				}
			} g_CurrentProject;
		}
	}
}