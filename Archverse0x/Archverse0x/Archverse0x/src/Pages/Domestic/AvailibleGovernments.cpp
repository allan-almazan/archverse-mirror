#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Domestic
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class AvailibleGovernments : public RemoteProcedureCallImpl<AvailibleGovernments>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					response.push_back(Pair("Governments", Array()));
					Array& governments = response.back().value_.get_array();
					governments.push_back(Object());
					Object& element = governments.back().get_obj();
					element.push_back(Pair("Id", "0"));
					element.push_back(Pair("Name", "Corperate Oligarchy"));
					element.push_back(Pair("Description", "You get mo money"));
				}
			};
			AvailibleGovernments g_AvailibleGovernments;
		}
	}
}