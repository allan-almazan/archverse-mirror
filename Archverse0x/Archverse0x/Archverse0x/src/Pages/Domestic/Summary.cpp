#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Archverse.h"

#include<string>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Domestic
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class Summary : public RemoteProcedureCallImpl<Summary>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					PlayerPresentation_JSON::PlayerOverview(player, response);
				}
			};
			Summary g_Summary;
		}
	}
}