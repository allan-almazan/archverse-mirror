#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Territory.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace Domestic
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class SetBuildRatio : public RemoteProcedureCallImpl<SetBuildRatio>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					Territory territory;
					if(!TERRITORY_TABLE.TerritoryFromId(lexical_cast<uint32_t>(find_value(parameters, "TerritoryId").get_int()), territory))
						throw exception("bad TerritoryId");
					if(territory.OwnerId() != player.Id())
						throw exception("you do not own this territory");

					territory.UpdateBuildRatio(find_value(parameters, "Factories").get_int(),
						find_value(parameters, "Mines").get_int()
						,find_value(parameters, "MillitaryBases").get_int()
						,find_value(parameters, "Labs").get_int());
				}
			} g_SetBuildRatio;
		}
	}
}