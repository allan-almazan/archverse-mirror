#include "System/HTTP/RemoteProcedureCall.h"
#include "Game/UserAccount.h"
#include "Game/Archverse.h"

#include<string>
#include<ostream>
#include<exception>

namespace ArchverseCore
{
	namespace Pages
	{
		using namespace ArchverseCore::Game;;
		using namespace json_spirit;
		using std::ostream;
		using std::string;
		using std::exception;
		using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;
		class AccountHasPlayer : public RemoteProcedureCallImpl<AccountHasPlayer>
		{
			virtual void pageHandler(Object& response, Object& parameters) 
			{
				UserAccount requestingUser;
				if(!USER_ACCOUNT_TABLE.UserAccountFromAuthString(find_value(parameters, "AuthString").get_str(),requestingUser))
				{
					throw exception("bad authString");
				}

				if(requestingUser.PlayerId() != 0)
					response.push_back(Pair("Result", "true"));
				else
					response.push_back(Pair("Result", "false"));
			}
		};
		AccountHasPlayer g_AccountHasPlayer;
	}
}