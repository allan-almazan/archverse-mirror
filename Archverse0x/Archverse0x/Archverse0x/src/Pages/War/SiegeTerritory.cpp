#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Territory.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace War
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class SiegeTerritory : public RemoteProcedureCallImpl<SiegeTerritory>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}
					Territory target;
					if(!TERRITORY_TABLE.TerritoryFromId(lexical_cast<uint32_t>(find_value(parameters, "TargetId").get_str()), target))
						throw exception("bad territoryId");
					
					uint32_t fleetId = lexical_cast<uint32_t>(find_value(parameters, "FleetId").get_str());

					//if target !belongs to visible siege target, throw exception("territory is not siegeable");
					//if fleet !belongs to player, throw exception("invalid fleetId");

					response.push_back(Pair("Result", "Failure"));
					response.push_back(Pair("BattleLog", "sdfsdfsfgfgbkmcs;akdcms;djlfnvbsdb"));

				}
			};
			SiegeTerritory g_SiegeTerritory;
		}
	}
}