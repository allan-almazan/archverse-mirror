#include "System\HTTP\RemoteProcedureCall.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\Council.h"

#include<string>
#include<vector>
#include<ostream>
#include<exception>
#include<boost\lexical_cast.hpp>
#include<boost\foreach.hpp>

namespace ArchverseCore
{
	namespace Pages
	{
		namespace War
		{
			using namespace ArchverseCore::Game;
			using namespace json_spirit;
			using std::ostream;
			using std::string;
			using std::vector;
			using std::pair;
			using std::exception;
			using boost::lexical_cast;
			using ArchverseCore::System::HTTP::RemoteProcedureCallImpl;

			class VisibleBlockadeList : public RemoteProcedureCallImpl<VisibleBlockadeList>
			{
				virtual void pageHandler(Object& response, Object& parameters) 
				{
					Player player;
					if(!PLAYER_TABLE.PlayerByAuthString(find_value(parameters, "AuthString").get_str(),player))
					{
						throw exception("bad authString");
					}

					response.push_back(Pair("Targets", Array()));
					Array& targets = response.back().value_.get_array();
					targets.push_back(Object());
					Object& element = targets.back().get_obj();
					element.push_back(Pair("Id", "0"));
					element.push_back(Pair("Name", "Senator Date Rape"));
					element.push_back(Pair("Council", "(#43)DoucheBags"));
					element.push_back(Pair("Power", "1000000000"));

				}
			};
			VisibleBlockadeList g_VisibleBlockadeList;
		}
	}
}