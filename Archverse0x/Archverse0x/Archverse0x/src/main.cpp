#include "System/HTTP/Server.h"
#include "Game/Archverse.h"
#include "System/Daemons/DaemonManager.h"
#include "System/Daemons/PlayerUpdate.h"
#include "Game/GameInfo.h"

#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>

using namespace ArchverseCore::System::HTTP;

boost::function0<void> console_ctrl_function;
BOOL WINAPI console_ctrl_handler(DWORD ctrl_type)
{
	switch (ctrl_type)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_SHUTDOWN_EVENT:
		console_ctrl_function();
		return TRUE;
	default:
		return FALSE;
	}
}

int http_main(int argc, char* argv[])
{
	try
	{
		std::string ipAddy;
		if(argc > 1)
		{
			ipAddy = argv[1];
		}
		else
		{
			ipAddy = "127.0.0.1";
		}
		Server s(ipAddy, "9890", 1);

		// Set console control handler to allow server to be stopped.
		console_ctrl_function = boost::bind(&Server::Stop, &s);
		SetConsoleCtrlHandler(console_ctrl_handler, TRUE);

		// Run the server until stopped.
		s.Run();
	}
	catch (std::exception& e)
	{
		std::cerr << "exception: " << e.what() << "\n";
	}

	return 0;
}

ArchverseCore::Game::Archverse* g_Archverse;

void RunDaemonManager()
{
	ArchverseCore::System::Daemons::DaemonManager daemonManager;
	daemonManager.AddDaemon(new ArchverseCore::System::Daemons::PlayerUpdate());
	daemonManager.Run();
}

int main(int argc, char *argv[])
{
	ArchverseCore::Game::Archverse archverse;
	boost::thread daemonManager(&RunDaemonManager);
	int serverRet = http_main(argc, argv);
	GAMEINFO.Running(false);
	daemonManager.join();
	return serverRet;
}