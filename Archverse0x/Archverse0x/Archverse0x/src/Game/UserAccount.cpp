#include "Game/UserAccount.h"
#include "System/SerializationUtilitys.h"
#include "System/DBCore.h"
#include "Game/Archverse.h"
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/functional/hash.hpp>
#include <string>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>

using boost::uint8_t;
using std::vector;
using std::pair;
using std::string;
using boost::int64_t;
using boost::lexical_cast;
using boost::hash;
using namespace ArchverseCore::System;
using namespace ArchverseCore::Game;

bool UserAccountTable::CreateUser(const string& userName, const string& password)
{
	UserAccount userAccount;
	userAccount._userName = userName;
	userAccount._password = password;
	if(!Exists(userAccount._userName) && 
		SaveElement(userAccount._userName, userAccount) == 0)
	{
		return true;
	}
	return false;
}

bool UserAccountTable::LoadUserAccount(const string& userName, UserAccount& destination)
{
	return LoadElement(userName, destination) == 0;
}
bool UserAccount::Login(const string& password, string& authString)
{
	if(password == _password)
	{
		boost::minstd_rand rng;
		boost::uniform_int<> distribution(0,0x0fffffff);
		boost::variate_generator<boost::minstd_rand&, boost::uniform_int<> >
			randomGenerator(rng, distribution);             // glues randomness with mapping
		int64_t tmpHashCombo;
		hash<string> stringHasher;
		authString = _userName + '$';
		tmpHashCombo = static_cast<int64_t>(stringHasher(_password)) << 32;
		tmpHashCombo |=  randomGenerator();
		authString += lexical_cast<string>(tmpHashCombo);
		_lastGeneratedAuthString = authString;
		USER_ACCOUNT_TABLE.LockedFieldUpdate(_userName, &UserAccount::_lastGeneratedAuthString, authString);
		return true;
	}
	else
		return false;
}

bool UserAccountTable::Login(const string& username, const string& password, string& authString, UserAccount& destination)
{
	LoadUserAccount(username, destination);
	return destination.Login(password, authString);
}

bool UserAccountTable::UserAccountFromAuthString(const string& authString, UserAccount& destination)
{
	string userName;
	int seperatorPosition = authString.find('$');
	if(seperatorPosition != string::npos)
	{
		userName = authString.substr(0, seperatorPosition);
		if(LoadUserAccount(userName, destination))
		{
			return destination._lastGeneratedAuthString == authString;
		}
	}
	return false;
}
bool UserAccountTable::UserAccountFromName(const string& userName, UserAccount& destination)
{
	return LoadUserAccount(userName, destination);
}

bool UserAccount::serialize(vector<uint8_t> &destination, const UserAccount &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._playerId, &source._userName));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._userName));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._password));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._lastGeneratedAuthString));

	SerializationUtilitys::WriteSerializationPairs(pairs, destination);

	return true;
}

bool UserAccount::deserialize(const uint8_t* source, const size_t sourceSize, UserAccount &destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._playerId, &destination._userName, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._userName, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._password, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._lastGeneratedAuthString, source + totalSize);

	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}