#include "Game/Fleet.h"
#include "Game/Archverse.h"
#include "Game/Player.h"
#include "Game/Action.h"

#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>

using namespace ArchverseCore::Game;
using std::string;
using std::vector;
using boost::uint32_t;
using boost::lexical_cast;
using namespace json_spirit;

bool FleetStorage_BDB::serialize(vector<uint8_t>& destination, const Fleet& source)
{
	return false;
}

bool FleetStorage_BDB::deserialize(const uint8_t* source, const size_t sourceSize, Fleet& destination)
{
	return false;
}

void FleetList::ProcessMissions(ArchverseCore::Game::Council &council, ArchverseCore::Game::Player &player)
{
	BOOST_FOREACH(uint32_t fleetId, _fleets)
	{
		Fleet fleet;
		if(!FLEET_TABLE.FleetById(fleetId, fleet))
		{
			throw exception("bad fleetId while processing missions");
		}
		switch(fleet._currentMission)
		{
		case Action::AT_HAS_EXPEDITION:
			uint32_t expeditionLength = player.Turn() - fleet._missionStartTurn;
			if(rand() % expeditionLength == 24) //minimum for expedition
			{
				ActionList playerActions;
				player.Actions(playerActions);
				if(!playerActions.HasAction(static_cast<Action::ActionType>(Action::AT_HAS_EXPEDITION | Action::AT_WAITING)))
				{
					//add the action and pre-calculate the luck seed
					playerActions.AddAction(static_cast<Action::ActionType>(Action::AT_HAS_EXPEDITION | Action::AT_WAITING), player.Turn(), rand() % 100);
				}
			}
			break;
		}
	}
}

bool FleetTable::FleetById(boost::uint32_t fleetId, ArchverseCore::Game::Fleet &destination)
{
	return LoadElement(fleetId, destination) != 0;
}
	


