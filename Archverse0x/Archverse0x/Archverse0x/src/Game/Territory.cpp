#include "Game\Player.h"
#include "Game\Archverse.h"
#include "Game\Territory.h"
#include "Game\GameInfo.h"
#include "Game\Region.h"
#include "System\SerializationUtilitys.h"
#include "System\DBCore.h"
#include "Game\ControlModel.h"
#include "Game\News.h"
#include "Game\Project.h"
#include <boost\foreach.hpp>

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;
using boost::uint8_t;
using boost::uint32_t;
using std::vector;
using std::pair;

bool TerritoryStorage_BDB::serialize(vector<uint8_t> &destination, const ArchverseCore::Game::Territory &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;

	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._attributes));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._projects));

	SerializationUtilitys::WriteSerializationPairs(pairs, destination);

	return true;
}

bool TerritoryStorage_BDB::deserialize(const uint8_t* source, const size_t sourceSize, ArchverseCore::Game::Territory &destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._name, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._name, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._attributes, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._projects, source + totalSize);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool TerritoryTable::TerritoryFromId(uint32_t territoryId, Territory& destination)
{
	return LoadElement(territoryId, destination) == 0;
}
bool TerritoryTable::CreateNewTerritory(Territory& destination, int luckRoll)
{
	return false;
}
bool TerritoryTable::CreateNewHomeTerritory(uint32_t raceId, Territory& destination)
{
	Region parentRegion;
	REGION_TABLE.RandomRegionForPlacement(parentRegion);

	destination._millitaryBases = Territory::STARTING_MILLITARY_BASES;
	destination._mines = Territory::STARTING_MINES;
	destination._factories = Territory::STARTING_FACTORIES;
	destination._researchLabs = Territory::STARTING_LABS;
	destination._size = Territory::SIZE_MEDIUM;
	destination._resource = Territory::RESOURCE_NORMAL;
	do
	{
		destination._id = GAMEINFO.IncrementTerritoryID();
	}while(Exists(destination._id));
	destination._name = parentRegion.NameForNewTerritory();
	destination._targetProjectId = -1;
	destination._regionId = parentRegion.Id();

	if(SaveElement(destination._id, destination) == 0)
	{
		return REGION_TABLE.LockedTransaction(parentRegion.Id(), [&](Region& region) -> bool
		{
			TerritoryList territories = region.Territories();
			territories.AddTerritory(destination);
			return true;
		});
	}
	return false;
}
bool TerritoryTable::RemoveTerritory(uint32_t territoryId)
{
	return false;
}

void TerritoryPresentation_JSON::PrivateTerritoryOverview(TerritoryList& territories, Object& destination)
{
	BOOST_FOREACH(uint32_t& territoryId, territories._territories)
	{
		Territory territory;
		destination.push_back(Pair("Territory", Object()));
		Object& element = destination.back().value_.get_obj();
		TERRITORY_TABLE.TerritoryFromId(territoryId, territory);
		TerritorySummary(territory, element);
	}	
}
void TerritoryPresentation_JSON::TerritorySummary(Territory& territory, Object& destination)
{
	Region parentRegion;
	if(!REGION_TABLE.RegionFromId(territory._regionId, parentRegion))
		throw exception("bad regionId");

	destination.push_back(Pair("Name", territory._name));
	destination.push_back(Pair("Id", lexical_cast<string>(territory._id)));
	destination.push_back(Pair("RegionId", lexical_cast<string>(territory._regionId)));
	destination.push_back(Pair("RegionName", parentRegion.Name()));
	destination.push_back(Pair("Population", lexical_cast<string>(territory._population)));
	destination.push_back(Pair("Factories", lexical_cast<string>(territory._factories)));
	destination.push_back(Pair("Mines", lexical_cast<string>(territory._mines)));
	destination.push_back(Pair("MillitaryBases", lexical_cast<string>(territory._millitaryBases)));
	destination.push_back(Pair("ResearchLabs", lexical_cast<string>(territory._researchLabs)));
	destination.push_back(Pair("Size", territory.SizeString()));
	destination.push_back(Pair("Resource", territory.ResourceString()));
	destination.push_back(Pair("Attributes", territory.AttributesString()));

	destination.push_back(Pair("BuildRation_Factory", lexical_cast<string>(territory._buildRatio.Factory())));
	destination.push_back(Pair("BuildRation_ResearchLab", lexical_cast<string>(territory._buildRatio.ResearchLab())));
	destination.push_back(Pair("BuildRation_MillitaryBase", lexical_cast<string>(territory._buildRatio.MillitaryBase())));
	destination.push_back(Pair("BuildRation_Mine", lexical_cast<string>(territory._buildRatio.Mine())));

	destination.push_back(Pair("CurrentProject", territory._targetProjectId == -1 ? "None" : PROJECT_TABLE.ProjectFromId(territory._targetProjectId).Name()));
	ProjectPresentation_JSON::ProjectListOverview(territory.Projects(), destination);
	ControlModelPresentation_JSON::ControlModelDump(territory._controlModel, destination);

}

void TerritoryPresentation_JSON::ProjectSummary(Territory& territory, Object& destination)
{
	destination.push_back(Pair("CurrentProject", territory._targetProjectId == -1 ? "None" : PROJECT_TABLE.ProjectFromId(territory._targetProjectId).Name()));
	ProjectPresentation_JSON::ProjectListOverview(territory.Projects(), destination);
}

void TerritoryList::AddTerritory(ArchverseCore::Game::Territory& target)
{
	_territories.push_back(target._id);
}

string Territory::SizeString() const
{
	switch(_size)
	{
	case SIZE_TINY:
		return "Tiny";
	case SIZE_SMALL:
		return "Small";
	case SIZE_MEDIUM:
		return "Medium";
	case SIZE_LARGE:
		return "Large";
	case SIZE_HUGE:
		return "Huge";
	default:
		return "unknown";
	}
}

void Territory::UpdateBuildRatio(char factories, char mines, char millitaryBases, char labs)
{
	if((factories + mines + millitaryBases + labs) != 100)
		throw exception(string("invalid build ratio, combined build ratio must equal 100: build ratio was " + lexical_cast<string>((factories + mines + millitaryBases + labs))).c_str());

	_buildRatio.Factory(factories);
	_buildRatio.Mine(mines);
	_buildRatio.MillitaryBase(millitaryBases);
	_buildRatio.ResearchLab(labs);
}
string Territory::ResourceString() const
{
	switch(_resource)
	{
	case RESOURCE_ULTRA_POOR:
		return "Ultra Poor";
	case RESOURCE_POOR:
		return "Poor";
	case RESOURCE_NORMAL:
		return "Normal";
	case RESOURCE_RICH:
		return "Rich";
	case RESOURCE_ULTRA_RICH:
		return "Ultra Rich";
	default:
		return "unknown";
	}
}
string Territory::AttributeString(const TerritoryAttribute& attribute)
{
	switch(attribute)
	{
	case PA_LOST_TRABOTULIN_LIBRARY:
		return "Lost Trabotulin Library";
	case PA_COGNITION_AMPLIFIER:
		return "Cognition Amplifier";
	case PA_MILITARY_STRONGHOLD:
		return "Military Stronghold";
	case PA_ANCIENT_RUINS:
		return "Ancient Ruins";
	case PA_ARTIFACT:
		return "Artifact";
	case PA_MASSIVE_ARTIFACT:
		return "Massive Artifact";
	case PA_ASTEROID:
		return "Asteroid";
	case PA_MOON:
		return "Moon";
	case PA_RADIATION:
		return "Radiation";
	case PA_SEVERE_RADIATION:
		return "Severe Radiation";
	case PA_HOSTILE_MONSTER:
		return "Hostile Monster";
	case PA_OBSTINATE_MICROBE:
		return "Obstinate Microbe";
	case PA_BEAUTIFUL_LANDSCAPE:
		return "Beautiful Landscape";
	case PA_BLACK_HOLE:
		return "Black Hole";
	case PA_NEBULA:
		return "Nebula";
	case PA_DARK_NEBULA:
		return "Dark Nebula";
	case PA_VOLCANIC_ACTIVITY:
		return "Volcanic Activity";
	case PA_INTENSE_VOLCANIC_ACTIVITY:
		return "Intense Volcanic Activity";
	case PA_OCEAN:
		return "Ocean";
	case PA_IRREGULAR_CLIMATE:
		return "Irregular Climate";
	case PA_MAJOR_SPACE_ROUTE:
		return "Major Space Route";
	case PA_MAJOR_SPACE_CROSSROUTE:
		return "Major Space Cross route";
	case PA_FRONTIER_AREA:
		return "Frontier Area";
	case PA_GRAVITY_CONTROLED:
		return "Gravity Controlled";
	case PA_UNDERGROUND_CAVERNS:
		return "Underground Caverns";
	case PA_RARE_ORE:
		return "Rare Ore";
	case PA_MOON_CLUSTER:
		return "Moon Cluster";
	default:
		return "unknown";
	}
}
string Territory::AttributesString() const
{
	string result;
	BOOST_FOREACH(uint32_t attribute, _attributes)
	{
		result += AttributeString(static_cast<TerritoryAttribute>(attribute)) + ", ";
	}
	if(_attributes.size() > 0)
		result.erase(result.size() - 2);

	return result;
}

void Territory::Update(Council& council, Player& player, uint32_t turn)
{
	uint32_t oldPopulation = _population;
	uint32_t oldFactories = _factories;
	uint32_t oldMines = _mines;
	uint32_t oldResearchLabs = _researchLabs;
	uint32_t oldMillitaryBases = _millitaryBases;
	//////////////////////////////////////////////////////////////////////////
	// build planet control model
	///////////////////////////////////////
	ControlModel controlModel = player.ControlModel();

	BOOST_FOREACH(uint32_t territoryId, _projects)
	{
		Project& proj = PROJECT_TABLE.ProjectFromId(territoryId);
		proj.OnUpdateTurn(council, player, *this, controlModel);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// calc output
	//////////////////////////////////////////////////////////////////////////
	uint32_t production = (uint32_t)((double)_factories * ((double)controlModel._production / 100.0f));
	uint32_t ore = (uint32_t)((double)_mines * ((double)controlModel._mining / 100.0f));
	uint32_t mp = (uint32_t)((double)_millitaryBases * ((double)controlModel._commandAndControl / 100.0f));
	uint32_t kp = (uint32_t)((double)_researchLabs * ((double)controlModel._research / 100.0f));

	//////////////////////////////////////////////////////////////////////////
	// build new building
	//////////////////////////////////////////////////////////////////////////
	//the idea is that BASE_WORKFORCE_FOR_BUILDING population is needed to work a building
	unsigned long currentCost = (_researchLabs * BASE_WORKFORCE_FOR_BUILDING) + 
		(_millitaryBases * BASE_WORKFORCE_FOR_BUILDING) + 
		(_mines * BASE_WORKFORCE_FOR_BUILDING) +
		(_factories * BASE_WORKFORCE_FOR_BUILDING);

	int tmpBuildPower = _population - currentCost;
	unsigned buildPower = 0;

	//simplify the math by cutting things off at increments of 100
	if(tmpBuildPower < 100 && tmpBuildPower > 0)
	{
		buildPower = 100;
	}
	else if(tmpBuildPower < 0) //less then 0 means there is no construction
	{
		buildPower = 0;
	}
	else
		buildPower = tmpBuildPower;
	buildPower /= 100;
	//Build power costs Production in varying amounts so make sure we charge for it
	uint32_t researchLab = (buildPower * _buildRatio.ResearchLab()) + _buildRatio.ResearchLabLeftover();
	uint32_t millitaryBase = (buildPower * _buildRatio.MillitaryBase()) + _buildRatio.MillitaryBaseLeftover();
	uint32_t mine = (buildPower * _buildRatio.Mine()) + _buildRatio.MineLeftover();
	uint32_t factory = (buildPower * _buildRatio.Factory()) + _buildRatio.FactoryLeftover();

	_buildRatio.ResearchLabLeftover(researchLab % BUILDING_CONSTRUCTION_COST);
	_buildRatio.MillitaryBaseLeftover(millitaryBase % BUILDING_CONSTRUCTION_COST);
	_buildRatio.MineLeftover(mine % BUILDING_CONSTRUCTION_COST);
	_buildRatio.FactoryLeftover(factory % BUILDING_CONSTRUCTION_COST);

	researchLab -= researchLab % BUILDING_CONSTRUCTION_COST;
	millitaryBase -= millitaryBase % BUILDING_CONSTRUCTION_COST;
	mine -= mine % BUILDING_CONSTRUCTION_COST;
	factory -= factory % BUILDING_CONSTRUCTION_COST;

	_researchLabs += (researchLab / BUILDING_CONSTRUCTION_COST);
	_millitaryBases += (millitaryBase / BUILDING_CONSTRUCTION_COST);
	_mines += (mine / BUILDING_CONSTRUCTION_COST);
	_factories += (factory / BUILDING_CONSTRUCTION_COST);


	//////////////////////////////////////////////////////////////////////////
	// calc pop max and pop growth
	//////////////////////////////////////////////////////////////////////////

	double growth = (double)controlModel._populationGrowth / 100.0;
	uint32_t maxPopulation = (uint32_t)((double)MaxPopulationForSize(_size) * ((double)controlModel._populationDensity / 100.0));

	if (_population == 0)
	{
		_population = 500;
	}
	else
	{
		_population = (uint32_t)(((double)_population) * (.025f + (growth * .005f)));
	}

	if(_population > maxPopulation)
	{
		_population = maxPopulation;
	}
	player.FinishTerritoryUpdate(production, ore, kp, mp, _population, _power);
	player.CurrentNews().CreateUpdateNews(_population - oldPopulation, _mines - oldMines, 
		_factories - oldFactories,_millitaryBases - oldMillitaryBases, _researchLabs - oldResearchLabs ,turn);
}

uint32_t Territory::MaxPopulationForSize(ArchverseCore::Game::Territory::TerritorySize territorySize)
{
	switch(territorySize)
	{
	case Territory::SIZE_HUGE:
			return 250000;
	case Territory::SIZE_LARGE:
		return 215000;
	case Territory::SIZE_MEDIUM:
		return 200000;
	case Territory::SIZE_SMALL:
		return 145000;
	case Territory::SIZE_TINY:
		return 100000;
	default:
		return 0;
	}
}

uint32_t Territory::Raid(boost::uint32_t raidPower)
{
	return raidPower;
}
ProjectList Territory::Projects()
{
	return ProjectList(_projects);
}