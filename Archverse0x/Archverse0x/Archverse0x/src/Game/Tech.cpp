#include "Game/Tech.h"
#include "Game/Archverse.h"
#include "Game/Prerequisite.h"
#include "Game/Component.h"
#include "Game/ControlModel.h"
#include "Game/Race.h"
#include "Game/Project.h"
#include "Game/Event.h"
#include <boost\cstdint.hpp>
#include <boost\foreach.hpp>
#include <boost\lexical_cast.hpp>
#include <boost/assign.hpp>

using namespace ArchverseCore::Game;
using namespace json_spirit;
using std::vector;
using std::string;
using boost::uint32_t;
using boost::uint8_t;
using boost::lexical_cast;
using boost::assign::list_of;

void TechPresentation_JSON::TechListOverview(ArchverseCore::Game::TechList &techList, json_spirit::Object &destination)
{
	destination.push_back(Pair("KnownTechs", Array()));
	Array& techArray = destination.back().value_.get_array();
	BOOST_FOREACH(uint32_t techId, techList._techs)
	{
		techArray.push_back(Object());
		Object& element = techArray.back().get_obj();
		Tech& tech = TECH_TABLE.TechFromId(techId);
		TechPresentation_JSON::TechOverview(tech, element);
	}
}

void TechPresentation_JSON::TechOverview(Tech& tech, Object& destination)
{
	destination.push_back(Pair("Id", lexical_cast<string>(tech._id)));
	destination.push_back(Pair("Name", tech._name));
	destination.push_back(Pair("Description", tech._description));
	PrerequisitePresentation_JSON::PrerequisiteListOverview(PrerequisiteList(tech._prerequisites), destination);

}

Tech& TechTable::TechFromId(boost::uint32_t techId)
{
	return *_techList[techId];
}


TechTable::TechTable()
{
	//////////////////////////////////////////////////////////////////////////
	//Techs
	_techList.push_back(new Tech());
	Tech& metaphysics = *_techList.back();
	metaphysics._id = _techList.size() - 1;
	metaphysics._name = "Metaphysics";
	metaphysics._description = "Metaphysics is the study of comprehending the essence which exists beyond the world of experience in its organized final principles and theories.  What one can feel with his/her five senses of sight, touch, smell, hearing, and taste is something is not \"real\" but only \"feels real.\"  The beginning principle of metaphysics is the effort to realize the \"real\" truth beyond the sensory perceptions through thought.  By starting to realize the principles beyond what was perceived, humans began to treat the essence of existence, not mere objects.";
	metaphysics._category = "Social";

	_techList.push_back(new Tech());
	Tech& logic = *_techList.back();
	logic._id = _techList.size() - 1;
	logic._name = "Logic";
	logic._description = "A cause-and-effect relationship exists in all things.  Logic is the study which seeks to delve into these relationships and arrive at the right conclusions.  This academia is a part of the study of standardization.  Logic seeks to prove the validity of correct reasoning through orderly thought and reasoning procedures.  Through these steps, one can find out errors in one's rationalism, thus coming one step closer to correct reasoning.  Because of this, logic places great importance in the art of expression, and does not make room for ambiguity.  Ambiguity itself is analyzed to the depth of clarity in order to make ideas understood.";
	logic._category = "Social";

	_techList.push_back(new Tech());
	Tech& philosophy = *_techList.back();
	philosophy._id = _techList.size() - 1;
	philosophy._name = "Philosophy";
	philosophy._description = "Philosophy refers to thought itself.  That is, the thought of thought creates the base of philosophy.  This study aims to study the method of arriving at knowledge, rather than knowledge itself.  Recognition, interpretation, understanding, and concluding make up the acts of the academia.  Philosophy is a necessary part in the basic understanding of life and the universe.";
	philosophy._category = "Social";
	
	_techList.push_back(new Tech());
	Tech& religion = *_techList.back();
	religion._id = _techList.size() - 1;
	religion._name = "Religion";
	religion._description = "Since the dawn of literature and academics, religion and faith have always been an existing foundation for improvement and advancement.  In an age where there were more things to be understood and discovered than things that were, an ultimate being which stood above all of creation became the simplest answer to impossible phenomenons.  As a result, religion and faith became the strength for humans to escape from their ignorant fears and carry on with their lives.  Religion has carried out its role well and has held onto its ground of influence amongst advancements in thought and culture.  In becoming the model of devotion and will power, the guiding principle of society and its social codes, and the one thing that people sought for their spiritual strength, religion has existed in the past, present, and will carry on to the future.";
	religion._category = "Social";
	religion._prerequisites.push_back(PREREQUISITE_TABLE.TechRequisite(philosophy));
	
	//////////////////////////////////////////////////////////////////////////
	//Components
	religion._components.push_back(COMPONENT_TABLE.AddWeapon(Weapon(
		"holy hand grenade", 
		"count to 3 not 5", 
		list_of( PREREQUISITE_TABLE.TechRequisite(religion) ),
		vector<uint32_t>(), 
		500, 
		1, 
		vector<uint32_t>(), 
		vector<uint32_t>(), 
		Weapon::WT_BEAM, 
		50, 
		100, 
		100, 
		1, 
		5, 
		1600, 
		360, 
		65536)));

	//////////////////////////////////////////////////////////////////////////
	//Race Initialization
	RACE_TABLE.Seraphim().AddStartingTech(religion);
	RACE_TABLE.Seraphim().AddStartingTech(philosophy);

	//////////////////////////////////////////////////////////////////////////
	//Projects

	class PhilosophersStone
	{
	public:
		PhilosophersStone(vector<uint32_t>& buildings, Project& proj)  
		{
			buildings.push_back(proj.Id());
			proj.OnUpdateTurn = &OnUpdateTurn;
		}
		static void OnUpdateTurn(Council& council, Player& player, Territory& territory, ControlModel& controlModel)
		{
			controlModel._commandAndControl += 5;
		}

	} philosophersStone(philosophy._buildings, PROJECT_TABLE.CreateProject(5000, 
		10000, 
		"Philosophers Stone", 
		"its super cool and stuff", 
		list_of( PREREQUISITE_TABLE.TechRequisite(philosophy))));


	//////////////////////////////////////////////////////////////////////////
	//Events

	class NuclearMeltdown
	{
	public:
		NuclearMeltdown(Event& evnt)  
		{
			evnt.OnUpdateTurn = &OnUpdateTurn;
		}
		static bool OnUpdateTurn(Council& council, Player& player, ControlModel& controlModel)
		{
			controlModel._populationGrowth -= 5;
			//pick a random territory and nuke it
			return true; //the event only occurs once
		}
	} nuclearMeltdown(EVENT_TABLE.CreateEvent("Nuclear Meltdown",
		"Its super suck",
		list_of( PREREQUISITE_TABLE.TechRequisite(philosophy)))); //pretend that says nuclear power

}

TechTable::~TechTable()
{
	BOOST_FOREACH(Tech* tech, _techList)
	{
		delete tech;
	}
}