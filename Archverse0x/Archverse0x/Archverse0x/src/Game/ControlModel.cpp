#include"Game\ControlModel.h"

using namespace ArchverseCore::Game;
using boost::lexical_cast;
using std::string;
using std::vector;
using boost::uint32_t;
using namespace json_spirit;

ControlModel& ControlModel::operator+=(ControlModel& target)
{
	_populationGrowth += target._populationGrowth;
	_populationDensity += target._populationDensity;
	_lifeTechResearch += target._lifeTechResearch;
	_socialTechResearch += target._socialTechResearch;
	_infoTechResearch += target._infoTechResearch;
	_matterEnergyTechResearch += target._matterEnergyTechResearch;
	_racialTechResearch += target._racialTechResearch;
	_research += target._research;
	_commandAndControl += target._commandAndControl;
	_shipProduction += target._shipProduction;
	_shipStrength += target._shipStrength;
	_morale += target._morale;
	_survival += target._survival;
	_berzerker += target._berzerker;
	_admiralty += target._admiralty;
	_mining += target._mining;
	_production += target._production;
	_efficiency += target._efficiency;
	_commerce += target._commerce;
	_diplomacy += target._diplomacy;
	return *this;
}

void ControlModelPresentation_JSON::ControlModelDump(const ControlModel& controlModel, Object& destination)
{
	destination.push_back(Pair("ControlModel", Object()));
	Object& element = destination.back().value_.get_obj();
	element.push_back(Pair("PopulationGrowth", lexical_cast<string>(controlModel._populationGrowth)));
	element.push_back(Pair("PopulationDensity", lexical_cast<string>(controlModel._populationDensity)));
	element.push_back(Pair("LifeTechResearch", lexical_cast<string>(controlModel._lifeTechResearch)));
	element.push_back(Pair("SocialTechResearch", lexical_cast<string>(controlModel._socialTechResearch)));
	element.push_back(Pair("InfoTechResearch", lexical_cast<string>(controlModel._infoTechResearch)));
	element.push_back(Pair("MatterEnergyTechResearch", lexical_cast<string>(controlModel._matterEnergyTechResearch)));
	element.push_back(Pair("RacialTechResearch", lexical_cast<string>(controlModel._racialTechResearch)));
	element.push_back(Pair("Research", lexical_cast<string>(controlModel._research)));
	element.push_back(Pair("CommandAndControl", lexical_cast<string>(controlModel._commandAndControl)));
	element.push_back(Pair("ShipProduction", lexical_cast<string>(controlModel._shipProduction)));
	element.push_back(Pair("ShipStrength", lexical_cast<string>(controlModel._shipStrength)));
	element.push_back(Pair("Moral", lexical_cast<string>(controlModel._morale)));
	element.push_back(Pair("Survival", lexical_cast<string>(controlModel._survival)));
	element.push_back(Pair("Berzerker", lexical_cast<string>(controlModel._berzerker)));
	element.push_back(Pair("Admiralty", lexical_cast<string>(controlModel._admiralty)));
	element.push_back(Pair("Mining", lexical_cast<string>(controlModel._mining)));
	element.push_back(Pair("Production", lexical_cast<string>(controlModel._production)));
	element.push_back(Pair("Efficiency", lexical_cast<string>(controlModel._efficiency)));
	element.push_back(Pair("Commerce", lexical_cast<string>(controlModel._commerce)));
	element.push_back(Pair("Diplomacy", lexical_cast<string>(controlModel._diplomacy)));
}