#include "Game/Ability.h"

using namespace ArchverseCore::Game;
using std::string;

string Ability::AbilityTypeString(ArchverseCore::Game::Ability::AbilityType abilityType)
{
	switch(abilityType)
	{
	case ABILITY_NO_BREATH:
		return "No Breath";
	case ABILITY_TERRAFORM_GRAVITY:
		return "Terraform Gravity";
	case ABILITY_NO_SPY:
		return "No Spy";
	case ABILITY_UNDEAD:
		return "Undead";
	case ABILITY_PSI:
		return "Psi";
	case ABILITY_ENHANCED_PSI:
		return "Enhanced Psi";
	case ABILITY_DUKE:
		return "Duke";
	case ABILITY_MARQUIS:
		return "Marquis";
	case ABILITY_EARL:
		return "Earl";
	case ABILITY_VISCOUNT:
		return "Viscount";
	case ABILITY_BARON:
		return "Baron";
	case ABILITY_ROGUE_DUKE:
		return "Rogue Duke";
	case ABILITY_ROGUE_MARQUIS:
		return "Rogue Marquis";
	case ABILITY_ROGUE_EARL:
		return "Rogue Earl";
	case ABILITY_ROGUE_VISCOUNT:
		return "Rogue Viscount";
	case ABILITY_ROGUE_BARON:
		return "Rogue Baron";
	case ABILITY_GENETIC_ENGINEERING_SPECIALIST:
		return "Genetic Engineering Specialist";
	case ABILITY_FRAGILE_MIND_STRUCTURE:
		return "Fragile Mind Structure";
	case ABILITY_GREAT_SPAWNING_POOL:
		return "Great Spawing Pool";
	case ABILITY_FAST_MANEUVER:
		return "Fast Maneuver";
	case ABILITY_STEALTH:
		return "Stealth";
	case ABILITY_SCAVENGER:
		return "Scavenger";
	case ABILITY_INFORMATION_NETWORK_SPECIALIST:
		return "Information Network Specialist";
	case ABILITY_EFFICIENT_INVESTMENT:
		return "Efficient Investment";
	case ABILITY_DOWNLOADABLE_COMMANDER_EXPERIENCE:
		return "Downloadable Commander Experiance";
	case ABILITY_ASTEROID_MANAGEMENT:
		return "Astroid Management";
	case ABILITY_DIPLOMAT:
		return "Diplomat";
	case ABILITY_TRAINED_MIND:
		return "Trained Mind";
	case ABILITY_PACIFIST:
		return "Pacifist";
	case ABILITY_FANATIC_FLEET:
		return "Fanatic Fleet";
	case ABILITY_HIGH_MORALE:
		return "High Morale";
	case ABILITY_STEALTH_PIRATE:
		return "Stealth Pirate";
	case ABILITY_TACTICAL_MASTERY:
		return "Tactical Mastery";
	case ABILITY_FANATICAL_RECRUITING:
		return "Fanatical Recruiting";
	case ABILITY_MILITARISTIC_DOMINANCE:
		return "Militaristic Dominance";
	case ABILITY_ADVANCED_BATTLE_PROCESSING:
		return "Advanced Battle Processing";
	case ABILITY_HIVE_SHIP_YARD:
		return "Hive Ship Yard";
	case ABILITY_ENHANCED_SHIELDING:
		return "Enhanced Shielding";
	}
	return "Unknown";
}