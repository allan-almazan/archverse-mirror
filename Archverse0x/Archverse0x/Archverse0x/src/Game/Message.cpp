#include "Game\Message.h"
#include "Game\Archverse.h"
#include "Game\GameInfo.h"
#include "Game\Player.h"
#include "System\SerializationUtilitys.h"
#include "System\DBCore.h"
#include "Game\Relationship.h"
#include <boost\foreach.hpp>
#include <exception>
#include <boost\lexical_cast.hpp>
#include <vector>
#include <algorithm>
#include<boost\cstdint.hpp>

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;

using boost::uint32_t;
using boost::uint8_t;
using std::vector;
using std::string;
using std::exception;
using boost::lexical_cast;

bool MessageTable::MessageFromId(boost::uint32_t messageId, DiplomaticMessage &destination)
{
	return LoadElement(messageId, destination) == 0;
}

void MessageTable::CreateMessage(uint32_t senderId, uint32_t targetId, MessageType type, 
						  const string& title, const string& body, DiplomaticMessage& destination)
{
	do
	{
		destination._id = GAMEINFO.IncrementMessageID();
	}while(Exists(destination._id));
	destination._senderId = senderId;
	destination._targetId = targetId;
	destination._type = type;
	destination._title = title;
	destination._body = body;
}

bool MessageTable::SaveMessage(const DiplomaticMessage &source)
{
	return SaveElement(source._id, source) == 0;
}



bool MessageStorage_BDB::deserialize(const boost::uint8_t *source, const size_t sourceSize, ArchverseCore::Game::DiplomaticMessage &destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._title, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._title, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._body, source + totalSize);

	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool MessageStorage_BDB::serialize(std::vector<uint8_t> &destination, const ArchverseCore::Game::DiplomaticMessage &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;

	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._title));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._title));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._body));
	
	SerializationUtilitys::WriteSerializationPairs(pairs, destination);

	return true;
}

