#include "Game\Archverse.h"
#include "Game\GameInfo.h"
#include "Game\UserAccount.h"
#include "Game\Player.h"
#include "Game\Territory.h"
#include "Game\Region.h"
#include "Game\Message.h"
#include "Game\Relationship.h"
#include "Game\Council.h"
#include "Game\AdmissionRequest.h"
#include "Game\Race.h"
#include "Game\Tech.h"
#include "Game\Component.h"
#include "Game\ShipDesign.h"
#include "Game\News.h"
#include "Game\Project.h"
#include "Game\Event.h"
#include "Game\Fleet.h"
#include "Game\Action.h"
#include "Game\Admiral.h"

using namespace ArchverseCore::Game;

Archverse::Archverse()
{
	g_Archverse = this;
	_gameInfo = new ArchverseCore::Game::GameInfo();
	_userAccountTable = new ArchverseCore::Game::UserAccountTable();
	_playerMessageTable = new ArchverseCore::Game::MessageTable("PlayerMessageTable");
	_councilMessageTable = new ArchverseCore::Game::MessageTable("CouncilMessageTable");
	_playerRelationTable = new ArchverseCore::Game::RelationTable("PlayerRelationTable");
	_councilRelationTable = new ArchverseCore::Game::RelationTable("CouncilRelationTable");
	_regionTable = new ArchverseCore::Game::RegionTable();
	_territoryTable = new ArchverseCore::Game::TerritoryTable();
	_playerTable = new ArchverseCore::Game::PlayerTable();
	_councilTable = new ArchverseCore::Game::CouncilTable();
	_admissionTable = new ArchverseCore::Game::AdmissionRequestTable();
	_raceTable = new ArchverseCore::Game::RaceTable();
	_prerequisiteTable = new ArchverseCore::Game::PrerequisiteTable();
	_componentTable = new ArchverseCore::Game::ComponentTable();
	_shipDesignTable = new ArchverseCore::Game::ShipDesignTable();
	_projectTable = new ArchverseCore::Game::ProjectTable();
	_eventTable = new ArchverseCore::Game::EventTable();
	_techTable = new ArchverseCore::Game::TechTable();
	_newsTable = new ArchverseCore::Game::NewsTable();
	_fleetTable = new ArchverseCore::Game::FleetTable();
	_actionTable = new ArchverseCore::Game::ActionTable();
	_admiralTable = new ArchverseCore::Game::AdmiralTable();
	
}
Archverse::~Archverse()
{
	delete _techTable;
	delete _gameInfo;
	delete _userAccountTable;
	delete _playerMessageTable;
	delete _councilMessageTable;
	delete _playerRelationTable;
	delete _councilRelationTable;
	delete _regionTable;
	delete _territoryTable;
	delete _playerTable;
	delete _councilTable;
	delete _admissionTable;
	delete _raceTable;
	delete _prerequisiteTable;
	delete _componentTable;
	delete _shipDesignTable;
	delete _newsTable;
	delete _projectTable;
	delete _eventTable;
	delete _fleetTable;
	delete _actionTable;
	delete _admiralTable;
}