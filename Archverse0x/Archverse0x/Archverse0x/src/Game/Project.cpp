#include "Game/Project.h"
#include "Game/Prerequisite.h"
#include "Game/Archverse.h"

#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>

using namespace ArchverseCore::Game;
using std::string;
using std::vector;
using boost::uint32_t;
using boost::lexical_cast;
using namespace json_spirit;

Project& ProjectTable::ProjectFromId(boost::uint32_t projectId)
{
	return *_projects.at(projectId);
}

Project& ProjectTable::CreateProject(uint32_t rpCost, uint32_t ppCost, const string& name, const string& description, const vector<uint32_t>& prerequisites)
{
	_projectStorage.push_back(Project());
	Project& project = _projectStorage.back();
	project._id = _projects.size();
	project._rpCost = rpCost;
	project._ppCost = ppCost;
	project._prerequisites = prerequisites;
	project._name = name;
	_projects.push_back(&project);
	return project;
}

void ProjectPresentation_JSON::ProjectOverview(Project& project, Object& destination)
{
	/*uint32_t _id;
	uint32_t _rpCost;
	uint32_t _ppCost;
	string _name;
	string _description;
	vector<uint32_t> _prerequisites;*/
	destination.push_back(Pair("Id", lexical_cast<string>(project._id)));
	destination.push_back(Pair("RPCost", lexical_cast<string>(project._rpCost)));
	destination.push_back(Pair("PPCost", lexical_cast<string>(project._ppCost)));
	destination.push_back(Pair("Name", project._name));
	destination.push_back(Pair("Description", project._description));
	PrerequisitePresentation_JSON::PrerequisiteListOverview(project.Prerequisites(), destination);
}

void ProjectPresentation_JSON::ProjectListOverview(ProjectList& projectList, Object& destination)
{
	destination.push_back(Pair("Projects", Array()));
	Array& projects = destination.back().value_.get_array();
	BOOST_FOREACH(uint32_t projectId, projectList._projects)
	{
		projects.push_back(Object());
		Object& element = projects.back().get_obj();
		Project& project = PROJECT_TABLE.ProjectFromId(projectId);
		ProjectOverview(project, element);
	}
	
}

PrerequisiteList Project::Prerequisites()
{
	return PrerequisiteList(_prerequisites);
}

