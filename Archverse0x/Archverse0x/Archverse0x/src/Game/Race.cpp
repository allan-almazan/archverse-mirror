#include "Game/Race.h"
#include "Game/Tech.h"
#include "Game/Ability.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <boost/foreach.hpp>
#include <list>
#include <string>

using std::string;
using std::list;
using boost::lexical_cast;
using boost::uint32_t;

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;

RaceTable::RaceTable()
{
	_races.push_back(Race());
	Race& seraphim = _races.back();
	seraphim._id = _races.size() - 1;
	seraphim._name = "Seraphim";
	_races.push_back(Race());
	Race& human = _races.back();
	human._id = _races.size() - 1;
	human._name = "Human";
}

void Race::AddStartingTech(const Tech& tech)
{
	 _startingTechs.push_back(tech.Id());
}

Race& RaceTable::RaceFromId(boost::uint32_t raceId)
{
	return _races.at(raceId);
}

string Race::SocialTypeString(ArchverseCore::Game::Race::SocialType socialType)
{
	switch(socialType)
	{
	case Race::SOCIETY_CLASSISM:
		return "Classism";
	case Race::SOCIETY_PERSONALISM:
		return "Personalism";
	case Race::SOCIETY_TOTALISM:
		return "Totalism";
	}
	return "Unknown";
}

void RacePresentation_JSON::RaceOverview(ArchverseCore::Game::Race &race, json_spirit::Object &destination)
{
	destination.push_back(Pair("Id", lexical_cast<string>(race._id)));
	destination.push_back(Pair("SocialType", Race::SocialTypeString(race._socialType)));
	ControlModelPresentation_JSON::ControlModelDump(race._controlModel, destination);
	destination.push_back(Pair("Name", race._name));
	destination.push_back(Pair("Description", race._description));
	list<string> abilities;
	BOOST_FOREACH(uint32_t abilityId, race._abilities)
	{
		abilities.push_back(Ability::AbilityTypeString((Ability::AbilityType)abilityId));
	}
	destination.push_back(Pair("Abilities", boost::algorithm::join(abilities, ", ")));

	TechPresentation_JSON::TechListOverview(TechList(race._startingTechs), destination);
}