#include "Game\Relationship.h"
#include "Game\Archverse.h"
#include "System\SerializationUtilitys.h"
#include "Game\GameInfo.h"

#include <boost\foreach.hpp>
#include <vector>
#include <string>
#include<boost\cstdint.hpp>

using namespace ArchverseCore::Game;
using boost::uint32_t;
using boost::uint8_t;
using std::vector;
using std::string;
using std::pair;

bool RelationTable::RelationById(uint32_t relationId, Relationship& destination)
{
	return LoadElement(relationId ,destination) == 0;
}
bool RelationTable::CreateRelationship(uint32_t member1, uint32_t member2, Relation relation, Relationship& destination)
{
	destination._member1 = member1;
	destination._member2 = member2;
	destination._relation = relation;
	do
	{
		destination._id = GAMEINFO.IncrementRelationID();
	}while(Exists(destination._id));
	return SaveElement(destination._id, destination) == 0;
}

bool RelationStorage_BDB::serialize(vector<uint8_t>& destination, const Relationship& source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, reinterpret_cast<void*>(reinterpret_cast<size_t>(&source._member2) + sizeof(source._member2))));
	SerializationUtilitys::WriteSerializationPairs(pairs, destination);
	return true;
}
bool RelationStorage_BDB::deserialize(const uint8_t* source, const size_t sourceSize, Relationship& destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, reinterpret_cast<void*>(reinterpret_cast<size_t>(&destination._member2) + sizeof(destination._member2)), source);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool RelationList::RelationTo(uint32_t targetId, bool create, Relationship& destination)
{
	BOOST_FOREACH(uint32_t relationId, _relations)
	{
		_relationTable.RelationById(relationId, destination);
		if(destination.Member1() == targetId || destination.Member2() == targetId)
		{
			return true;
		}
	}
	if(create)
	{
		_relationTable.CreateRelationship(_ownerId, targetId, RELATION_NONE, destination);
	}
	return false;
}

string Relationship::RelationText() const
{
	switch(_relation)
	{
		case RELATION_NONE:
			return "None";
		case RELATION_SUBORDINARY:
			return "Subordinary";
		case RELATION_ALLY:
			return "Allied";
		case RELATION_TRADE:
			return "Trade Pact";
		case RELATION_TRUCE:
			return "Truce";
		case RELATION_WAR:
			return "War";
		case RELATION_TOTAL_WAR:
			return "Total War";
		case RELATION_BOUNTY:
			return "Bounty";
		case RELATION_HOSTILE:
			return "Hostile";
	}
}