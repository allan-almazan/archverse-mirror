#include"Game/Component.h"
#include"Game/Prerequisite.h"
#include <boost/lexical_cast.hpp>

using namespace ArchverseCore::Game;
using std::vector;
using std::string;
using std::list;
using boost::uint32_t;
using boost::lexical_cast;

uint32_t ComponentTable::AddWeapon(Weapon& weapon)
{
	weapon._id = _components.size();
	_weapons.push_back(weapon);
	_components.push_back(&(_weapons.back()));
	return weapon._id;
}
uint32_t ComponentTable::AddDevice(Device& device)
{
	device._id = _components.size();
	_devices.push_back(device);
	_components.push_back(&(_devices.back()));
	return device._id;
}
uint32_t ComponentTable::AddComputer(Computer& computer)
{
	computer._id = _components.size();
	_computers.push_back(computer);
	_components.push_back(&(_computers.back()));
	return computer._id;
}
uint32_t ComponentTable::AddArmor(Armor& armor)
{
	armor._id = _components.size();
	_armors.push_back(armor);
	_components.push_back(&(_armors.back()));
	return armor._id;
}
uint32_t ComponentTable::AddEngine(Engine& engine)
{
	engine._id = _components.size();
	_engines.push_back(engine);
	_components.push_back(&(_engines.back()));
	return engine._id;
}

Component* ComponentTable::ComponentFromId(uint32_t componentId)
{
	return _components[componentId];
}

void ComponentPresentation_JSON::ComponentOverview(ArchverseCore::Game::Component *component, json_spirit::Object &destination)
{
	Weapon* weapon;
	Shield* shield;
	Device* device;
	Engine* engine;
	Computer* computer;
	Armor* armor;
	if((weapon = dynamic_cast<Weapon*>(component)) != NULL)
		WeaponOverview(weapon, destination);

	else if((shield = dynamic_cast<Shield*>(component)) != NULL)
		ShieldOverview(shield, destination);

	else if((device = dynamic_cast<Device*>(component)) != NULL)
		DeviceOverview(device, destination);

	else if((engine = dynamic_cast<Engine*>(component)) != NULL)
		EngineOverview(engine, destination);

	else if((computer = dynamic_cast<Computer*>(component)) != NULL)
		ComputerOverview(computer, destination);

	else if((armor = dynamic_cast<Armor*>(component)) != NULL)
		ArmorOverview(armor, destination);
}

void ComponentPresentation_JSON::WeaponOverview(Weapon* component, Object& destination)
{
	destination.push_back(Pair("ComponentType", "Weapon"));
	ComponentOverviewImpl(component, destination);
	destination.push_back(Pair("WeaponType", Weapon::WeaponTypeString(component->_weaponType)));
	destination.push_back(Pair("AttackingRate", lexical_cast<string>(component->_attackingRate)));
	destination.push_back(Pair("DamageRoll", lexical_cast<string>(component->_damageRoll)));
	destination.push_back(Pair("DamageDice", lexical_cast<string>(component->_damageDice)));
	destination.push_back(Pair("Space", lexical_cast<string>(component->_space)));
	destination.push_back(Pair("CoolingTime", lexical_cast<string>(component->_coolingTime)));
	destination.push_back(Pair("Range", lexical_cast<string>(component->_range)));
	destination.push_back(Pair("AngleOfFire", lexical_cast<string>(component->_angleOfFire)));
	destination.push_back(Pair("Speed", lexical_cast<string>(component->_speed)));
}
void ComponentPresentation_JSON::ShieldOverview(Shield* component, Object& destination)
{
	destination.push_back(Pair("ComponentType", "Shield"));
	ComponentOverviewImpl(component, destination);
	destination.push_back(Pair("Strength", lexical_cast<string>(component->_strength)));
	destination.push_back(Pair("Solidity", lexical_cast<string>(component->_solidity)));
	destination.push_back(Pair("RechargeRate", lexical_cast<string>(component->_rechargeRate)));
}
void ComponentPresentation_JSON::ArmorOverview(Armor* component, Object& destination)
{
	destination.push_back(Pair("ComponentType", "Armor"));
	ComponentOverviewImpl(component, destination);
	destination.push_back(Pair("ArmorType", Armor::ArmorTypeString(component->_armorType)));
	destination.push_back(Pair("DefenseRate", lexical_cast<string>(component->_defenseRate)));
	destination.push_back(Pair("HPMultiplier", lexical_cast<string>(component->_hpMultiplier)));
}
void ComponentPresentation_JSON::ComputerOverview(Computer* component, Object& destination)
{
	destination.push_back(Pair("ComponentType", "Computer"));
	ComponentOverviewImpl(component, destination);
	destination.push_back(Pair("AttackRate", lexical_cast<string>(component->_attackRate)));
	destination.push_back(Pair("DefenseRate", lexical_cast<string>(component->_defenseRate)));
	destination.push_back(Pair("MaxTargets", lexical_cast<string>(component->_maxTargets)));
}
void ComponentPresentation_JSON::EngineOverview(Engine* component, Object& destination)
{
	destination.push_back(Pair("ComponentType", "Engine"));
	ComponentOverviewImpl(component, destination);
	destination.push_back(Pair("BattleSpeed", lexical_cast<string>(component->_battleSpeed)));
	destination.push_back(Pair("BattleMobility", lexical_cast<string>(component->_battleMobility)));
	destination.push_back(Pair("EngineRecharge", lexical_cast<string>(component->_engineRecharge)));
}

void ComponentPresentation_JSON::DeviceOverview(Device* component, Object& destination)
{
	destination.push_back(Pair("ComponentType", "Device"));
	ComponentOverviewImpl(component, destination);
}

void ComponentPresentation_JSON::ComponentOverviewImpl(Component* component, Object& destination)
{
	/*
	vector<uint32_t> _staticFleetEffect;
	vector<uint32_t> _dynamicFleetEffect;*/

	destination.push_back(Pair("Id", lexical_cast<string>(component->_id)));
	destination.push_back(Pair("Name", component->_name));
	destination.push_back(Pair("Description", component->_description));
	
	PrerequisitePresentation_JSON::PrerequisiteListOverview(PrerequisiteList(component->_prerequisites), destination);
	PrerequisitePresentation_JSON::DesignPrerequisiteListOverview(PrerequisiteList(component->_designPrerequisites), destination);
	destination.push_back(Pair("Cost", lexical_cast<string>(component->_cost)));
	destination.push_back(Pair("Power", lexical_cast<string>(component->_power)));
}

string Weapon::WeaponTypeString(ArchverseCore::Game::Weapon::WeaponType weaponType)
{
	switch(weaponType)
	{
	case Weapon::WT_BEAM:
		return "Beam";
	case Weapon::WT_PROJECTILE:
		return "Projectile";
	case Weapon::WT_MISSILE:
		return "Missile";
	}
	return "UNKNOWN";
}

string Armor::ArmorTypeString(ArchverseCore::Game::Armor::ArmorType armorType)
{
	switch(armorType)
	{
	case Armor::AT_NORM:
		return "Normal";
	case Armor::AT_BIO:
		return "Bio";
	case Armor::AT_REACTIVE:
		return "Reactive";
	}
	return "UNKNOWN";
}