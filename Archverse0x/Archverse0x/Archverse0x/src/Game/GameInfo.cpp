#include "Game\GameInfo.h"
#include "System\SerializationUtilitys.h"
#include "System\DBCore.h"
#include "Game\Archverse.h"

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;

class IncrementID
{
private:
	uint32_t GameInfoData::* _target;
	uint32_t _rslt;
public:
	IncrementID(uint32_t GameInfoData::* target) : _target(target) {_rslt = 1;}
	bool operator()(GameInfoData& gameInfo)
	{
		gameInfo.*_target += 1;
		_rslt = gameInfo.*_target;
		return true;
	}
	uint32_t Result() {return _rslt;}
};
class PlusEquals
{
private:
	uint32_t GameInfoData::* _target;
	uint32_t _rslt;
	uint32_t _amount;
public:
	PlusEquals(uint32_t GameInfoData::* target, uint32_t amount) : _target(target), _amount(amount) {_rslt = 1;}
	bool operator()(GameInfoData& gameInfo)
	{
		gameInfo.*_target += _amount;
		_rslt = gameInfo.*_target;
		return true;
	}
	uint32_t Result() {return _rslt;}
};
template<typename T>
class LockedRead
{
private:
	T GameInfoData::* _target;
	T _rslt;
public:
	LockedRead(T GameInfoData::* target) : _target(target) {_rslt = 1;}
	bool operator()(GameInfoData& gameInfo)
	{
		_rslt = gameInfo.*_target;
		return true;
	}
	T Result() {return _rslt;}
};

uint32_t GameInfo::IncrementPlayerID()
{
	IncrementID incr(&GameInfoData::_nextPlayerId);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment Player Id");
	return incr.Result() - 1;
}

uint32_t GameInfo::Tick()
{
	IncrementID incr(&GameInfoData::_tick);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment Tick");
	return incr.Result() - 1;
}
void GameInfo::AddTicks(uint32_t ticks)
{
	PlusEquals incr(&GameInfoData::_tick, ticks);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to add ticks");
}
bool GameInfo::Running()
{
	LockedRead<bool> incr(&GameInfoData::_running);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to get Running");
	return incr.Result();
}

void GameInfo::Running(bool running)
{
	if(!LockedFieldUpdate(1, &GameInfoData::_running, running))
		throw exception("Failed to set Running");

}
uint32_t GameInfo::IncrementTerritoryID()
{
	IncrementID incr(&GameInfoData::_nextTerritoryId);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment TerritoryId");
	return incr.Result() - 1;
}
uint32_t GameInfo::IncrementNewsID()
{
	IncrementID incr(&GameInfoData::_nextNewsId);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment NewsId");
	return incr.Result() - 1;
}
uint32_t GameInfo::IncrementRegionID()
{
	IncrementID incr(&GameInfoData::_nextRegionId);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment RegionId");
	return incr.Result() - 1;
}
uint32_t GameInfo::IncrementMessageID()
{
	IncrementID incr(&GameInfoData::_nextMessageId);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment MessageId");
	return incr.Result() - 1;
}
uint32_t GameInfo::IncrementRelationID()
{
	IncrementID incr(&GameInfoData::_nextRelationId);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment RelationId");
	return incr.Result() - 1;
}
uint32_t GameInfo::IncrementCouncilID()
{
	IncrementID incr(&GameInfoData::_nextCouncilId);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment CouncilId");
	return incr.Result() - 1;
}

uint32_t GameInfo::IncrementAdmissionID()
{
	IncrementID incr(&GameInfoData::_nextAdmissionId);
	if(!LockedTransaction(1, incr))
		throw exception("Failed to Increment AdmissionId");
	return incr.Result() - 1;
}

uint32_t GameInfo::RegionCount()
{
	return 10;
}

bool GameInfo::deserialize(const boost::uint8_t *source, const size_t sourceSize, ArchverseCore::Game::GameInfoData &destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._nextPlayerId, &destination._councils, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._councils, source + totalSize);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool GameInfo::serialize(std::vector<uint8_t> &destination, const ArchverseCore::Game::GameInfoData &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._nextPlayerId, &source._councils));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._councils));
	SerializationUtilitys::WriteSerializationPairs(pairs, destination);
	return true;
}

CouncilList GameInfo::Councils()
{
	int loadElementRslt = LoadElement(1, gid);
	if(loadElementRslt != 0 && loadElementRslt != DB_NOTFOUND)
		throw exception("error loading GameInfo::Councils");

	return CouncilList(gid._councils);
}

CouncilList GameInfo::Councils(GameInfoData& gameInfo)
{
	GAMEINFO.LoadElement(1, gameInfo);
	return CouncilList(gameInfo._councils);
}