#include "Game/Player.h"
#include "Game/Prerequisite.h"
#include "Game/Tech.h"
#include "Game/Archverse.h"

using namespace ArchverseCore::Game;

bool Prerequisite::evaluate(Player& player)
{
	uint32_t value = player.ValueForPrerequisite(_type);
	switch(_boolOperator)
	{
	case RBO_NONE:
		switch(_operator)
		{
		case RO_EQUAL:
			return _argument == value;
		case RO_NOT:
			return _argument != value;
		case RO_GREATER:
			return _argument < value;
		case RO_LESS:
			return _argument > value;
		case RO_GREATER_EQUAL:
			return _argument <= value;
		case RO_LESS_EQUAL:
			return _argument >= value;
		}
		break;
	}
	
}

void PrerequisiteTable::AddPrerequisite(ArchverseCore::Game::Prerequisite &prerequisite)
{
	prerequisite._id = _prerequisiteList.size();
	_prerequisiteList.push_back(prerequisite);
}

Prerequisite& PrerequisiteTable::PrerequisiteFromId(uint32_t prerequisiteId)
{
	return _prerequisiteList[prerequisiteId];
}

void PrerequisitePresentation_JSON::PrerequisiteListOverview(ArchverseCore::Game::PrerequisiteList &prerequisiteList, Object &destination)
{
	destination.push_back(Pair("Prerequisites", Array()));
	Array& elements = destination.back().value_.get_array();
	BOOST_FOREACH(uint32_t prerequisiteId, prerequisiteList._prerequisites)
	{
		elements.push_back(Object());
		Object& element = elements.back().get_obj();
		Prerequisite& prereq = PREREQUISITE_TABLE.PrerequisiteFromId(prerequisiteId);
		PrerequisiteOverview(prereq, element);
	}
}

void PrerequisitePresentation_JSON::PrerequisiteOverview(ArchverseCore::Game::Prerequisite &prerequisite, Object &destination)
{
	destination.push_back(Pair("Id", lexical_cast<string>(prerequisite._id)));
	destination.push_back(Pair("Description", prerequisite._description));
}

uint32_t PrerequisiteTable::TechRequisite(Tech &requirement)
{
	_prerequisiteList.push_back(Prerequisite());
	Prerequisite& prerequisite = _prerequisiteList.back();
	prerequisite._id = _prerequisiteList.size() - 1;

	prerequisite._description = "Requires " + requirement.Name();
	prerequisite._type = Prerequisite::RT_TECH;
	prerequisite._boolOperator = Prerequisite::RBO_NONE;
	prerequisite._argument = requirement.Id();
	prerequisite._operator = Prerequisite::RO_EQUAL;

	return prerequisite._id;
}

void PrerequisitePresentation_JSON::DesignPrerequisiteListOverview(ArchverseCore::Game::PrerequisiteList &prerequisiteList, json_spirit::Object &destination)
{
	destination.push_back(Pair("DesignPrerequisites", Array()));
	Array& elements = destination.back().value_.get_array();
	BOOST_FOREACH(uint32_t prerequisiteId, prerequisiteList._prerequisites)
	{
		elements.push_back(Object());
		Object& element = elements.back().get_obj();
		Prerequisite& prereq = PREREQUISITE_TABLE.PrerequisiteFromId(prerequisiteId);
		PrerequisiteOverview(prereq, element);
	}
}