#include "Game/Action.h"
#include "System/SerializationUtilitys.h"
#include "Game/Archverse.h"
#include "Game/Player.h"
#include <boost/foreach.hpp>
#include <vector>

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;
using namespace json_spirit;
using boost::uint32_t;
using boost::uint8_t;
using std::string;
using std::vector;


bool ActionTable::ActionById(uint32_t actionId, ArchverseCore::Game::ActionList &destination)
{
	
	return LoadElement(actionId, destination) != 0;
}

bool ActionStorage_BDB::serialize(vector<uint8_t>& destination, const ActionList& source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;

	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._actions));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._actions));

	SerializationUtilitys::WriteSerializationPairs(pairs, destination);

	return true;
}
bool ActionStorage_BDB::deserialize(const uint8_t* source, const size_t sourceSize, ActionList& destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._actions, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._actions, source + totalSize);

	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

void ActionList::AddAction(ArchverseCore::Game::Action::ActionType actionType, boost::uint32_t turn, boost::uint32_t data)
{
	Action action;
	action._actionType = actionType;
	action._turn = turn;
	action._data = data;
	PushAction(action); //make sure our local version has this new action in it
	ACTION_TABLE.LockedSetterUpdate(_id, &ActionList::PushAction, action); //add to db
}

void ActionList::PushAction(const ArchverseCore::Game::Action &action)
{
	_actions.push_back(action);
}

bool ActionList::HasAction(ArchverseCore::Game::Action::ActionType actionType)
{
	BOOST_FOREACH(Action& action, _actions)
	{
		if((action._actionType & actionType) == actionType)
			return true;
	}
	return false;
}

void ActionList::FindActions(ArchverseCore::Game::Action::ActionType actionType, std::vector<Action> &actions)
{
	BOOST_FOREACH(Action& action, _actions)
	{
		if((action._actionType & actionType) == actionType)
			actions.push_back(action);
	}
}

void ActionList::InteractWithActions(ArchverseCore::Game::Action::ActionType actionType, int input, Player& player)
{
	ACTION_TABLE.LockedTransaction(_id, [&](ActionList& action) -> bool
	{
		BOOST_FOREACH(Action& actionElement, action._actions)
		{
			if((actionElement._actionType & actionType) == actionType)
			{
				//return of true means we're finished with the action
				if(actionElement.Interact(player, input))
				{
					action._actions.erase(std::remove_if(action._actions.begin(), action._actions.end(), [&](Action& actionElement) -> bool
					{
						return ((actionElement._actionType & actionType) == actionType);
					}));
				}
				return true;
			}
		}
		return false;
	});
}

bool Action::Interact(Player &player, int input)
{
	switch(_actionType)
	{
	case Action::AT_HAS_EXPEDITION | Action::AT_WAITING:
		{
			if(input != 0)
			{
				//grant player planet
				PLAYER_TABLE.LockedTransaction(player.Id(), [&](Player& player) -> bool
				{
					player.ExpeditionTerritory(input);
					return true;
				});
			}
			return true;
		}
	}
}

void ActionListPresentation_JSON::ActionsOverview(std::vector<Action> &actions, json_spirit::Object &destination)
{
	destination.push_back(Pair("Actions", Array()));
	Array& actionArray = destination.back().value_.get_array();
	BOOST_FOREACH(Action& action, actions)
	{
		actionArray.push_back(Object());
		Object& element = actionArray.back().get_obj();
		element.push_back(Pair("ActionType", action.ActionString()));
		element.push_back(Pair("Turn", lexical_cast<string>(action._turn)));
	}
}

string Action::ActionString() const
{
	return "";
}