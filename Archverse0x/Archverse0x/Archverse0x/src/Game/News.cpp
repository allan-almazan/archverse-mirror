#include "Game/News.h"
#include "System/json/json_spirit.h"
#include "System/SerializationUtilitys.h"
#include "Game/Archverse.h"
#include "Game/GameInfo.h"

#include<boost/lexical_cast.hpp>
#include<boost/foreach.hpp>
#include<exception>

using namespace ArchverseCore::Game;
using namespace json_spirit;
using boost::lexical_cast;
using std::string;
using std::exception;

bool NewsStorage_BDB::deserialize(const boost::uint8_t *source, const size_t sourceSize, ArchverseCore::Game::News &destination)
{
	using ArchverseCore::System::SerializationUtilitys;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._title, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._title, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._body, source + totalSize);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool NewsStorage_BDB::serialize(std::vector<uint8_t> &destination, const ArchverseCore::Game::News &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._title));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._title));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._body));
	SerializationUtilitys::WriteSerializationPairs(pairs, destination);
	return true;
}

bool NewsTable::NewsById(uint32_t newsId, News& destination)
{
	return LoadElement(newsId, destination) == 0;
}

void NewsPresentation_JSON::NewsOverview(News& news, Object& destination)
{
	destination.push_back(Pair("Id", lexical_cast<string>(news._id)));
	destination.push_back(Pair("Turn", lexical_cast<string>(news._turn)));
	destination.push_back(Pair("Owner", lexical_cast<string>(news._owner)));
	destination.push_back(Pair("Title", news._title));
	destination.push_back(Pair("Body", news._body));
}

void NewsPresentation_JSON::NewsListOverview(NewsList& news, Object& destination)
{
	destination.push_back(Pair("News", Array()));
	Array& newsList = destination.back().value_.get_array();

	BOOST_FOREACH(uint32_t newsId, news._news)
	{
		News news;
		if(!NEWS_TABLE.NewsById(newsId, news))
			throw exception("bad news id");

		newsList.push_back(Object());
		Object& element = newsList.back().get_obj();
		NewsOverview(news, element);
	}
}

void NewsList::CreateUpdateNews(int popChange, int mineChange, int factoryChange, int millitaryBaseChange, int labChange, boost::uint32_t turn)
{
	News updateNews;
	updateNews._body = "Update Turn(" + lexical_cast<string>(turn) + "Population Change(" + lexical_cast<string>(popChange) + "), Mines Built(" + 
		lexical_cast<string>(mineChange) + ") Factories Built(" + lexical_cast<string>(factoryChange) + ") Millitary Bases Built(" + 
		lexical_cast<string>(millitaryBaseChange) + ") Research Labs Built(" + lexical_cast<string>(labChange) + ")";
	updateNews._title = "Territory Update";
	do
	{
		updateNews._id = GAMEINFO.IncrementNewsID();
	}while(NEWS_TABLE.Exists(updateNews._id));

	updateNews._turn = turn;

	NEWS_TABLE.SaveElement(updateNews._id, updateNews);

	_news.push_back(updateNews._id);

}