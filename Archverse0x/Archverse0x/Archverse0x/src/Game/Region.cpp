#include "Game\Player.h"
#include "Game\Archverse.h"
#include "Game\Region.h"
#include "Game\Territory.h"
#include "Game\GameInfo.h"
#include "System\SerializationUtilitys.h"
#include "System\DBCore.h"
#include <boost\foreach.hpp>
#include <boost\lexical_cast.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;
using boost::uint8_t;
using std::vector;
using std::pair;
using boost::lexical_cast;

bool RegionStorage_BDB::serialize(vector<uint8_t> &destination, const ArchverseCore::Game::Region &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;

	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._territories));

	SerializationUtilitys::WriteSerializationPairs(pairs, destination);

	return true;
}

bool RegionStorage_BDB::deserialize(const uint8_t* source, const size_t sourceSize, ArchverseCore::Game::Region &destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._name, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._name, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._territories, source + totalSize);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

TerritoryList Region::Territories()
{
	return TerritoryList(_territories);
}

string Region::NameForNewTerritory()
{
	return _name + "-" + lexical_cast<string>(_territories.size());
}
bool RegionTable::RandomRegionForPlacement(ArchverseCore::Game::Region &destination)
{
	int regionCount = GAMEINFO.RegionCount();
	boost::minstd_rand rng;
	boost::uniform_int<> distribution(0,regionCount - 1);
	boost::variate_generator<boost::minstd_rand&, boost::uniform_int<> >
		randomGenerator(rng, distribution);             // glues randomness with mapping
	return LoadElement(randomGenerator(), destination) == 0;
}

bool RegionTable::RegionFromId(uint32_t regionId, ArchverseCore::Game::Region &destination)
{
	return LoadElement(regionId, destination) == 0;
}

RegionTable::RegionTable()
{
	for(int i = 0 ; i < STARTING_REGIONS; i++)
	{
		Region region;
		region._name = lexical_cast<string>(i);
		region._id = i;
		SaveElement(i, region);
	}
}