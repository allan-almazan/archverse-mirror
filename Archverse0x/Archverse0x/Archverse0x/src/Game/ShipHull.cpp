#include"Game/ShipHull.h"
#include"Game/Prerequisite.h"
#include"System/json/json_spirit.h"

#include<boost/lexical_cast.hpp>
#include<boost/foreach.hpp>

using namespace ArchverseCore::Game;
using namespace json_spirit;
using boost::lexical_cast;
using std::string;

void ShipHullPresentation_JSON::ShipHullOverview(ShipHull& hull, Object& destination)
{
	destination.push_back(Pair("Id", lexical_cast<string>(hull._id)));
	destination.push_back(Pair("Size", lexical_cast<string>(hull._size)));
	destination.push_back(Pair("BaseHP", lexical_cast<string>(hull._baseHp)));
	destination.push_back(Pair("BaseShield", lexical_cast<string>(hull._baseShield)));
	destination.push_back(Pair("BaseCost", lexical_cast<string>(hull._baseCost)));
	destination.push_back(Pair("Name", lexical_cast<string>(hull._name)));
	destination.push_back(Pair("Description", lexical_cast<string>(hull._description)));

	destination.push_back(Pair("WeaponSlots", Array()));
	Array& weaponSlots = destination.back().value_.get_array();
	BOOST_FOREACH(WeaponSlot& slot, hull._weaponSlots)
	{
		weaponSlots.push_back(Object());
		Object& element = weaponSlots.back().get_obj();
		element.push_back(Pair("Direction", lexical_cast<string>(slot._direction)));
		element.push_back(Pair("Type", WeaponSlot::WeaponTypeString(slot._type)));
		element.push_back(Pair("MaxSize", lexical_cast<string>(slot._maxSize)));
	}
	PrerequisitePresentation_JSON::PrerequisiteListOverview(PrerequisiteList(hull._prerequisites), destination);

}

string WeaponSlot::WeaponTypeString(ArchverseCore::Game::WeaponSlot::WeaponSlotType slotType)
{
	switch(slotType)
	{
	case WeaponSlot::FIXED:
		return "Fixed";
	case WeaponSlot::TURRET:
		return "Turret";
	default:
		return "Unknown";
	}
}