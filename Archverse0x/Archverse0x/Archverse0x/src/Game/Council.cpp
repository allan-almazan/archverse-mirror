#include "Game\Player.h"
#include "Game\Archverse.h"
#include "Game\Council.h"
#include "Game\GameInfo.h"
#include "Game\Region.h"
#include "Game\ControlModel.h"
#include "System\SerializationUtilitys.h"
#include "System\DBCore.h"
#include "Game\AdmissionRequest.h"
#include <boost\foreach.hpp>
#include <boost\lexical_cast.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include<boost\cstdint.hpp>

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;
using boost::uint8_t;
using boost::uint32_t;
using std::vector;
using std::pair;

void Council::RemovePlayer(const uint32_t& playerId)
{
	std::remove(_players.begin(), _players.end(), playerId);
}

AdmissionRequestList Council::PlayerAdmissionRequests()
{
	return AdmissionRequestList(_admissionRequests);
}

void Council::AdmisionRequest(uint32_t playerId, const string& messageBody)
{
	AdmissionRequest request;
	ADMISSION_REQUEST_TABLE.CreateAdmissionRequest(_id, playerId, messageBody, request);

	COUNCIL_TABLE.LockedTransaction(_id, [&](Council& council) -> bool
	{
		council.PlayerAdmissionRequests().AddRequest(request.Id());
		return true;
	});
}

MessageBox<Council, CouncilTable> Council::Messages()
{
	return MessageBox<Council, CouncilTable>(_sentMessages, _recivedMessages, COUNCIL_MESSAGE_TABLE, COUNCIL_TABLE);
}

RelationList Council::Relations()
{
	return RelationList(_relations, COUNCIL_RELATION_TABLE);
}

void (Council::*Council::DiplomaticStateSetter(Relation relation, bool initiate))(const uint32_t& relationId)
{
	switch(relation)
	{
	case RELATION_NONE:
		{
			return &ClearRelation;
		}
	case RELATION_ALLY:
		{
			return &EnterAlliance;
		}
	case RELATION_TRADE:
		{
			return &EnterTrade;
		}
	case RELATION_TRUCE:
		{
			return &EnterTruce;
		}
	case RELATION_WAR:
		{
			if(initiate)
				return &DeclareWar;
			else
				return &EnterWar;
		}
	case RELATION_TOTAL_WAR:
		{
			if(initiate)
				return &DeclareTotalWar;
			else
				return &EnterTotalWar;
		}
	case RELATION_SUBORDINARY:
		{
			if(initiate)
				return &DominateCouncil;
			else
				return &EnterSubordinary;
		}
	default:
		return NULL;
	}
}

bool CouncilStorage_BDB::serialize(vector<uint8_t> &destination, const ArchverseCore::Game::Council &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;

	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._motto));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._actions));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._players));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._sentMessages));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._recivedMessages));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._relations));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._admissionRequests));

	SerializationUtilitys::WriteSerializationPairs(pairs, destination);

	return true;
}

bool CouncilStorage_BDB::deserialize(const uint8_t* source, const size_t sourceSize, ArchverseCore::Game::Council &destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._name, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._name, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._motto, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._actions, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._players, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._sentMessages, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._recivedMessages, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._relations, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._admissionRequests, source + totalSize);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool CouncilTable::CouncilById(uint32_t councilId, Council& destination)
{
	return LoadElement(councilId, destination) == 0;
}
void CouncilTable::CreateNewCouncil(const string& name, const string& motto, uint32_t founderId, Council& destination)
{
	do
	{
		destination._id = GAMEINFO.IncrementCouncilID();
	}while(Exists(destination._id));

	destination._name = name;
	destination._motto = motto;
	destination._speakerId = founderId;
	destination._players.push_back(founderId);
	AddCouncil(destination);
}
void CouncilTable::AddCouncil(Council& council)
{
	GAMEINFO.LockedTransaction(1, [&](GameInfoData& t) -> bool
	{
		GameInfo::Councils(t)._councils.push_back(council._id);
		return true;
	});
	SaveElement(council._id, council);
}
void CouncilTable::CreateNewCouncil(Council& destination)
{
	do
	{
		destination._id = GAMEINFO.IncrementCouncilID();
	}while(Exists(destination._id));

	destination._name = "#" + lexical_cast<string>(destination._id);
	destination._speakerId = -1;
	AddCouncil(destination);
}
void CouncilTable::RandomCouncil(Council& destination)
{
	vector<uint32_t> allCouncils = GAMEINFO.Councils()._councils;
	vector<uint32_t> options;
	Council tmpCouncil;
	BOOST_FOREACH(uint32_t councilId, allCouncils)
	{
		if(CouncilById(councilId, tmpCouncil))
		{
			if(tmpCouncil._acceptingNewPlayers && tmpCouncil._players.size() < Council::MAX_COUNCIL_SIZE)
				options.push_back(tmpCouncil._id);
		}
	}
	if(options.size() > 1)
	{
		boost::minstd_rand rng;
		boost::uniform_int<> distribution(0,options.size() - 1);
		boost::variate_generator<boost::minstd_rand&, boost::uniform_int<> >
			randomGenerator(rng, distribution);             // glues randomness with mapping
		CouncilById(options[randomGenerator()], destination);

	}
	else if(options.size() == 1)
	{
		CouncilById(options[0], destination);
	}
	else
	{
		CreateNewCouncil(destination);
	}
}

PlayerList Council::Players()
{
	return PlayerList(_players);
}

void CouncilPresentation_JSON::CouncilOverview(Council& council, Object& destination)
{
	destination.push_back(Pair("Id", lexical_cast<string>(council._id)));
	Player speaker;
	PLAYER_TABLE.PlayerById(council.SpeakerId(), speaker);
	destination.push_back(Pair("Speaker", speaker.Name()));
	destination.push_back(Pair("Power", lexical_cast<string>(council._power)));
	destination.push_back(Pair("Name", council._name));
	destination.push_back(Pair("Motto", council._motto));
	destination.push_back(Pair("Players", Array()));
	Array& players = destination.back().value_.get_array();
	BOOST_FOREACH(uint32_t playerId, council._players)
	{
		Player member;
		if(!PLAYER_TABLE.PlayerById(playerId, member))
			throw exception("bad member id");

		players.push_back(Object());
		Object& element = players.back().get_obj();
		PlayerPresentation_JSON::PlayerOverview(member, element);
	}
	
}

void CouncilPresentation_JSON::CouncilMinimalOverview(Council& council, Object& destination)
{
	destination.push_back(Pair("Id", lexical_cast<string>(council._id)));
	Player speaker;
	PLAYER_TABLE.PlayerById(council.SpeakerId(), speaker);
	destination.push_back(Pair("Speaker", speaker.Name()));
	destination.push_back(Pair("Power", lexical_cast<string>(council._power)));
	destination.push_back(Pair("Name", council._name));
	destination.push_back(Pair("Motto", council._motto));
	destination.push_back(Pair("MemberCount", lexical_cast<string>(council._players.size())));
}

void CouncilPresentation_JSON::CouncilMemberDiplomaticStatus(Council& council, Player& relation, Object& destination)
{
	BOOST_FOREACH(uint32_t playerId, council._players)
	{
		Player member;
		if(!PLAYER_TABLE.PlayerById(playerId, member))
			throw exception("invalid playerId");

		destination.push_back(Pair("Players", Array()));
		Array& players = destination.back().value_.get_array();
		players.push_back(Object());
		PlayerPresentation_JSON::PlayerDiplomaticStatus(member, relation, players.back().get_obj());
	}
}