#include "Game\Player.h"
#include "Game\Archverse.h"
#include "Game\GameInfo.h"
#include "Game\Council.h"
#include "System\SerializationUtilitys.h"
#include "System\DBCore.h"
#include "Game\AdmissionRequest.h"
#include <boost\foreach.hpp>
#include <boost\lexical_cast.hpp>
#include <boost\random\linear_congruential.hpp>
#include <boost\random\uniform_int.hpp>
#include <boost\random\variate_generator.hpp>
#include <vector>
#include <string>
#include<boost\cstdint.hpp>

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;
using boost::uint8_t;
using boost::uint32_t;
using std::vector;
using std::pair;
using std::string;

void AdmissionRequestTable::CreateAdmissionRequest(uint32_t councilId, uint32_t playerId, 
												   const string& message, AdmissionRequest& destination)
{
	destination._councilId = councilId;
	destination._playerId = playerId;
	destination._message = message;
	do
	{
		destination._id = GAMEINFO.IncrementAdmissionID();
	}while(Exists(destination._id));
	SaveElement(destination._id, destination);
}

void AdmissionRequestList::AddRequest(uint32_t admissionRequestId)
{
	_admissionRequests.push_back(admissionRequestId);
}
bool AdmissionRequestStorage_BDB::deserialize(const boost::uint8_t *source, const size_t sourceSize, ArchverseCore::Game::AdmissionRequest &destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._message, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._message, source + totalSize);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool AdmissionRequestStorage_BDB::serialize(std::vector<uint8_t> &destination, const ArchverseCore::Game::AdmissionRequest &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._message));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._message));
	SerializationUtilitys::WriteSerializationPairs(pairs, destination);
	return true;
}


void AdmissionRequestPresentation_JSON::AdmissionRequestOverview(Council& council, Object& destination)
{
	destination.push_back(Pair("Requests", Array()));
	Array& requestArray = destination.back().value_.get_array();
	BOOST_FOREACH(uint32_t admissionId, council.PlayerAdmissionRequests()._admissionRequests)
	{
		requestArray.push_back(Object());
		Object& element = requestArray.back().get_obj();
		AdmissionRequest request;
		if(ADMISSION_REQUEST_TABLE.LoadElement(admissionId, request) != 0)
			throw exception("bad admission request id");
		Player player;
		if(!PLAYER_TABLE.PlayerById(request.PlayerId(), player))
			throw exception("bad player id");

		element.push_back(Pair("Id", lexical_cast<string>(request.Id())));
		element.push_back(Pair("PlayerName", player.Name()));
		element.push_back(Pair("Message", request.Message()));

	}
	 
}
void AdmissionRequestPresentation_JSON::AdmissionRequestDetail(uint32_t admissionId, Object& destination)
{
}
		