#include "Game/Event.h"
#include "Game/Prerequisite.h"
#include "Game/Archverse.h"

#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>

using namespace ArchverseCore::Game;
using std::string;
using std::vector;
using boost::uint32_t;
using boost::lexical_cast;
using namespace json_spirit;

Event& EventTable::EventFromId(boost::uint32_t eventId)
{
	return *_events.at(eventId);
}

Event& EventTable::CreateEvent(const string& name, const string& description, const vector<uint32_t>& prerequisites)
{
	_eventStorage.push_back(Event());
	Event& evnt = _eventStorage.back();
	evnt._id = _events.size();
	evnt._prerequisites = prerequisites;
	evnt._name = name;
	_events.push_back(&evnt);
	return evnt;
}

void EventPresentation_JSON::EventOverview(Event& event, Object& destination)
{
	destination.push_back(Pair("Id", lexical_cast<string>(event._id)));
	destination.push_back(Pair("Name", event._name));
	destination.push_back(Pair("Description", event._description));
	PrerequisitePresentation_JSON::PrerequisiteListOverview(event.Prerequisites(), destination);
}

void EventPresentation_JSON::EventListOverview(EventList& eventList, Object& destination)
{
	destination.push_back(Pair("Events", Array()));
	Array& events = destination.back().value_.get_array();
	BOOST_FOREACH(uint32_t eventId, eventList._events)
	{
		events.push_back(Object());
		Object& element = events.back().get_obj();
		Event& event = EVENT_TABLE.EventFromId(eventId);
		EventOverview(event, element);
	}

}

PrerequisiteList Event::Prerequisites()
{
	return PrerequisiteList(_prerequisites);
}

