#include"Game/ShipDesign.h"
#include"System/json/json_spirit.h"
#include"System/SerializationUtilitys.h"
#include"Game/Archverse.h"

#include<boost/lexical_cast.hpp>
#include<boost/foreach.hpp>
#include<exception>

using namespace ArchverseCore::Game;
using namespace json_spirit;
using boost::lexical_cast;
using std::string;
using std::exception;

bool ShipDesignTable::ShipDesignFromId(uint32_t id, ArchverseCore::Game::ShipDesign &destination)
{
	return LoadElement(id, destination) == 0;
}

bool ShipDesignStorage_BDB::deserialize(const boost::uint8_t *source, const size_t sourceSize, ArchverseCore::Game::ShipDesign &destination)
{
	using ArchverseCore::System::SerializationUtilitys;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._name, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._name, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._weapons, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._devices, source + totalSize);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool ShipDesignStorage_BDB::serialize(std::vector<uint8_t> &destination, const ArchverseCore::Game::ShipDesign &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._weapons));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._devices));
	SerializationUtilitys::WriteSerializationPairs(pairs, destination);
	return true;
}

void ShipDesignPresentation_JSON::ShipDesignListOverview(ArchverseCore::Game::ShipDesignList &designList, json_spirit::Object &destination)
{
	destination.push_back(Pair("ShipDesigns", Array()));
	Array& shipDesigns = destination.back().value_.get_array();
	BOOST_FOREACH(uint32_t elementId, designList._designs)
	{
		shipDesigns.push_back(Object());
		ShipDesign element;
		if(!SHIP_DESIGN_TABLE.ShipDesignFromId(elementId, element))
			throw exception("invalid shipDesignId");

		ShipDesignOverview(element, shipDesigns.back().get_obj());
	}
}

void ShipDesignPresentation_JSON::ShipDesignOverview(ArchverseCore::Game::ShipDesign &design, json_spirit::Object &destination)
{
	/*uint32_t _id;
	uint32_t _ownerId;
	uint32_t _hullType;
	uint32_t _shieldId;
	uint32_t _computerId;
	uint32_t _armorType;
	uint32_t _currentPool;
	uint32_t _cost;
	string _name;*/
	destination.push_back(Pair("Id", lexical_cast<string>(design._id)));
	destination.push_back(Pair("OwnerId", lexical_cast<string>(design._ownerId)));
	destination.push_back(Pair("Hull", lexical_cast<string>(design._hullId)));
	destination.push_back(Pair("Shield", lexical_cast<string>(design._shieldId)));
	destination.push_back(Pair("Computer", lexical_cast<string>(design._computerId)));
	destination.push_back(Pair("Armor", lexical_cast<string>(design._armorId)));
	destination.push_back(Pair("CurrentPool", lexical_cast<string>(design._currentPool)));
	destination.push_back(Pair("Cost", lexical_cast<string>(design._cost)));
	destination.push_back(Pair("Name", design._name));
}