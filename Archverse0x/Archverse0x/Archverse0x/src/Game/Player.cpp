#include "Game\Player.h"
#include "Game\Archverse.h"
#include "Game\UserAccount.h"
#include "Game\Territory.h"
#include "Game\Council.h"
#include "Game\GameInfo.h"
#include "Game\Tech.h"
#include "System\SerializationUtilitys.h"
#include "System\DBCore.h"
#include "Game\Message.h"
#include "Game\Race.h"
#include "Game\News.h"
#include "Game\Project.h"
#include "Game\Event.h"
#include <boost\foreach.hpp>
#include <boost\lexical_cast.hpp>
#include<boost\cstdint.hpp>
#include <algorithm>

using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;
using boost::uint8_t;
using std::vector;
using std::pair;
using std::find;
using namespace json_spirit;
using boost::lexical_cast;

RelationList Player::Relations()
{
	return RelationList(_relations, PLAYER_RELATION_TABLE);
}
TerritoryList Player::Territories()
{
	return TerritoryList(_territories);
}
TechList Player::KnownTechs()
{
	return TechList(_knownTechs);
}
NewsList Player::CurrentNews()
{
	return NewsList(_news);
}

MessageBox<Player, PlayerTable> Player::Messages()
{
	return MessageBox<Player, PlayerTable>(_recivedMessages, _sentMessages, PLAYER_MESSAGE_TABLE, PLAYER_TABLE);
}

void Player::JoinCouncil(const uint32_t& councilId)
{
	COUNCIL_TABLE.LockedSetterUpdate(_council, &Council::RemovePlayer, _id);
	_council = councilId;
	//note that we dont actualy join the council here, we just prepare to join the council
}


void (Player::*Player::DiplomaticStateSetter(Relation relation, bool initiate))(const uint32_t& relationId)
{
	switch(relation)
	{
	case RELATION_NONE:
		{
			return &ClearRelation;
		}
	case RELATION_ALLY:
		{
			return &EnterAlliance;
		}
	case RELATION_TRADE:
		{
			return &EnterTrade;
		}
	case RELATION_TRUCE:
		{
			return &EnterTruce;
		}
	case RELATION_WAR:
		{
			if(initiate)
				return &DeclareWar;
			else
				return &EnterWar;
		}
	case RELATION_HOSTILE:
		{
			if(initiate)
				return &DeclareHostilities;
			else
				return &EnterHostilities;
		}
	default:
		return NULL;
	}
}

bool PlayerTable::PlayerByAuthString(const string& authString, Player& destination)
{
	UserAccount user;
	if( USER_ACCOUNT_TABLE.UserAccountFromAuthString(authString, user))
	{
		return PlayerById(user.PlayerId(), destination);
	}
	else
		return false;
}
bool PlayerTable::CreatePlayer(UserAccount& user, const string& playerName, const uint32_t raceId, Player& destination)
{
	destination._name = playerName;
	do
	{
		destination._id = GAMEINFO.IncrementPlayerID();
	}while(Exists(destination._id));

	Territory homeTerritory;
	Council startingCouncil;
	if(!TERRITORY_TABLE.CreateNewHomeTerritory(destination._race, homeTerritory))
		return false;
	destination.Territories().AddTerritory(homeTerritory);
	COUNCIL_TABLE.RandomCouncil(startingCouncil);
	destination._council = startingCouncil.Id();
	destination._race = raceId;

	COUNCIL_TABLE.LockedTransaction(startingCouncil.Id(), [&](Council& t) -> bool
	{
		t.Players().AddPlayer(destination._id);
		return true;
	});

	if(SaveElement(destination._id, destination) == 0)
	{
		user.PlayerId(destination._id);
		return USER_ACCOUNT_TABLE.LockedSetterUpdate(user.UserName(), &UserAccount::PlayerId, destination._id) &&
			TERRITORY_TABLE.LockedSetterUpdate(homeTerritory.Id(), &Territory::OwnerId, destination._id);
	}
	else
		return false;
}

bool PlayerTable::PlayerById(uint32_t playerId, Player& destination)
{
	return LoadElement(playerId, destination) == 0;
}
bool PlayerTable::PlayerByAccount(const string& accountName, Player& destination)
{
	UserAccount user;
	if( USER_ACCOUNT_TABLE.UserAccountFromName(accountName, user))
	{
		return PlayerById(user.PlayerId(), destination);
	}
	else
		return false;
}
bool PlayerTable::PlayerByName(const string& name, Player& destination)
{
	return false;
}
bool PlayerTable::RemovePlayer(uint32_t playerId)
{
	return false;
}
void PlayerPresentation_JSON::PlayerOverview(Player& player, Object& destination)
{
	Council council;
	if(!COUNCIL_TABLE.CouncilById(player.CouncilId(), council))
		throw exception("bad councilId");

	PlayerOverview(player, council, destination);
}
void PlayerPresentation_JSON::PlayerOverview(Player& player, const Council& council, Object& destination)
{
	destination.push_back(Pair("Name", player._name));
	destination.push_back(Pair("Id", lexical_cast<string>(player._id)));
	destination.push_back(Pair("TerritoryCount", lexical_cast<string>(player._territories.size())));
	destination.push_back(Pair("Valor", lexical_cast<string>(player._valor)));
	destination.push_back(Pair("Turn", lexical_cast<string>(player._turn)));
	destination.push_back(Pair("MP", lexical_cast<string>(player._currentMP)));
	destination.push_back(Pair("KP", lexical_cast<string>(player._currentKP)));
	destination.push_back(Pair("PP", lexical_cast<string>(player._currentPP)));
	destination.push_back(Pair("RP", lexical_cast<string>(player._currentRP)));
	destination.push_back(Pair("Race", RACE_TABLE.RaceFromId(player._race).Name()));
	destination.push_back(Pair("Council", council.Name()));
	destination.push_back(Pair("Rank", lexical_cast<string>(player._rank)));

	Territory homeTerritory;
	if(!TERRITORY_TABLE.TerritoryFromId(player._territories[0], homeTerritory))
	{
		throw exception("bad territoryId");
	}
	destination.push_back(Pair("HomeTerritory", homeTerritory.Name()));
	//in this case we arent going to modify the news list but it cant be const
	NewsPresentation_JSON::NewsListOverview(NewsList(player._news), destination);
	EventPresentation_JSON::EventListOverview(EventList(player._events), destination);
}

void PlayerPresentation_JSON::PlayerDiplomaticStatus(Player& player, const Player& relation, Object& destination)
{
	Player voteTarget;
	//if this is an invalid target the name will just be blank so dont worry about it
	PLAYER_TABLE.PlayerById(player._councilVote, voteTarget);
	Relationship relationship;
	RelationList relationList = player.Relations();
	relationList.RelationTo(relation.Id(), false, relationship);

	destination.push_back(Pair("Name", player._name));
	destination.push_back(Pair("Power", lexical_cast<string>(player._power)));
	destination.push_back(Pair("Relationship", relationship.RelationText()));
	destination.push_back(Pair("VotePower", lexical_cast<string>(player._votePower)));
	destination.push_back(Pair("VotingFor", voteTarget.Name()));
}




bool PlayerStorage_BDB::serialize(vector<uint8_t> &destination, const ArchverseCore::Game::Player &source)
{
	using ArchverseCore::System::SerializationUtilitys;

	vector<pair<const void*, size_t> > pairs;

	pairs.push_back(SerializationUtilitys::BuildSerializationPair(&source._id, &source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._name));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._admirals));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._battlePlans));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._recivedMessages));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._sentMessages));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._events));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._news));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._territories));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._relations));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._abilities));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._shipDesigns));
	pairs.push_back(SerializationUtilitys::BuildSerializationPair(source._knownTechs));

	SerializationUtilitys::WriteSerializationPairs(pairs, destination);

	return true;
}

bool PlayerStorage_BDB::deserialize(const uint8_t* source, const size_t sourceSize, ArchverseCore::Game::Player &destination)
{
	using ArchverseCore::System::SerializationUtilitys;
	using std::exception;

	size_t totalSize = 0;
	totalSize += SerializationUtilitys::DeserializePair(&destination._id, &destination._name, source);
	totalSize += SerializationUtilitys::DeserializePair(destination._name, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._admirals, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._battlePlans, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._recivedMessages, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._sentMessages, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._events, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._news, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._territories, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._relations, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._abilities, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._shipDesigns, source + totalSize);
	totalSize += SerializationUtilitys::DeserializePair(destination._knownTechs, source + totalSize);
	if(totalSize != sourceSize)
	{
		exception ex("deserialization error: source size differs from destination size");
		throw ex;
	}
	return true;
}

bool PlayerList::AddPlayer(ArchverseCore::Game::Player &source)
{
	_players.push_back(source._id);
	return true;
}
void PlayerList::AddPlayer(const uint32_t& source)
{
	_players.push_back(source);
}

bool PlayerList::ContainsPlayer(uint32_t playerId)
{
	return find(_players.begin(), _players.end(), playerId) != _players.end();
}

uint32_t Player::ValueForPrerequisite(ArchverseCore::Game::Prerequisite::RequisiteType reqType)
{
	switch(reqType)
	{
	case Prerequisite::RT_RACE:
		return _race;
	case Prerequisite::RT_TERRITORY_COUNT:
		return _territories.size();
	case Prerequisite::RT_TECH:
		return _techGoal;
	case Prerequisite::RT_TECH_COUNT:
		return _knownTechs.size();
	case Prerequisite::RT_BUILDING_COUNT:
		return 0;
	case Prerequisite::RT_POWER:
		return _power;
	case Prerequisite::RT_RANK:
		return _rank;
	case Prerequisite::RT_REGION:
		return 0;
	case Prerequisite::RT_COMMANDER_LEVEL:
		return 0;
	case Prerequisite::RT_FLEET:
		return 0;
	case Prerequisite::RT_RP:
		return _currentRP;
	case Prerequisite::RT_MP:
		return _currentMP;
	case Prerequisite::RT_KP:
		return _currentKP;
	case Prerequisite::RT_PP:
		return _currentPP;
	case Prerequisite::RT_HAS_SHIP:
		return 0;
	case Prerequisite::RT_SHIP_POOL:
		return 0;
	case Prerequisite::RT_POPULATION:
		return 0;
	case Prerequisite::RT_GOVERNMENT_MODE:
		return _government;
	case Prerequisite::RT_TITLE:
		return 0;
	case Prerequisite::RT_COUNCIL_SPEAKER:
		return 0;
	case Prerequisite::RT_COUNCIL_WAR:
		return 0;
	case Prerequisite::RT_WAR_IN_COUNCIL:
		return 0;
	case Prerequisite::RT_VALOR:
		return _valor;

	case Prerequisite::CM_POPULATION_GROWTH:
		return _controlModel._populationGrowth;
	case Prerequisite::CM_POPULATION_DENSITY:
		return _controlModel._populationDensity;
	case Prerequisite::CM_LIFE_TECH_RESEARCH:
		return _controlModel._lifeTechResearch;
	case Prerequisite::CM_SOCIAL_TECH_RESEARCH:
		return _controlModel._socialTechResearch;
	case Prerequisite::CM_INFO_TECH_RESEARCH:
		return _controlModel._infoTechResearch;
	case Prerequisite::CM_MATTER_ENERGY_TECH_RESEARCH:
		return _controlModel._matterEnergyTechResearch;
	case Prerequisite::CM_RACIAL_TECH_RESEARCH:
		return _controlModel._racialTechResearch;
	case Prerequisite::CM_RESEARCH:
		return _controlModel._research;
	case Prerequisite::CM_COMMAND_AND_CONTROL:
		return _controlModel._commandAndControl;
	case Prerequisite::CM_SHIP_PRODUCTION:
		return _controlModel._production;
	case Prerequisite::CM_SHIP_STRENGTH:
		return _controlModel._shipStrength;
	case Prerequisite::CM_MORALE:
		return _controlModel._morale;
	case Prerequisite::CM_SURVIVAL:
		return _controlModel._survival;
	case Prerequisite::CM_BERZERKER:
		return _controlModel._berzerker;
	case Prerequisite::CM_ADMIRALTY:
		return _controlModel._admiralty;
	case Prerequisite::CM_MINING:
		return _controlModel._mining;
	case Prerequisite::CM_PRODUCTION:
		return _controlModel._production;
	case Prerequisite::CM_EFFICIANCY:
		return _controlModel._efficiency;
	case Prerequisite::CM_COMMERCE:
		return _controlModel._commerce;
	case Prerequisite::CM_DIPLOMACY:
		return _controlModel._diplomacy;
	}
}

bool Player::Update(ArchverseCore::Game::Council &council, ArchverseCore::Game::ControlModel &councilControlModel, uint32_t tick)
{
	//determine if we need processing or not
	uint32_t nextTurnTick = (_turn + 1) * Archverse::TICKS_IN_TURN;
	if(tick >= nextTurnTick)
	{
		_turn++;
		_lastTurnMP = _currentMP;
		_lastTurnRP = _currentRP;
		_lastTurnKP = _currentKP;
		_lastTurnPP = _currentPP;
		_power = 0;
		_population = 0;
		//////////////////////////////////////////////////////////////////////////
		// build control model
		//////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////
		// run actions
		//////////////////////////////////////////////////////////////////////////


		// pay fleet upkeep
		// process research
		// process fleet building
		// check protected mode
		return true;
	}
	else
		return false;
}
void Player::FinishTerritoryUpdate(boost::uint32_t pp, boost::uint32_t rp, boost::uint32_t kp, boost::uint32_t mp, boost::uint32_t population, boost::uint32_t power)
{
	_currentMP += mp;
	_currentRP += rp;
	_currentKP += kp;
	_currentPP += pp;
	_power += power;
	_population += population;
}

EventList Player::Events()
{
	return EventList(_events);
}
void Player::Actions(ArchverseCore::Game::ActionList &destination)
{
	if(!ACTION_TABLE.ActionById(_actionsId, destination))
		throw exception("failed to load actionList for player");
}

void Player::ExpeditionTerritory(int luckRoll)
{
	Territory territory;
	TERRITORY_TABLE.CreateNewTerritory(territory, luckRoll);
	this->_territories.push_back(territory.Id());

}