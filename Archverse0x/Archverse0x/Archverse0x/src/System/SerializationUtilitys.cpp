#include"System/SerializationUtilitys.h"

#include<utility>
#include<boost/foreach.hpp>

using namespace ArchverseCore::System;
using boost::uint32_t;
using std::string;
using std::vector;
using std::pair;

pair<const void*, size_t> SerializationUtilitys::BuildSerializationPair(const void* start, const void* end)
{
	return pair<const void*, size_t>(start, reinterpret_cast<size_t>(end) - reinterpret_cast<size_t>(start));
}

pair<const void*, size_t> SerializationUtilitys::BuildSerializationPair(const string& member)
{
	return pair<const void*, size_t>(member.c_str(), member.size());
}

pair<const void*, size_t> SerializationUtilitys::BuildSerializationPair(const vector<uint32_t>& member)
{
	if(member.size() > 0)
	{
		return pair<const void*, size_t>(&member.at(0), (member.size() * sizeof(uint32_t)));
	}
	else
	{
		return pair<const void*, size_t>(NULL, 0);
	}
}
void SerializationUtilitys::WriteSerializationPairs(vector<pair<const void*, size_t> >& pairs, vector<uint8_t>& buffer)
{
	typedef pair<const void*, size_t> serialization_type_t;
	size_t totalSize = 0;

	BOOST_FOREACH(serialization_type_t& pair, pairs)
	{
		totalSize += sizeof(pair.second);
		totalSize += pair.second;

	}
	buffer.resize(totalSize, 0);
	size_t position = 0;

	BOOST_FOREACH(serialization_type_t& pair, pairs)
	{
		if(pair.second > 0 && pair.first != NULL)
		{
			memcpy(&buffer[position], &pair.second, sizeof(pair.second) );
			position += sizeof(pair.second);
			memcpy(&buffer[position], pair.first, pair.second );
			position += pair.second;
		}
		else
		{
			memcpy(&buffer[position], &pair.second, sizeof(pair.second) );
			position += sizeof(pair.second);
		}
	}
}

int SerializationUtilitys::DeserializePair(void* start, const void* end, const uint8_t* bufPtr)
{
	size_t pairLength = 0;
	memcpy(&pairLength, bufPtr,sizeof(pairLength));
	bufPtr += sizeof(pairLength);
	if(pairLength != reinterpret_cast<size_t>(end) - reinterpret_cast<size_t>(start))
	{
		using std::exception;
		exception badSerialization("bad serialization data: pair length doesnt match");
		throw badSerialization;
	}
	memcpy(start, bufPtr, pairLength);
	return pairLength + sizeof(pairLength);
}
int SerializationUtilitys::DeserializePair(string& member, const uint8_t* bufPtr)
{
	size_t pairLength = 0;
	memcpy(&pairLength, bufPtr,sizeof(pairLength));
	bufPtr += sizeof(pairLength);
	if(pairLength > 0)
	{
		member.clear();
		member.resize(pairLength / sizeof(char));	
		memcpy(&member[0], bufPtr,pairLength);
	}
	return pairLength + sizeof(pairLength);
}
int SerializationUtilitys::DeserializePair(vector<uint32_t>& member, const uint8_t* bufPtr)
{
	size_t pairLength = 0;
	memcpy(&pairLength, bufPtr,sizeof(pairLength));
	bufPtr += sizeof(pairLength);
	if(pairLength > 0)
	{
		member.clear();
		member.resize(pairLength / sizeof(uint32_t));	
		memcpy(&member[0], bufPtr, pairLength);
	}
	return pairLength + sizeof(pairLength);
}