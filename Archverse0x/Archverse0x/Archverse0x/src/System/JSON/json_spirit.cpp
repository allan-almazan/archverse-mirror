#include "System/JSON/json_spirit.h"

#include <boost\bind.hpp>
#include <algorithm>

using std::string;
using namespace json_spirit;
using namespace boost;
using std::find_if;

bool same_name( const Pair& pair, const string& name )
{
	return pair.name_ == name;
}

const Value& find_value( const Object& obj, const string& name )
{
	Object::const_iterator i = find_if( obj.begin(), obj.end(), bind( same_name, _1, ref( name ) ) );

	if( i == obj.end() ) return Value::null;

	return i->value_;
}