

#include "System/http/MimeTypes.h"

using namespace ArchverseCore::System::HTTP;

struct hash_mapping
{
	const char* extension;
	const char* mime_type;
} hash_mappings[] =
{
	{ "gif", "image/gif" },
	{ "htm", "text/html" },
	{ "html", "text/html" },
	{ "jpg", "image/jpeg" },
	{ "png", "image/png" },
	{ 0, 0 } // Marks end of list.
};

std::string ExtensionToType(const std::string& extension)
{
	for (hash_mapping* m = hash_mappings; m->extension; ++m)
	{
		if (m->extension == extension)
		{
			return m->mime_type;
		}
	}

	return "text/plain";
}

