#include "System/http/server.h"
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>

using namespace ArchverseCore::System::HTTP;

Server::Server(const std::string& address, const std::string& port,
			   std::size_t thread_pool_size)
			   : _threadPoolSize(thread_pool_size),
			   _acceptor(_ioService),
			   _newConnection(new Connection(_ioService, *RequestHandler::GetRequestHandler()))
{
	// Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
	boost::asio::ip::tcp::resolver resolver(_ioService);
	boost::asio::ip::tcp::resolver::query query(address, port);
	boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);
	_acceptor.open(endpoint.protocol());
	_acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
	_acceptor.bind(endpoint);
	_acceptor.listen();
	_acceptor.async_accept(_newConnection->Socket(),
		boost::bind(&Server::HandleAccept, this,
		boost::asio::placeholders::error));
}

void Server::Run()
{
	// Create a pool of threads to run all of the io_services.
	std::vector<boost::shared_ptr<boost::thread> > threads;
	for (std::size_t i = 0; i < _threadPoolSize; ++i)
	{
		boost::shared_ptr<boost::thread> thread(new boost::thread(
			boost::bind(&boost::asio::io_service::run, &_ioService)));
		threads.push_back(thread);
	}

	// Wait for all threads in the pool to exit.
	for (std::size_t i = 0; i < threads.size(); ++i)
		threads[i]->join();
}

void Server::Stop()
{
	_ioService.stop();
}

void Server::HandleAccept(const boost::system::error_code& e)
{
	if (!e)
	{
		_newConnection->Start();
		_newConnection.reset(new Connection(_ioService, *RequestHandler::GetRequestHandler()));
		_acceptor.async_accept(_newConnection->Socket(),
			boost::bind(&Server::HandleAccept, this,
			boost::asio::placeholders::error));
	}
}
