#include "System/HTTP/RequestHandler.h"
#include "System/HTTP/RemoteProcedureCall.h"
#include "System/HTTP/MimeTypes.h"
#include "System/HTTP/Reply.h"
#include "System/HTTP/Request.h"
#include "System/JSON/json_spirit.h"

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <boost\lexical_cast.hpp>

using namespace std;
using namespace ArchverseCore::System::HTTP;
using namespace json_spirit;
using namespace boost;

void RequestHandler::AddRequestHandler(IRemoteProcedureCall* pageHandler)
{
	_pageHandlers.insert(std::pair<std::string, IRemoteProcedureCall*>(toLower(pageHandler->getPageName()), pageHandler));
}

void RequestHandler::HandleRequest(const Request& req, Reply& rep)
{
	ostringstream strStream;
	try
	{
		Value value;
		Object rootObj;
		if(read( req.postValue, value ))
		{
			rootObj = Object( value.get_obj() );
		}
		// Fill out the reply to be sent to the client.
		rep.status = Reply::ok;
		IRemoteProcedureCall* rpcCall = this->GetRPC(req.uri);
		if(rpcCall != NULL)
		{
			Object response;
			rpcCall->pageHandler(response, rootObj);
			write_formatted(response, strStream);
			//cout<<req.uri<<endl;
		}
		else
		{
			Object errorObj;
			errorObj.push_back( Pair( "error", "failed to find reqested page" ) );
			write_formatted( errorObj, strStream );
		}
		
	}
	catch(std::exception& ex)
	{
		Object errorObj;
		errorObj.push_back( Pair( "error", ex.what() ) );
		write_formatted( errorObj, strStream );
	}
	catch(...)
	{
		Object errorObj;
		errorObj.push_back( Pair( "error", "unknown" ) );
		write_formatted( errorObj, strStream );
	}
	rep.content = strStream.str();
	rep.headers.resize(2);
	rep.headers[0].name = "Content-Length";
	rep.headers[0].value = boost::lexical_cast<std::string>(rep.content.size());
	rep.headers[1].name = "Content-Type";
	rep.headers[1].value = "TEXT";
}

bool RequestHandler::url_decode(const std::string& in, std::string& out)
{
	out.clear();
	out.reserve(in.size());
	for (std::size_t i = 0; i < in.size(); ++i)
	{
		if (in[i] == '%')
		{
			if (i + 3 <= in.size())
			{
				int value;
				std::istringstream is(in.substr(i + 1, 2));
				if (is >> std::hex >> value)
				{
					out += static_cast<char>(value);
					i += 2;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else if (in[i] == '+')
		{
			out += ' ';
		}
		else
		{
			out += in[i];
		}
	}
	return true;
}

