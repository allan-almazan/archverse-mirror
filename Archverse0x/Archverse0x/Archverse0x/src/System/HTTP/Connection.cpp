#include "System/HTTP/connection.h"
#include "System/HTTP/RequestHandler.h"
#include "System/HTTP/RequestParser.h"

#include <vector>
#include <boost/bind.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/tuple/tuple.hpp>

using namespace ArchverseCore::System::HTTP;
using namespace boost;
using std::vector;
using namespace boost::logic;
using namespace boost::tuples;

Connection::Connection(boost::asio::io_service& io_service,
					   RequestHandler& handler)
					   : _strand(io_service),
					   _socket(io_service),
					   _requestHandler(handler)
{
}

boost::asio::ip::tcp::socket& Connection::Socket()
{
	return _socket;
}

void Connection::Start()
{
	
	_socket.async_read_some( boost::asio::buffer(_buffer),
		_strand.wrap(
		bind(&Connection::HandleRead, shared_from_this(),
		asio::placeholders::error,
		asio::placeholders::bytes_transferred)));
}

void Connection::HandleRead(const boost::system::error_code& e,
							std::size_t bytes_transferred)
{
	if (!e)
	{
		tribool result;
		tie(result, ignore) = _requestParser.Parse(
			_request, _buffer.data(), _buffer.data() + bytes_transferred);

		if (result)
		{
			_requestHandler.HandleRequest(_request, _reply);
			boost::asio::async_write(_socket, _reply.ToBuffers(),
				_strand.wrap(
				bind(&Connection::HandleWrite, shared_from_this(),
				boost::asio::placeholders::error)));
		}
		else
		{
			_socket.async_read_some( boost::asio::buffer(_buffer),
				_strand.wrap(
				bind(&Connection::HandleRead, shared_from_this(),
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred)));
		}
	}

	// If an error occurs then no new asynchronous operations are started. This
	// means that all shared_ptr references to the connection object will
	// disappear and the object will be destroyed automatically after this
	// handler returns. The connection class's destructor closes the socket.
}

void Connection::HandleWrite(const boost::system::error_code& e)
{
	if (!e)
	{
		// Initiate graceful connection closure.
		boost::system::error_code ignored_ec;
		_socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ignored_ec);
	}

	// No new asynchronous operations are started. This means that all shared_ptr
	// references to the connection object will disappear and the object will be
	// destroyed automatically after this handler returns. The connection class's
	// destructor closes the socket.
}

