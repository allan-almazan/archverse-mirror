#include "System/HTTP/RequestParser.h"
#include "System/HTTP/Request.h"
#include "System/HTTP/HTMLUtilitys.h"

using namespace ArchverseCore::System::HTTP;

RequestParser::RequestParser()
: _state(method_start)
{
}

void RequestParser::Reset()
{
	_state = method_start;
}

boost::tribool RequestParser::Consume(Request& req, char input)
{
	switch (_state)
	{
	case method_start:
		if (!is_char(input) || is_ctl(input) || is_tspecial(input))
		{
			return false;
		}
		else
		{
			_state = method;
			req.method.push_back(input);
			return boost::indeterminate;
		}
	case method:
		if (input == ' ')
		{
			_state = uri;
			return boost::indeterminate;
		}
		else if (!is_char(input) || is_ctl(input) || is_tspecial(input))
		{
			return false;
		}
		else
		{
			req.method.push_back(input);
			return boost::indeterminate;
		}
	case uri_start:
		if (is_ctl(input))
		{
			return false;
		}
		else
		{
			_state = uri;
			req.uri.push_back(input);
			return boost::indeterminate;
		}
	case uri:
		if (input == ' ')
		{
			_state = http_version_h;
			return boost::indeterminate;
		}
		else if (is_ctl(input))
		{
			return false;
		}
		else
		{
			req.uri.push_back(input);
			return boost::indeterminate;
		}
	case http_version_h:
		if (input == 'H')
		{
			_state = http_version_t_1;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case http_version_t_1:
		if (input == 'T')
		{
			_state = http_version_t_2;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case http_version_t_2:
		if (input == 'T')
		{
			_state = http_version_p;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case http_version_p:
		if (input == 'P')
		{
			_state = http_version_slash;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case http_version_slash:
		if (input == '/')
		{
			req.httpVersionMajor = 0;
			req.httpVersionMinor = 0;
			_state = http_version_major_start;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case http_version_major_start:
		if (is_digit(input))
		{
			req.httpVersionMajor = req.httpVersionMajor * 10 + input - '0';
			_state = http_version_major;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case http_version_major:
		if (input == '.')
		{
			_state = http_version_minor_start;
			return boost::indeterminate;
		}
		else if (is_digit(input))
		{
			req.httpVersionMajor = req.httpVersionMajor * 10 + input - '0';
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case http_version_minor_start:
		if (is_digit(input))
		{
			req.httpVersionMinor = req.httpVersionMinor * 10 + input - '0';
			_state = http_version_minor;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case http_version_minor:
		if (input == '\r')
		{
			_state = expecting_newline_1;
			return boost::indeterminate;
		}
		else if (is_digit(input))
		{
			req.httpVersionMinor = req.httpVersionMinor * 10 + input - '0';
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case expecting_newline_1:
		if (input == '\n')
		{
			_state = header_line_start;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case header_line_start:
		if (input == '\r')
		{
			_state = expecting_newline_3;
			return boost::indeterminate;
		}
		else if (!req.Headers.empty() && (input == ' ' || input == '\t'))
		{
			_state = header_lws;
			return boost::indeterminate;
		}
		else if (!is_char(input) || is_ctl(input) || is_tspecial(input))
		{
			return false;
		}
		else
		{
			req.Headers.push_back(Header());
			req.Headers.back().name.push_back(input);
			_state = header_name;
			return boost::indeterminate;
		}
	case header_lws:
		if (input == '\r')
		{
			_state = expecting_newline_2;
			return boost::indeterminate;
		}
		else if (input == ' ' || input == '\t')
		{
			return boost::indeterminate;
		}
		else if (is_ctl(input))
		{
			return false;
		}
		else
		{
			_state = header_value;
			req.Headers.back().value.push_back(input);
			return boost::indeterminate;
		}
	case header_name:
		if (input == ':')
		{
			_state = space_before_header_value;
			return boost::indeterminate;
		}
		else if (!is_char(input) || is_ctl(input) || is_tspecial(input))
		{
			return false;
		}
		else
		{
			req.Headers.back().name.push_back(input);
			return boost::indeterminate;
		}
	case space_before_header_value:
		if (input == ' ')
		{
			_state = header_value;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case header_value:
		if (input == '\r')
		{
			_state = expecting_newline_2;
			return boost::indeterminate;
		}
		else if (is_ctl(input))
		{
			return false;
		}
		else
		{
			req.Headers.back().value.push_back(input);
			if(req.Headers.back().name == "Content-Length")
			{
				req.contentLength = from_string<int>(req.Headers.back().value);
				req.postValue.reserve(req.contentLength);
			}

			return boost::indeterminate;
		}
	case expecting_newline_2:
		if (input == '\n')
		{
			_state = header_line_start;
			return boost::indeterminate;
		}
		else
		{
			return false;
		}
	case expecting_newline_3:
		_state = expecting_newline_4;
		if(req.contentLength != 0)
			return boost::indeterminate;
		else
			return true;
		

	case expecting_newline_4:
		req.postValue.push_back(input);
		if(req.postValue.length() >= req.contentLength)
		{
			
			return true;
		}
		else
		{
			return boost::indeterminate;
		}
	default:
		return false;
	}
}

bool RequestParser::is_char(int c)
{
	return c >= 0 && c <= 127;
}

bool RequestParser::is_ctl(int c)
{
	return c >= 0 && c <= 31 || c == 127;
}

bool RequestParser::is_tspecial(int c)
{
	switch (c)
	{
	case '(': case ')': case '<': case '>': case '@':
	case ',': case ';': case ':': case '\\': case '"':
	case '/': case '[': case ']': case '?': case '=':
	case '{': case '}': case ' ': case '\t':
		return true;
	default:
		return false;
	}
}

bool RequestParser::is_digit(int c)
{
	return c >= '0' && c <= '9';
}

