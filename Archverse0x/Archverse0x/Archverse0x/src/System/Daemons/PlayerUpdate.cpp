#include "System/Daemons/PlayerUpdate.h"
#include "Game/Player.h"
#include "Game/Council.h"
#include "Game/Territory.h"
#include "Game/ControlModel.h"
#include "Game/Archverse.h"
#include "Game/GameInfo.h"


using namespace ArchverseCore::System::Daemons;
using namespace ArchverseCore::Game;

void PlayerUpdate::ProcessCouncil(ArchverseCore::Game::Council& council, uint32_t tick)
{
	ControlModel controlModel;
	council.Players().ForeachWrite([&](Player& targetPlayer) -> bool
	{
		PlayerUpdate::ProcessPlayer(council, targetPlayer, controlModel, tick);
		return true;
	});
}
void PlayerUpdate::ProcessPlayer(ArchverseCore::Game::Council& council, ArchverseCore::Game::Player& player, ArchverseCore::Game::ControlModel& councilControlModel, uint32_t tick)
{
	if(player.Update(council, councilControlModel, tick))
	{
		player.Territories().ForeachWrite([&](Territory& targetTerritory) -> bool
		{
			PlayerUpdate::ProcessTerritory(council, player, targetTerritory);
			return true;
		});
	}
}
void PlayerUpdate::ProcessTerritory(ArchverseCore::Game::Council& council, ArchverseCore::Game::Player& player, ArchverseCore::Game::Territory& territory)
{
	territory.Update(council, player, player.Turn());
}

int PlayerUpdate::Run(uint32_t currentTick, uint32_t lastTickToProcess)
{
	GameInfoData localGameInfo;
	GameInfo::Councils(localGameInfo).ForeachWrite([&](Council& targetCouncil) -> bool 
	{
		PlayerUpdate::ProcessCouncil(targetCouncil, currentTick);
		return true;
	});
	return 0;
}