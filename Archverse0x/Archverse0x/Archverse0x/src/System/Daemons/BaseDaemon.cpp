#include "System\Daemons\BaseDaemon.h"

using namespace ArchverseCore::System::Daemons;

void BaseDaemon::Run(uint32_t currentTick)
{
	_lastCycleTime = Run(_lastTickProcessed, currentTick);
	_lastTickProcessed = currentTick;
	
}