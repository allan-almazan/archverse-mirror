#include "System/Daemons/DaemonManager.h"
#include "Game/Archverse.h"
#include "Game/GameInfo.h"
#include <time.h>
#include <boost/thread/thread.hpp>
#include <boost/thread/xtime.hpp>
#include <iostream>
#include <exception>

using namespace ArchverseCore::System::Daemons;
using namespace ArchverseCore::Game;
using namespace ArchverseCore::System;

void DaemonManager::AddDaemon(ArchverseCore::System::Daemons::BaseDaemon *newDaemon)
{
	_registeredDaemons.push_back(newDaemon);
}

void DaemonManager::Run()
{
	while(GAMEINFO.Running())
	{
		uint32_t tick = GAMEINFO.Tick();
		boost::xtime startTick;
		boost::xtime_get(&startTick, boost::TIME_UTC);
		BOOST_FOREACH(ArchverseCore::System::Daemons::BaseDaemon *daemon, _registeredDaemons)
		{
			try
			{
				daemon->Run(tick);
			}
			catch(std::exception& ex)
			{
				std::cout<<ex.what();
			}
			catch(std::exception* ex)
			{
				std::cout<<ex->what();
			}
			catch(...)
			{
				std::cout<<"unknown error";
			}
		}
		boost::xtime endTick;
		boost::xtime_get(&endTick, boost::TIME_UTC);
		uint32_t d = endTick.sec - startTick.sec;
		GAMEINFO.AddTicks(std::max(d, (uint32_t)1));

		if(d == 0)
		{
			boost::xtime xt;
			boost::xtime_get(&xt, boost::TIME_UTC);
			xt.sec += 1;
			boost::thread::sleep(xt); // Sleep for 1 second
		}
	}
}